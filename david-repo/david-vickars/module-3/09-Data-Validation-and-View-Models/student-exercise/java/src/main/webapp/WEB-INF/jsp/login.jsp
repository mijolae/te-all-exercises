<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="pageTitle" value="Home Page"/>
<%@include file="common/header.jspf" %>

		<c:url var="loginUrl" value="/loginTest"/>
		<form:form method="POST" action="${loginUrl}" modelAttribute="login">
			
			<div>
				<label for="email">Email Address</label>
				<form:input path="email"/>
				<form:errors path="email" cssClass="error"/>
			</div>
			<div>
				<label for="password">Password</label>
				<form:input type="password" path="password"/>
				<form:errors path="password" cssClass="error"/>
			</div>
			<div>
				<input type="submit" value="Login"/>
			</div>
		</form:form>
<%@include file="common/footer.jspf" %>