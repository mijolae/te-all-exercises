package com.techelevator.validation.controller;

import com.techelevator.validation.model.Login;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class UserController {
	// GET: /
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String getMainScreen() {

		return "homePage";
	}

	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public String showLoginForm(ModelMap modelHolder) {
		if (!modelHolder.containsAttribute("login")) {
			modelHolder.put("login", new Login());
		}

		return "login";
	}

	@RequestMapping(path = "/loginTest")
	public String processLoginForm(
			@Valid @ModelAttribute Login login,
			BindingResult result,
			RedirectAttributes flash) {

		flash.addFlashAttribute("login", login);

		if (result.hasErrors()) {
			flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "login", result);
			return "redirect:/login";
		}

		return "redirect:/confirmation";
	}

	@RequestMapping(path = "/confirmation", method = RequestMethod.GET)
	public String showConfirmationPage() {
		return "confirmation";
	}


	// Add the following Controller Actions

	// GET: /register
	// Return the empty registration view

	// POST: /register
	// Validate the model and redirect to confirmation (if successful) or return
	// the
	// registration view (if validation fails)

	// GET: /login
	// Return the empty login view

	// POST: /login
	// Validate the model and redirect to a confirmation page if validation is
	// successful. Return the login view (if validation fails).

	// GET: /confirmation
	// Return the confirmation view
}
