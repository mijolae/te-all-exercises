package com.techelevator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PersonServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		/*
		 * Pretend this list was created by a call to the PersonDao object
		 */
		List<Person> personList = new ArrayList<Person>();
		personList.add(new Person("Mike", "Mazzullo", 21));
		personList.add(new Person("Asher", "Díaz", 22));
		personList.add(new Person("Sondra", "Coffin", 23));
		personList.add(new Person("Jeff", "Prescott", 12));

		/*
		 * Store our personList as a request scoped (gone when request sends back a response)
		 * so our JSP has the data to build the view
		 */
		req.setAttribute("WeMakeUpThisName", personList);

		/*
		 * This sends the request over to the JSP to build the response.
		 * Fancy language of saying this: we dispatch of request to the jsp to build the view
		 */
		getServletContext().getRequestDispatcher("/WEB-INF/personList.jsp").forward(req, resp);
	}
}
