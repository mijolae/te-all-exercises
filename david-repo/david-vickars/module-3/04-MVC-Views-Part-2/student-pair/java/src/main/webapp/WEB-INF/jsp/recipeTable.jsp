<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Recipe Table View</title>
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
        <ul>
            <li><a href="recipeTiles">Tile Layout</a></li>
            <li><a href="recipeTable">Table Layout</a></li>
        </ul>
        
    </nav>
    <section id="main-content">
        <h2>Recipes</h2>
       <!-- Use the request attribute "recipes" (List<Recipe>) -->
        <div class="recipes_flex">
            <c:set var="count" value="0"/>
            <c:forEach var="recipe" items="${recipes}">
               <div>
                   <img src="<c:url value="/img/recipe${count}.jpg" />" />
                   <c:set var="count" value="${count+1}"/>
                   <br/>
                   ${recipe.name}<br/>
                   ${recipe.recipeType}<br/>
                   ${recipe.cookTimeInMinutes} min<br/>
                   ${recipe.ingredients.size()} ingredients<br/>
                   <img src="<c:url value="/img/${Math.round(recipe.averageRating)}-star.png" />" />
               </div>
            </c:forEach>
        </div>
    </section>
</body>
</html>