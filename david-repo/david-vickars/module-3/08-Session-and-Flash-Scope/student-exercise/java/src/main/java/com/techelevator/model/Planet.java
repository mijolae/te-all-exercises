package com.techelevator.model;

public enum Planet {

    MERCURY(0.36),
    EARTH(1.0),
    VENUS(0.9);

    private double gravityConstant;

    private Planet(double gravityConstant){
        this.gravityConstant = gravityConstant;
    }

    public double getGravityConstant(){
        return gravityConstant;
    }

    public static Planet toPlanet(String planet){
        if (planet.equals("MERCURY"))
            return MERCURY;
        else if (planet.equals("EARTH"))
            return EARTH;
        else if (planet.equals("VENUS"))
            return VENUS;
        else
            return null;
    }
}
