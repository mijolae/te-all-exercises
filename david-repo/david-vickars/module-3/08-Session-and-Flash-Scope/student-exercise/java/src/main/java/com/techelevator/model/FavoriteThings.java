package com.techelevator.model;

public class FavoriteThings {

    private String color;

    private String food;

    private String season;

    private String planet;

    private double gravityConstant;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    public double getGravityConstant() {
        return gravityConstant;
    }

    public void setGravityConstant(double gravityConstant) {
        this.gravityConstant = gravityConstant;
    }
}
