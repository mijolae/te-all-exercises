<%@ page language="java" import="com.techelevator.model.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp">
	<c:param name="title" value="Favorite Season" />
</c:import>

<form method="POST" action="">
	<label>What is your favorite season?</label>
	<input type="text" name="favoriteSeason">
    <select name="planet">
        <c:forEach var="planet" items="${Planet.values()}">
            <option value="${planet}">${planet}</option>
        </c:forEach>
    </select>
	<button type="submit">Next >>></button>
</form>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />