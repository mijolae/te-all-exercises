1. What does REST stand for? 

    a. Resource Enterprise State Transfer
    b. Resource Enterprise State
    c. Representational State Topics
    d. Representational State Transfer *

2. REST was created to combat the complexity of this architecture 

    a. SOUP
    b. SOAP *
    c. STATE
    d. SAP

3. The key abstraction of information in a REST service is also referred to as:

    a. Resource *
    b. Verb
    c. Header
    d. Status Code

4. Which of the following is not a valid HTTP Method:

    a. POST
    b. DELETE
    c. LIST *
    d. GET

5. What can we use to pass additional information wit the request or response:

    a. Status Code
    b. Header *
    c. Verb
    d. URI

6. Which HTTP method would you use when you are submitting an online form to add a new comment to a blog post: 

    a. GET
    b. PATCH
    c. PUT
    d. POST *

7. What HTTP method would you use when submitting an online form to update your user profile: 

    a. PUT *
    b. POST
    c. GET
    d. None of the above

8. What category of status codes is used for Client Error Responses

    a. 500.x
    b. 400.x *
    c. 300.x
    d. 200.x

9. If we wanted to display a list of todos what would be a logical resource name

    a. my-awesome-list-of-todos
    b. todos *
    c. display-todos
    d. view-todos
    
