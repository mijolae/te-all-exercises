package com.techelevator.data.jdbc;

import com.techelevator.data.JDBCParkDAO;
import com.techelevator.entity.Park;
import org.junit.*;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.util.Map;

import static org.junit.Assert.*;

public class JDBCParkDAOTest {
    private static SingleConnectionDataSource dataSource;
    private JDBCParkDAO dao;


    @BeforeClass
    public static void beforeClass() throws Exception {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        dataSource.destroy();
    }

    @Before
    public void setUp() throws Exception {
        dao = new JDBCParkDAO(dataSource);
    }

    @After
    public void tearDown() throws Exception {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getParks() {
    }

    @Test
    public void testGetParks() {
        Map<String, Park> actual = dao.getParks();
        assertEquals(3, actual.size());
    }

    @Test
    public void getParksByName() {
        Park actual = dao.getParks().get("Acadia");
        Park testPark = dao.getParksByName("Acadia");
        assertParksAreEqual(testPark, actual);
    }

    private void assertParksAreEqual(Park parkOne, Park parkTwo){
        assertEquals(parkOne.getPark_id(), parkTwo.getPark_id());
        assertEquals(parkOne.getName(),parkTwo.getName());
        assertEquals(parkOne.getLocation(),parkTwo.getLocation());
        assertEquals(parkOne.getEstablish_date(), parkTwo.getEstablish_date());
        assertEquals(parkOne.getArea(), parkTwo.getArea());
        assertEquals(parkOne.getVisitors(), parkTwo.getVisitors());
        assertEquals(parkOne.getDescription(), parkTwo.getDescription());
    }
}