package com.techelevator.data.jdbc;

import org.junit.*;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class JDBCReservationDAOTest {
    private static SingleConnectionDataSource dataSource;
    private JDBCReservationDAO dao;
    private JDBCCampgroundDAO daoTwo;

    @BeforeClass
    public static void beforeClass() throws Exception {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        dataSource.destroy();
    }

    @Before
    public void setUp() throws Exception {
        dao = new JDBCReservationDAO(dataSource);
    }

    @After
    public void tearDown() throws Exception {
        dataSource.getConnection().rollback();
    }

//    @Test
//    public void getAllCampgroundsAvailable() throws ParseException {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        String dateString = format.format(new Date());
//        Date dateFrom = format.parse("2020-07-14");
//        Date dateTo = format.parse("2020-08-01");
//
//        List<Site> testSites = dao.getAllCampgroundsAvailable(1l,dateFrom, dateTo);
//        assertEquals(5, testSites.size());
//
//    }

    @Test
    public void makeReservationForSelectedCampsite() {
    }

//    @Test
//    public void getReservationID() {
//        dao = new JDBCReservationDAO(dataSource);
//        Site testSite = new Site(1, 2, 2, 4,
//                true, 0, true );
//
//        String expected = "Your reservation has been confirmed. Confirmation ID: 53";
//        assertEquals(expected,dao.makeReservationForSelectedCampsite(testSite, "Tarek Mijolae", LocalDate.of(2020, 8, 14),
//                LocalDate.of(2020, 8, 20)));
//    }
}