package com.techelevator.data.jdbc;

import com.techelevator.entity.Campground;
import org.junit.*;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class JDBCCampgroundDAOTest {
    private static SingleConnectionDataSource dataSource;
    private JDBCCampgroundDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        dataSource.setAutoCommit(false);
    }

    @AfterClass
    public static void afterClass() {
        dataSource.destroy();
    }

    @Before
    public void setUp() {
        dao = new JDBCCampgroundDAO(dataSource);
    }

    @After
    public void tearDown() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getCampgrounds() {
        List<Campground> actual = dao.getCampgrounds();
        assertEquals(7, actual.size());
    }

    @Test
    public void getAllCampgroundsAvailableBasedOnParkChoice() {
        List<Campground> actual = dao.getAllCampgroundsAvailableBasedOnParkChoice(2);
        assertEquals(3, actual.size());
    }
}