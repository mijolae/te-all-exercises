package com.techelevator.data.jdbc;

import com.techelevator.data.ReservationDAO;
import com.techelevator.entity.Site;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JDBCReservationDAO implements ReservationDAO {
    private JdbcTemplate jdbcTemplate;
    public JDBCReservationDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Site> getAllCampgroundsAvailable(long campgroundId, Date from_date, Date to_date) {
        List<Site> reservationAvailable = new ArrayList<>();
        //statement below grabs the reservations that are not in the selected time frame
        String sqlSearchForAvailableReservation = "SELECT s.* "+
                                                     "FROM site as s "+
                                                     "JOIN campground as c ON c.campground_id = s.campground_id "+
                                                     "WHERE c.campground_id = ? "+
                                                     "AND s.site_id IN " +
                                                            "(SELECT site_id FROM reservation as r "+
                                                            "WHERE (? NOT BETWEEN r.from_date AND r.to_date) "+
                                                            "AND (? NOT BETWEEN r.from_date AND r.to_date)) "+
                                                    "LIMIT 5";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForAvailableReservation, campgroundId, from_date, to_date);
        while(results.next()){
            Site siteHolder = new Site(results.getInt("site_id"), results.getInt("campground_id"),
                                        results.getInt("site_number"), results.getInt("max_occupancy"),
                                        results.getBoolean("accessible"), results.getInt("max_rv_length"),
                                        results.getBoolean("utilities"));
            reservationAvailable.add(siteHolder);
        }


        return reservationAvailable;
    }

    public List<Site> getAllCampsitesAvailableParkWide(Date from_date, Date to_date) {
        List<Site> reservationAvailable = new ArrayList<>();
        //statement below grabs the reservations that are not in the selected time frame
        String sqlSearchForAvailableReservation = "SELECT s.* "+
                "FROM site as s "+
                "JOIN campground as c ON c.campground_id = s.campground_id "+
                "WHERE s.site_id IN " +
                "(SELECT site_id FROM reservation as r "+
                "WHERE (? NOT BETWEEN r.from_date AND r.to_date) "+
                "AND (? NOT BETWEEN r.from_date AND r.to_date)) "+
                "LIMIT 5";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForAvailableReservation, from_date, to_date);
        while(results.next()){
            Site siteHolder = new Site(results.getInt("site_id"), results.getInt("campground_id"),
                    results.getInt("site_number"), results.getInt("max_occupancy"),
                    results.getBoolean("accessible"), results.getInt("max_rv_length"),
                    results.getBoolean("utilities"));
            reservationAvailable.add(siteHolder);
        }


        return reservationAvailable;
    }



    public void makeReservationForSelectedCampsite(Site selectedReservation, String name, LocalDate startDate, LocalDate endDate){
        String sqlUpdateReservation = "INSERT INTO reservation (site_id, name, from_date, to_date, create_date)\n" +
                "VALUES(?, ?, ?, ?, ?)";
        jdbcTemplate.update(sqlUpdateReservation,selectedReservation.getSite_id(), name, startDate,
                endDate, LocalDate.now());

        int reservationId = getReservationID(selectedReservation);
        String confirmation = "Your reservation has been confirmed. Confirmation ID: " + reservationId;
        System.out.println(confirmation);
    }

    public int getReservationID(Site selectedReservation){
        String sqlGetReservationID = "SELECT reservation_id FROM reservation WHERE site_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetReservationID, selectedReservation.getSite_id());
        int reservationId = 0;
        while(result.next()){
            reservationId = result.getInt("reservation_id");
        }
        return reservationId;
    }

    public long getCampgroundIdFromName(String name){
        long campgroundIdToReturn = 0;
        String sqlGetCampgroundIdFromName = "SELECT campground_id "+
                                            "FROM campground "+
                                            "WHERE name = ?";

        SqlRowSet campgroundId = jdbcTemplate.queryForRowSet(sqlGetCampgroundIdFromName, name);
        while(campgroundId.next()){
            campgroundIdToReturn = campgroundId.getLong("campground_id");
        }

        return campgroundIdToReturn;
    }
}
