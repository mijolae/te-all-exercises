package com.techelevator.entity;

import java.time.LocalDate;

public class Park {

    private int park_id;
    private String name;
    private String location;
    private LocalDate establish_date;
    private String area;
    private String visitors;
    private String description;

    public Park(int park_id, String name, String location, LocalDate establish_date, String area, String visitors, String description) {
        this.park_id = park_id;
        this.name = name;
        this.location = location;
        this.establish_date = establish_date;
        this.area = area;
        this.visitors = visitors;
        this.description = description;
    }

    public static void main(String[] args){
        LocalDate date = LocalDate.of(2008, 10, 01);
        Park testPark = new Park(10, "Wolf Pond", "Staten Island, NY", date, "1,000,000",
                "2,000,000", "This is a test park.");
        System.out.println(testPark.toString());
    }

    @Override
    public String toString() {
        System.out.println(name + "\n" +
                "Location: " + "\t\t\t" + location + "\n" +
                "Established: " + "\t\t" + establish_date + "\n" +
                "Area: " + "\t\t\t\t" + area + " sq km\n" +
                "Annual Visitors: " + "\t" + visitors + "\n\n" +
                description);
        return "";
    }

    public int getPark_id() {
        return park_id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public LocalDate getEstablish_date() {
        return establish_date;
    }

    public String getArea() {
        return area;
    }

    public String getVisitors() {
        return visitors;
    }

    public String getDescription() {
        return description;
    }
}