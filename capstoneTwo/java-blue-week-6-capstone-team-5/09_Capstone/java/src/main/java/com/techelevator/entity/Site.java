package com.techelevator.entity;

public class Site {

    private int site_id;
    private int campground_id;
    private int site_number;
    private int max_occupancy;
    private boolean accessible;
    private int max_rv_length;
    private boolean utilities;

    public Site(int site_id, int campground_id, int site_number, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
        this.site_id = site_id;
        this.campground_id = campground_id;
        this.site_number = site_number;
        this.max_occupancy = max_occupancy;
        this.accessible = accessible;
        this.max_rv_length = max_rv_length;
        this.utilities = utilities;
    }

    public int getSite_id() {
        return site_id;
    }

    public int getCampground_id() {
        return campground_id;
    }

    public int getSite_number() {
        return site_number;
    }

    public int getMax_occupancy() {
        return max_occupancy;
    }

    public String isAccessible() {
        if(accessible){
            return "Yes";
        }
        return "No";
    }

    public String getMax_rv_length() {
        if(max_rv_length == 0){
            return "N/A";
        }
        return String.valueOf(max_rv_length);
    }

    public String isUtilities() {
        if(utilities){
            return "Yes";
        }
        return "No";
    }

    @Override
    public String toString() {
        return "Site{" +
                "site_id=" + site_id +
                ", campground_id=" + campground_id +
                ", site_number=" + site_number +
                ", max_occupancy=" + max_occupancy +
                ", accessible=" + accessible +
                ", max_rv_length=" + max_rv_length +
                ", utilities=" + utilities +
                '}';
    }
}
