package com.techelevator.data;

import com.techelevator.entity.Park;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;


public class JDBCParkDAO implements ParkDao {

    private JdbcTemplate jdbcTemplate;

    public JDBCParkDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Map<String, Park> getParks() throws DataAccessException {
        return getParks(null, null);
    }

    @Override
    public Map<String, Park> getParks(String connectionString, String delimiter) throws DataAccessException {
        Map<String, Park> parkMap = new LinkedHashMap<>();
        String sqlGetAllSites = "SELECT * FROM park";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllSites);
        while (results.next()) {
            Park park = mapRowToPark(results);
            if (park != null) {
                parkMap.put(park.getName(), park);
            }
        }
        return parkMap;
    }

    public Park getParksByName(String name){
        Park park = null;
        String sqlGetAllSites = "SELECT * FROM park WHERE name = ?";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllSites, name);
        results.next();
        park = mapRowToPark(results);
        return park;
    }



    private Park mapRowToPark(SqlRowSet result) {
        Park currentPark = null;
        int park_id = result.getInt("park_id");
        String name = result.getString("name");
        String location = result.getString("location");
        LocalDate establish_date = result.getDate("establish_date").toLocalDate();
        String area = result.getString("area");
        String visitors = result.getString("visitors");
        String description = result.getString("description");
        currentPark = new Park(park_id, name, location, establish_date, area, visitors, description);
        return currentPark;
    }
}
