package com.techelevator.data.jdbc;

import com.techelevator.entity.Site;
import com.techelevator.data.SiteDAO;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class JDBCSiteDAO implements SiteDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCSiteDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Map<Integer, Site> getSites() throws DataAccessException {
        return getSites(null, null);
    }

    @Override
    public Map<Integer, Site> getSites(String connectionString, String delimiter) throws DataAccessException {
        Map<Integer, Site> siteMap = new LinkedHashMap<>();
        String sqlGetAllSites = "SELECT * FROM site";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllSites);
        while (results.next()) {
            Site site = mapRowToSite(results);
            if (site != null) {
                siteMap.put(site.getSite_id(), site);
            }
        }
        return siteMap;
    }

    public void displayParkWideSites(List<Site> siteList){
        System.out.println("Campground\tSite No.\t Max Occup.\tAccessible?\tMax RV Length\tUtility\tCost");
        for(Site site : siteList){
            String result = getNameOfCampground(site.getCampground_id()) + "\t"+ site.getSite_number() + "\t\t" + site.getMax_occupancy() + "\t" + site.isAccessible() +
                    "\t\t" + site.getMax_rv_length() + "\t\t" + site.isUtilities() + "\t\t" + getDailyFee(site.getCampground_id());
            System.out.println(result);
        }
    }

    public void displaySites(List<Site> siteList){
        System.out.println("Site No.\t Max Occup.\tAccessible?\tMax RV Length\tUtility\tCost");
        for(Site site : siteList){
            String result = site.getSite_number() + "\t" + site.getMax_occupancy() + "\t" + site.isAccessible() +
                    "\t" + site.getMax_rv_length() + "\t" + site.isUtilities() + "\t" + getDailyFee(site.getCampground_id());
            System.out.println(result);
        }
    }

    public Site getSiteBySiteId(int siteId){
        Site finalResult = null;
        String sqlGetFee = "SELECT * "+
                "FROM site "+
                "WHERE site_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetFee, siteId);
        if(result.next()){
            finalResult = mapRowToSite(result);
        }
        return finalResult;
    }

    private BigDecimal getDailyFee(Integer campId){
        BigDecimal finalResult = null;
        String sqlGetFee = "SELECT daily_fee "+
                "FROM campground "+
                "WHERE campground_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetFee, campId);
        if(result.next()){
            finalResult = result.getBigDecimal("daily_fee");
        }
        return finalResult;
    }

    private String getNameOfCampground(Integer campId){
        String finalResult = null;
        String sqlGetFee = "SELECT name "+
                "FROM campground "+
                "WHERE campground_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetFee, campId);
        if(result.next()){
            finalResult = result.getString("name");
        }
        return finalResult;
    }

    private Site mapRowToSite(SqlRowSet result) {
        Site currentSite = null;
        int site_id = result.getInt("site_id");
        int campground_id = result.getInt("campground_id");
        int site_number = result.getInt("site_number");
        int max_occupancy = result.getInt("max_occupancy");
        boolean accessible = result.getBoolean("accessible");
        int max_rv_length = result.getInt("max_rv_length");
        boolean utilities = result.getBoolean("utilities");
        currentSite = new Site(site_id, campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities);
        return currentSite;
    }
}
