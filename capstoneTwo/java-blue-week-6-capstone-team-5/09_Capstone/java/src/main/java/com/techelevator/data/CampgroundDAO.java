package com.techelevator.data;

import com.techelevator.entity.Campground;

import java.util.List;

public interface CampgroundDAO {
    public List<Campground> getCampgrounds();
    public List<Campground> getAllCampgroundsAvailableBasedOnParkChoice(int park_id);

}
