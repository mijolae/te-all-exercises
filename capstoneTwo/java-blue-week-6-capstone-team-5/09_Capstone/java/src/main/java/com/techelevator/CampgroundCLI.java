package com.techelevator;

import javax.sql.DataSource;


import com.techelevator.data.*;
import com.techelevator.data.JDBCParkDAO;
import com.techelevator.data.jdbc.JDBCSiteDAO;

import com.techelevator.data.jdbc.JDBCCampgroundDAO;
import com.techelevator.data.jdbc.JDBCReservationDAO;
import com.techelevator.entity.Campground;
import com.techelevator.entity.Park;
import com.techelevator.entity.Site;
import com.techelevator.view.Menu;

import org.apache.commons.dbcp2.BasicDataSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class CampgroundCLI {
	private CampgroundDAO dao;

	private static final String MAIN_MENU_OPTION_ACADIA = "Acadia";
	private static final String MAIN_MENU_OPTION_ARCHES = "Arches";
	private static final String MAIN_MENU_OPTION_CUYAHOGA = "Cuyahoga Valley";
	private static final String MAIN_MENU_OPTION_QUIT = "Quit";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_OPTION_ACADIA, MAIN_MENU_OPTION_ARCHES,
			MAIN_MENU_OPTION_CUYAHOGA, MAIN_MENU_OPTION_QUIT};

	private static final String SUBMENU_OPTION_VIEW_CAMPGROUNDS = "View Campgrounds";
	private static final String SUBMENU_OPTION_SEARCH_RESERVATION = "Search for Reservation";
	private static final String SUBMENU_OPTION_RETURN = "Return to Previous Screen";
	private static final String[] SUBMENU_OPTIONS = { SUBMENU_OPTION_VIEW_CAMPGROUNDS, SUBMENU_OPTION_SEARCH_RESERVATION,
			SUBMENU_OPTION_RETURN};

	private static final String CAMPGROUND_MENU_RETURN = "Return to Previous Screen";
	private static final String CAMPGROUND_MENU_SEARCH_AVAILABLE_RESERVATIONS = "Search for Available Reservation";
	private static final String[] VIEW_CAMPGROUND_MENU_OPTIONS = {CAMPGROUND_MENU_SEARCH_AVAILABLE_RESERVATIONS, CAMPGROUND_MENU_RETURN};

	private Menu menu;
	private CampgroundCLI campgroundCLI;
	private Park individualPark;

	private JDBCParkDAO park;
	private JDBCCampgroundDAO campground;
	private JDBCReservationDAO reservation;
	private JDBCSiteDAO site;


	public static void main(String[] args) {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		Menu menu = new Menu(System.in, System.out);


		CampgroundCLI application = new CampgroundCLI(dataSource, menu);
		application.run();
	}



	public CampgroundCLI(DataSource datasource,Menu menu) {
		this.menu = menu;
		park = new JDBCParkDAO(datasource);
		site = new JDBCSiteDAO(datasource);
		reservation = new JDBCReservationDAO(datasource);
		campground = new JDBCCampgroundDAO(datasource);
	}

	public void run() {
		while (true) {
			String choice = (String) menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);
			if (choice.equals(MAIN_MENU_OPTION_ACADIA)) {
				pickIndividualPark(choice);
			} else if (choice.equals(MAIN_MENU_OPTION_ARCHES)) {
				pickIndividualPark(choice);
			} else if (choice.equals(MAIN_MENU_OPTION_CUYAHOGA)) {
				pickIndividualPark(choice);
			} else if (choice.equals(MAIN_MENU_OPTION_QUIT)) {
				System.exit(0);
			}
		}
	}

	public void runCampgroundMenu(Park park) {
		while (true) {
			String choice = (String) menu.getChoiceFromOptions(SUBMENU_OPTIONS);
			List<Campground> campgroundList = null;
			if (choice.equals(SUBMENU_OPTION_VIEW_CAMPGROUNDS)) {
				ViewCampgrounds(individualPark);
				runViewCampgroundMenu();
			} else if (choice.equals(SUBMENU_OPTION_SEARCH_RESERVATION)) {
				String[] dates = getUserReservationDates();
				List<Site> reservationList = reservation.getAllCampsitesAvailableParkWide(getDate(dates[0]), getDate(dates[1]));
				site.displayParkWideSites(reservationList);
				bookReservation(dates);
				//make reservation method
			} else if (choice.equals(SUBMENU_OPTION_RETURN)) {
				break;
			}
		}
	}

	public void runViewCampgroundMenu() {
		while (true) {
			String choice = (String) menu.getChoiceFromOptions(VIEW_CAMPGROUND_MENU_OPTIONS);
			if (choice.equals(CAMPGROUND_MENU_SEARCH_AVAILABLE_RESERVATIONS)) {
				searchForReservation();
				break;
			} else if (choice.equals(CAMPGROUND_MENU_RETURN)) {
				break;
			}
		}
	}

	public void pickIndividualPark(String choice){
		System.out.println("\nPark Information Screen");
		individualPark = park.getParksByName(choice);
		System.out.println(individualPark);
		runCampgroundMenu(individualPark);
	}

	public String[] getUserReservationDates(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("What is the arrival date? (YYYY/MM/DD)");
		String result = scanner.next();
		System.out.println("What is the departure date? (YYYY/MM/DD)");
		result = result + " " + scanner.next() + " ";
		String[] results = result.split(" ");
		String from_date = results[0];
		String to_date = results[1];
		String[] userData = {from_date,to_date};
		return userData;
	}

	public String[] getUserDataForReservation(Park park) {
		List<Campground> campgroundList = campground.getAllCampgroundsAvailableBasedOnParkChoice(park.getPark_id());
		Scanner scanner = new Scanner(System.in);
		System.out.println("Which campground (enter 0 to cancel)?");
		String result = scanner.next();
		if(Integer.parseInt(result) == 0) {
			runViewCampgroundMenu();
		} else {
			System.out.println("What is the arrival date? (YYYY/MM/DD)");
			result = result + " " + scanner.next();
			System.out.println("What is the departure date? (YYYY/MM/DD)");
			result = result + " " + scanner.next() + " ";
			String[] results = result.split(" ");
			String campgroundNumberChoice = results[0];
			String from_date = results[1];
			String to_date = results[2];
			String name = campgroundList.get(Integer.parseInt(campgroundNumberChoice) - 1).getName();
			String[] userData = {name, from_date, to_date};
			return userData;
		}
		return null;
	}

	public void ViewCampgrounds(Park park) {
		List<Campground> campgroundList = campground.getAllCampgroundsAvailableBasedOnParkChoice(park.getPark_id());
		System.out.println("\nPark Campgrounds");
		System.out.println(park.getName() + " National Park Campgrounds");
		campground.displayCamps(campgroundList);
	}

	private Date getDate(String date){
		Date finalDate = null;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
			String dateString = formatter.format(new Date());
			finalDate = formatter.parse(date);
		}catch (ParseException exception){
			exception.printStackTrace();
		}
		return finalDate;
	}

	private void searchForReservation(){
		String[] userData = getUserDataForReservation(individualPark);
		if(userData == null){
			return;
		}
		long campgroundId = reservation.getCampgroundIdFromName(String.valueOf(userData[0]));
		List<Site> siteList = reservation.getAllCampgroundsAvailable(campgroundId, getDate(userData[1]), getDate(userData[2]));
		site.displaySites(siteList);
		bookReservation(userData);
	}

	private void bookReservation(String[] userData) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Which site should be reserved (enter 0 to cancel)?");
		String result = scanner.next();
		if(Integer.parseInt(result) == 0 || userData.length == 0) {
			return;
		}
		System.out.println("What name should the reservation be made under?");
		String name = scanner.next();
		String[] results = {result, name};
		Site toBook = site.getSiteBySiteId(Integer.parseInt(results[0]));
		if(userData.length == 3){
			reservation.makeReservationForSelectedCampsite(toBook,results[1], LocalDate.parse(userData[1]),LocalDate.parse(userData[2]));
		}else if(userData.length == 2){
			reservation.makeReservationForSelectedCampsite(toBook,results[1], LocalDate.parse(userData[0]),LocalDate.parse(userData[1]));
		}
	}

}
