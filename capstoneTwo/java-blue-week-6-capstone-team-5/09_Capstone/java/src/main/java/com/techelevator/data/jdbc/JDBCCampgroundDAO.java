package com.techelevator.data.jdbc;

import com.techelevator.entity.Campground;
import com.techelevator.data.CampgroundDAO;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class JDBCCampgroundDAO implements CampgroundDAO {
    private JdbcTemplate jdbcTemplate;
    public JDBCCampgroundDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Campground> getCampgrounds(){
        List<Campground> campgroundList = new ArrayList<>();
        String sqlGetAllCamps = "SELECT * " +
                                "FROM campground";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllCamps);
        while(results.next()){
            Campground camp = mapRowToCampground(results);
            campgroundList.add(camp);
        }
        return campgroundList;
    }

    @Override
    public List<Campground> getAllCampgroundsAvailableBasedOnParkChoice(int park_id) {
        List<Campground> campgroundList = new ArrayList<>();
        String sqlGetAllCamps = "SELECT * " +
                "FROM campground " +
                "WHERE park_id = ?";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllCamps, park_id);
        while(results.next()){
            Campground camp = mapRowToCampground(results);
            campgroundList.add(camp);
        }
        return campgroundList;
    }

    public long getCampgroundId(Campground camp){
        String sqlFindId = "SELECT campground_id " +
                            "FROM campground " +
                            "WHERE name = ?";
        SqlRowSet campId = jdbcTemplate.queryForRowSet(sqlFindId, camp.getName());
        long campIdNumber = 0;
        if(campId.next()){
            Campground campHolder = mapRowToCampground(campId);
            campIdNumber = campHolder.getCampground_id();
        }
        return campIdNumber;
    }
    

    public void displayCamps(List<Campground> campList){
        System.out.println("\tName\t\t\tOpen\t\tClose\t\tDaily Fee");
        for(int i =  0; i < campList.size(); i++){
            Campground campToPrint = campList.get(i);
            System.out.print("#"+(i+1) + "\t");
            System.out.printf("%10s %10s %10s %10s\n", campToPrint.getName(), campToPrint.getOpenMonth(), campToPrint.getClosingMonth(),
                    campToPrint.getDailyFee());
        }
    }

    private Campground mapRowToCampground(SqlRowSet rowSet){
        Campground camp;
        camp = new Campground(rowSet.getLong("campground_id"),
                                rowSet.getLong("park_id"),
                                rowSet.getString("name"),
                                rowSet.getString("open_from_mm"),
                                rowSet.getString("open_to_mm"),
                                rowSet.getBigDecimal("daily_fee"));

        return camp;
    }
}
