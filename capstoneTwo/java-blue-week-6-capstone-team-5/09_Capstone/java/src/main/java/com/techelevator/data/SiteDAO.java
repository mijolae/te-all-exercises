package com.techelevator.data;

import com.techelevator.entity.Site;
import org.springframework.dao.DataAccessException;

import java.util.Map;

public interface SiteDAO {

    Map<Integer, Site> getSites() throws DataAccessException;

    Map<Integer, Site> getSites(String connectionStrong, String delimiter) throws DataAccessException;

}