package com.techelevator.data;

import com.techelevator.entity.Site;

import java.util.Date;
import java.util.List;

public interface ReservationDAO {
    public List<Site> getAllCampgroundsAvailable(long campId, Date from_date, Date to_date);

}
