package com.techelevator.data;

import com.techelevator.entity.Park;
import org.springframework.dao.DataAccessException;

import java.util.Map;

public interface ParkDao {

    Map<String, Park> getParks() throws DataAccessException;

    Map<String, Park> getParks(String connectionStrong, String delimiter) throws DataAccessException;

}

