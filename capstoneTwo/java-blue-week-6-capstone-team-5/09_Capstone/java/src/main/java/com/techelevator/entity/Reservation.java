package com.techelevator.entity;

import java.util.Date;

public class Reservation {
    private int siteId;
    private String name;
    private Date fromDate;
    private Date toDate;
    private Date createdDate;

    public Reservation() {
    }

    public Reservation(int siteId, String name, Date fromDate, Date toDate, Date createdDate) {
        this.siteId = siteId;
        this.name = name;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.createdDate = createdDate;
    }

    public int getSiteId() {
        return siteId;
    }

    public String getName() {
        return name;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }
}
