package com.techelevator.entity;

import com.techelevator.CampgroundCLI;
import com.techelevator.view.Menu;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.cglib.core.Local;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.text.DateFormatSymbols;

public class Campground {
    private long campground_id;
    private long park_id;
    private String name;
    private String openMonth;
    private String closingMonth;
    private BigDecimal dailyFee;

    public Campground() {
    }

    public Campground(long campground_id, long park_id, String name, String openMonth, String closingMonth, BigDecimal dailyFee) {
        this.campground_id = campground_id;
        this.park_id = park_id;
        this.name = name;
        this.openMonth = openMonth;
        this.closingMonth = closingMonth;
        this.dailyFee = dailyFee;
    }

    public long getCampground_id() {
        return campground_id;
    }

    public long getPark_id() {
        return park_id;
    }

    public String getName() {
        return name;
    }

    public String getOpenMonth() {
        int month = Integer.valueOf(openMonth);
        String monthOutput = new DateFormatSymbols().getMonths()[month - 1];
        return monthOutput;
    }

    public String getClosingMonth() {
        int month = Integer.valueOf(closingMonth);
        String monthOutput = new DateFormatSymbols().getMonths()[month - 1];
        return monthOutput;
    }

    public BigDecimal getDailyFee() {
        return dailyFee;
    }

    public String toString(){
        return name + "\t";
    }
}
