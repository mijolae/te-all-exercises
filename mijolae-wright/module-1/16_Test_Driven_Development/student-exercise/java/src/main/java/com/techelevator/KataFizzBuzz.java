package com.techelevator;

public class KataFizzBuzz {
    public String fizzBuzz(int value){

        boolean containsThree = (value / 10 == 3 || value % 10 == 3);
        boolean containsFive = (value / 10 == 5 || value % 10 == 5);

        if(value < 1 || value > 100){
          return "";
        } else if(value % 3 == 0 && value % 5 == 0 || containsThree && containsFive){
            return "FizzBuzz";
        }else if (value % 3 == 0 || containsThree){
            return "Fizz";
        }else if (value % 5 == 0 || containsFive){
            return "Buzz";
        }else{
            return String.valueOf(value);
        }
    }

}
