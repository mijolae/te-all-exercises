package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class KataPotter {
    public double getCost(int[] books){
        int[] bucket = new int[5];
        double[] priceOfSets = new double[] {0.0, 8.0, 15.2, 21.6, 25.6, 30.0};
        double totalPrice = 0.0;

        for(int i= 0; i < books.length; i++){
            bucket[books[i]]++;
        }

        while(sum(bucket) != 0){
            int booksInSet = 0;
            for(int i = 0; i < bucket.length; i++){
                if(bucket[i] != 0){
                    booksInSet++;
                    bucket[i]--;
                }
            }

            //check if the two sets of four example occurs
            if(booksInSet == 5 && sum(bucket) == 3 && !oneBook(bucket,3)){
                double twoSetsOfFour = 2 * (8.0 * 4 * .80);
                totalPrice += twoSetsOfFour;
                bucket = new int[] {0,0,0,0,0};
            }else totalPrice += priceOfSets[booksInSet];
        }

        return totalPrice;
    }

    private boolean oneBook(int[] arr, int n){
        for(int element : arr){
            if(n == element){
                return true;
            }
        }
        return false;
    }


    private int sum(int[] arr){
        int sum = 0;
        for(int i = 0; i < arr.length; i++){
            sum += arr[i];
        }

        return sum;
    }
}
