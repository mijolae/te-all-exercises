package com.techelevator;

import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KataPotterTest {
    
    private KataPotter subject;

    @Before
    public void setUp() throws Exception {
        subject = new KataPotter();
    }

    @Test
    public void testBasicPurchases(){
        //test for empty string
        assertEquals(0, subject.getCost(new int[]{}),0.0001);

        //test for one book
        assertEquals(8.0, subject.getCost(new int[]{0}),0.0001);
        assertEquals(8.0, subject.getCost(new int[]{1}),0.0001);
        assertEquals(8.0, subject.getCost(new int[]{2}),0.0001);
        assertEquals(8.0, subject.getCost(new int[]{3}),0.0001);
        assertEquals(8.0, subject.getCost(new int[]{4}),0.0001);
    }

    @Test
    public void testSimpleDiscounts(){
        assertEquals(15.20, subject.getCost(new int[] {0,1}), 0.0001);
        assertEquals(21.6, subject.getCost(new int[] {0,1,2}), 0.0001);
        assertEquals(25.6, subject.getCost(new int[] {0,1,2,3}), 0.0001);
        assertEquals(30.0, subject.getCost(new int[]{0,1,2,3,4}), 0.0001);
    }

    @Test
    public void testMultipleBooks(){
        assertEquals(8.0 + (8.0 * 2 * .95), subject.getCost(new int[]{1,1,2}), 0.0001);
        assertEquals((8.0 * 2 * .95) + (8.0 * 5 * .75), subject.getCost(new int[]{0,0,1,1,2,3,4}), 0.0001);
        assertEquals((8.0 * 4 * .80) + (8.0 * 3 * .90), subject.getCost(new int[]{1,1,2,2,3,3,4}), 0.0001);
        assertEquals(8.0 + (8.0 * 2 * .95) + (8.0 * 4 * .80), subject.getCost(new int[]{0,0,0,1,1,2,3}), 0.0001);
        assertEquals(8.0 + 8.0 + (8.0 * 2 * .95) +(8.0 * 3 * .90), subject.getCost(new int[]{2,2,2,2,3,3,4}), 0.0001);
        assertEquals((8.0 * 3) + (8.0 * 3 * .90), subject.getCost(new int[]{0,0,0,0,3,4}), 0.0001);
    }

    @Test
    public void testComplexDiscounts() {
        assertEquals(3 * (8.0 * 5 * .75) + 2 * (8.0 * 2 * .95), subject.getCost(new int[]{0,0,0,0,0,1,1,1,1,1,2,2,2,3,3,3,4,4,4}), 0.0001);
        assertEquals(2 * (8.0 * 4 * .80) + 3 * (8.0 * 2 * .95), subject.getCost(new int[]{1,1,1,1,1,2,2,2,2,2,3,3,4,4}), 0.0001);
        assertEquals(3 * (8.0 * 2 * .95) + (8.0 * 3), subject.getCost(new int[]{3,3,3,3,3,3,4,4,4}), 0.0001);
        assertEquals(2 * (8.0 * 4 * .80), subject.getCost(new int[]{0,0,1,1,2,2,3,4}), 0.0001);
    }
}
