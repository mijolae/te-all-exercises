package com.techelevator;

import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KataFizzBuzzTest {

    private KataFizzBuzz fizzBuzzTest;

    @Before
    public void setup() throws Exception{
        fizzBuzzTest = new KataFizzBuzz();
    }

    @Test
    public void  numberIsDivisibleByThree(){
        assertEquals("Fizz", fizzBuzzTest.fizzBuzz(12));
        assertEquals("2", fizzBuzzTest.fizzBuzz(2));
        assertEquals("Fizz", fizzBuzzTest.fizzBuzz(24));
    }

    @Test
    public void numberIsDivisbleByFive(){
        assertEquals("Buzz", fizzBuzzTest.fizzBuzz(20));
        assertEquals("Buzz", fizzBuzzTest.fizzBuzz(40));
        assertEquals("22", fizzBuzzTest.fizzBuzz(22));
    }

    @Test
    public void numberIsDivisibleByBothThreeAndFive(){
        assertEquals("Buzz",fizzBuzzTest.fizzBuzz(58));
        assertEquals("77",fizzBuzzTest.fizzBuzz(77));
        assertEquals("FizzBuzz",fizzBuzzTest.fizzBuzz(60));
    }

    @Test
    public void turnNumberIntoEmptyStringIfOutOfRange(){
        assertEquals("",fizzBuzzTest.fizzBuzz(101));
        assertEquals("",fizzBuzzTest.fizzBuzz(-1));
        assertEquals("",fizzBuzzTest.fizzBuzz(0));
    }

    @Test
    public void numberIsEitherDivisibleByOrContainsThree(){
        assertEquals("Fizz", fizzBuzzTest.fizzBuzz(13));
        assertEquals("Fizz", fizzBuzzTest.fizzBuzz(37));
        assertEquals("Fizz", fizzBuzzTest.fizzBuzz(9));
    }

    @Test
    public void numberIsEitherDivisibleByOrContainsFive(){
        assertEquals("Buzz", fizzBuzzTest.fizzBuzz(52));
        assertEquals("Buzz", fizzBuzzTest.fizzBuzz(59));
        assertEquals("Buzz", fizzBuzzTest.fizzBuzz(25));
    }

    @Test
    public void numberIsEitherDivisbleOrContainsThreeAndFive(){
        assertEquals("FizzBuzz", fizzBuzzTest.fizzBuzz(30));
        assertEquals("FizzBuzz", fizzBuzzTest.fizzBuzz(53));
        assertEquals("FizzBuzz", fizzBuzzTest.fizzBuzz(35));
    }

}
