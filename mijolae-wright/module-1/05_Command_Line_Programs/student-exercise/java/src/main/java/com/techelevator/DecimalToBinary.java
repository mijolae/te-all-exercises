package com.techelevator;

import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter in a series of decimal values (separated by spaces): ");

		String input = scan.nextLine();

		String[] numbersToBinary = input.split(" ");

		for(int i = 0; i < numbersToBinary.length; i++){
			int number = Integer.valueOf(numbersToBinary[i]);
			String finalNumber ="";

			while(true){
				if(number / 2 == 0){
					finalNumber = (number % 2) + finalNumber;
					break;
				}

				int toBeAdded = number % 2;
				finalNumber = toBeAdded + finalNumber;
				number = number / 2;
			}

			System.out.println(numbersToBinary[i] + " in binary is " + finalNumber);
		}

	}

}
