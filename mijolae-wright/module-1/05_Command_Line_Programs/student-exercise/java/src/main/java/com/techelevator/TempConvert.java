package com.techelevator;

import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {
		Scanner	scan = new Scanner(System.in);

		System.out.println("Please enter the temperature: ");
		int temperature = Integer.parseInt(scan.nextLine());

		System.out.println("Is the temperature (C)elsius or (F)ahrenheit? ");
		String tempType = scan.nextLine();

		if(tempType.toUpperCase().equals("C")){
			int convertedValue = (int)((double)temperature * 1.8 + 32);
			System.out.println(temperature + "C is " + convertedValue + "F.");
		} else{
			int convertedValue = (int)((temperature - 32) / 1.8);
			System.out.println(temperature + "F is " + convertedValue + "C.");
		}



	}

}
