package com.techelevator;

import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter the length: ");
		int length = Integer.parseInt(scan.nextLine());

		System.out.println("Is the measurement in (m)eter, or (f)eet? ");
		String lengthType = scan.nextLine();

		if(lengthType.toUpperCase().equals("F")){
			int convertedValue = (int)((double)length * 0.3048);
			System.out.println(length + "f is " + convertedValue + "m.");
		} else{
			int convertedValue = (int)((double)length * 3.2808399);
			System.out.println(length + "m is " + convertedValue + "f.");
		}
	}

}
