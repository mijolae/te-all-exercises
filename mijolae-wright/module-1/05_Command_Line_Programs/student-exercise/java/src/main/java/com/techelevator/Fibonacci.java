package com.techelevator;

import java.util.ArrayList;
import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Please enter the Fibonacci number: ");
        int input = Integer.parseInt(scan.nextLine());

        int[] fibonacciNumbers = new int[2];

        fibonacciNumbers[0] = 0;
        fibonacciNumbers[1] = 1;

        System.out.print(fibonacciNumbers[0] + ", " + fibonacciNumbers[1]);

        while (true) {
        	int added = fibonacciNumbers[0] + fibonacciNumbers[1];
        	if(added <= input) {
				System.out.print(", " + added);
				fibonacciNumbers[0] = fibonacciNumbers[1];
				fibonacciNumbers[1] = added;
			} else {
        		break;
			}
        }


    }

}
