package com.techelevator;

import java.lang.reflect.Array;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer implements Accountable{l
    private String name;
    private String address;
    private String phoneNumber;
    private ArrayList<Accountable> accounts = new ArrayList<>();
    private Accountable Accountable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Accountable[] getAccounts() {
        Accountable[] finalAccounts = new Accountable[accounts.size()];
        for (int i = 0; i < accounts.size(); i++){
            finalAccounts[i] = accounts.get(i);
        }
        return finalAccounts;
    }

    public void addAccount(Accountable newAccount){
        if(newAccount != null) {
            accounts.add(newAccount);
        }
    }

    public int getBalance(){
        int totalCredits = 0;
        for(Accountable balance : accounts){
            totalCredits += balance.getBalance();

        }
        return totalCredits;
    }

    public boolean isVip(){
        if(this.getBalance() >= 25000){
            return true;
        }
        return false;
    }
}
