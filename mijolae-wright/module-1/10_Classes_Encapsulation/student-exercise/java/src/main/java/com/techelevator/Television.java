package com.techelevator;

public class Television {
    private boolean isOn = false;
    private int currentChannel = 3;
    private int currentVolume = 2;

    public Television(){

    }

    public boolean isOn() {
        return isOn;
    }

    public int getCurrentChannel() {
        return currentChannel;
    }

    public int getCurrentVolume() {
        return currentVolume;
    }

    public void turnOff(){
        isOn = false;
    }

    public void turnOn(){
        isOn = true;
        currentChannel = 3;
        currentVolume = 2;
    }

    public void changeChannel(int newChannel){
        if(newChannel >= 3 && newChannel <=18 && isOn){
            currentChannel = newChannel;
        }
    }

    public void channelUp(){
        if(currentChannel == 18 && isOn){
            currentChannel = 3;
        } else if (isOn) {
            currentChannel += 1;
        }

    }

    public void channelDown(){
        if(currentChannel == 3 && isOn){
            currentChannel = 18;
        }else if(isOn){
            currentChannel -= 1;
        }
    }

    public void raiseVolume(){
        if(isOn && currentVolume < 10){
            currentVolume++;
        }
    }

    public void lowerVolume(){
        if(isOn && currentVolume > 0){
            currentVolume--;
        }
    }

}
