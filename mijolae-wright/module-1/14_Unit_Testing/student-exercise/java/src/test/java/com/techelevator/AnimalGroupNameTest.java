package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AnimalGroupNameTest {
    private AnimalGroupName animalGroup;
    @Before
    public void setUp() throws Exception {
        animalGroup = new AnimalGroupName();
    }

    @Test
    public void getUnknownWithEmptyString() {
        String test_three = animalGroup.getHerd(" ");
        assertEquals("unknown", test_three);
    }

    @Test
    public void getUnknown() {
        String test_two = animalGroup.getHerd("walrus");
        assertEquals("unknown", test_two);
    }

    @Test
    public void getHerd() {
        String test_one = animalGroup.getHerd("elephant");
        assertEquals("Herd", test_one);
    }
}