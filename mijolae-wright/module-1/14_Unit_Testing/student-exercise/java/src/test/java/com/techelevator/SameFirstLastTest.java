package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SameFirstLastTest {

    private SameFirstLast same;

    @Before
    public void setUp() throws Exception {
        same = new SameFirstLast();
    }


    @Test
    public void IsItTheSameIfEmpty() {
        int[] nums_one = { };
        boolean test_one = same.isItTheSame(nums_one);
        assertEquals(false, test_one);
    }

    @Test
    public void isItNotTheSame() {
        int[] nums_three = {1,2,34,4,32,5,76,4,233,9};
        boolean test_three = same.isItTheSame(nums_three);
        assertEquals(false, test_three);
    }

    @Test
    public void isItTheSame() {
        int[] nums_two = {1,2,1};
        boolean test_two = same.isItTheSame(nums_two);
        assertEquals(true, test_two);
    }
}