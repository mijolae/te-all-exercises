package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringBitsTest {

    private StringBits stringBitsTest;

    @Before
    public void setUp() throws Exception {
        stringBitsTest = new StringBits();
    }

    @Test
    public void getBitsOfEmptyString() {
        String test_two = stringBitsTest.getBits("");
        assertEquals("", test_two);
    }

    @Test
    public void getBitsOfCapatalizedWord() {
        String test_three = stringBitsTest.getBits("Mijolae");
        assertEquals("Mjle", test_three);
    }

    @Test
    public void getBits() {
        String test_one = stringBitsTest.getBits("abcdefg");
        assertEquals("aceg", test_one);
    }
}