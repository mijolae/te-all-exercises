package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class WordCountTest {

    private WordCount wordCountTest;
    /*
     * getCount() → {"ba" : 2, "black": 1, "sheep":
     * 1 } getCount(["a", "b", "a", "c", "b"]) → {"b": 2, "c": 1, "a": 2}
     * getCount([]) → {} getCount(["c", "b", "a"]) → {"b": 1, "c": 1, "a": 1}
     */

    @Before
    public void setUp() throws Exception {
        wordCountTest = new WordCount();
    }

    @Test
    public void getCount() {
        String[] testArray = new String[] {"ba", "ba", "black", "sheep"};
        Map<String, Integer> test = wordCountTest.getCount(testArray);
        Map<String, Integer> testResult = new HashMap<String, Integer>()
        {{put("ba", 2); put("black", 1); put("sheep", 1);}};
        assertEquals(testResult, test);

    }

    @Test
    public void getCountWithEmptyString() {
        String[] testArray = new String[] {"beach", "Mijolae", "Mijolae", ""};
        Map<String, Integer> test = wordCountTest.getCount(testArray);
        Map<String, Integer> testResult = new HashMap<String, Integer>()
        {{put("Mijolae", 2); put("beach", 1); put("", 1);}};
        assertEquals(testResult, test);
    }

    @Test
    public void getCountOfNonRepeatedStrings() {
        String[] testArray = new String[] {"ba", "ab", "black", "sheep"};
        Map<String, Integer> test = wordCountTest.getCount(testArray);
        Map<String, Integer> testResult = new HashMap<String, Integer>()
        {{put("ba", 1); put("ab", 1); put("black", 1); put("sheep", 1);}};
        assertEquals(testResult, test);
    }


}