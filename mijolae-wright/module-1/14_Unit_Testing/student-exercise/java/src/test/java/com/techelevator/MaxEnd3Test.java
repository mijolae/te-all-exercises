package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaxEnd3Test {

    private MaxEnd3 maxEnd;

    @Before
    public void setUp() throws Exception {
        maxEnd = new MaxEnd3();
    }

    @Test
    public void makeArrayOfTwos() {
        int[] nums_two = {0,1,2};
        int[] test_two = maxEnd.makeArray(nums_two);
        assertEquals(test_two, test_two);
    }

    @Test
    public void makeArrayOfSevens() {
        int[] nums_five = {5,7,3};
        int[] test_five = maxEnd.makeArray(nums_five);
        assertEquals(test_five, test_five);
    }

    @Test
    public void makeArrayOfSixes() {
        int[] nums_four = {2,4,6};
        int[] test_four = maxEnd.makeArray(nums_four);
        assertEquals(test_four, test_four);
    }

    @Test
    public void makeArrayOfNegatives() {
        int[] nums_three = {-3, -1, -7};
        int[] test_three = maxEnd.makeArray(nums_three);
        assertEquals(test_three, test_three);
    }

    @Test
    public void makeArray() {
        int[] nums_one = {4,5,6};
        int[] test_one = maxEnd.makeArray(nums_one);
        assertEquals(test_one, test_one);
    }
}