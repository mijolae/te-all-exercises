package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Lucky13Test {

    private Lucky13 luckyTest;

    @Before
    public void setUp() throws Exception {
        luckyTest = new Lucky13();
    }

    @Test
    public void getUnluckyWithThree() {
        int[] nums_five = {5,7,3};
        boolean test_five = luckyTest.getLucky(nums_five);
        assertEquals(false, test_five);
    }

    @Test
    public void getLuckyWithEvens() {
        int[] nums_four = {2,4,6};
        boolean test_four = luckyTest.getLucky(nums_four);
        assertEquals(true, test_four);
    }

    @Test
    public void getLuckyWithNegatives() {
        int[] nums_three = {-3, -1, -7};
        boolean test_three = luckyTest.getLucky(nums_three);
        assertEquals(true, test_three);
    }

    @Test
    public void getUnluckyWithOne() {
        int[] nums_two = {0,1,2};
        boolean test_two = luckyTest.getLucky(nums_two);
        assertEquals(false, test_two);
    }

    @Test
    public void getLucky() {
        int[] nums_one = {4,5,6};
        boolean test_one = luckyTest.getLucky(nums_one);
        assertEquals(true, test_one);
    }
}