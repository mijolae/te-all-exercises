package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CigarPartyTest {
    private CigarParty partyTime;

    @Before
    public void setUp() throws Exception {
        partyTime = new CigarParty();
    }

    @Test
    public void tooManyCigarsOnaWeekday() {
        boolean test_two = partyTime.haveParty(68, false);
        assertEquals(false, test_two);
    }

    @Test
    public void aLotOfCigarsOnAWeekend() {
        boolean test_four = partyTime.haveParty(72,true);
        assertEquals(true, test_four);
    }

    @Test
    public void noCigarsMeansNoParty() {
        boolean test_six = partyTime.haveParty(0,true);
        assertEquals(false, test_six);
    }

    @Test
    public void haveParty() {
        boolean test_one = partyTime.haveParty(45, true);
        assertEquals(true, test_one);
    }
}