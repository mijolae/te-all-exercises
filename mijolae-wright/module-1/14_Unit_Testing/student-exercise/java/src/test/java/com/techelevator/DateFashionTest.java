package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class DateFashionTest {

    private DateFashion dateTest;

    @Before
    public void setUp() throws Exception {
        dateTest = new DateFashion();
    }

    @Test
    public void twoMeansNoTable() {
        int test_two = dateTest.getATable(2,8);
        assertEquals(0,test_two);
    }

    @Test
    public void midLevelMightGetTable() {
        int test_five = dateTest.getATable(5,6);
        assertEquals(1,test_five);
    }

    @Test
    public void eightGetsATable() {
        int test_seven = dateTest.getATable(7,8);
        assertEquals(2,test_seven);
    }

    @Test
    public void getATable() {
        int test_nine = dateTest.getATable(4,10);
        assertEquals(2,test_nine);
    }
}