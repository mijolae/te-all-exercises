package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Less20Test {

    private Less20 lessTwentyTest;

    @Before
    public void setUp() throws Exception {
        lessTwentyTest = new Less20();
    }

    @Test
    public void isLessThanAMultipleOf20ByOne() {
        boolean test_two = lessTwentyTest.isLessThanMultipleOf20(19);
        assertEquals(true, test_two);
    }

    @Test
    public void isAMultipleOfTwenty() {
        boolean test_three = lessTwentyTest.isLessThanMultipleOf20(20);
        assertEquals(false, test_three);
    }

    @Test
    public void isLessThanAHigherMultipleofTwenty() {
        boolean test_four = lessTwentyTest.isLessThanMultipleOf20(39);
        assertEquals(true, test_four);
    }

    @Test
    public void isLessThanAMultipleOf80() {
        boolean test_five = lessTwentyTest.isLessThanMultipleOf20(78);
        assertEquals(true,test_five);
    }

    @Test
    public void isLessThanMultipleOf20() {
        boolean test_one = lessTwentyTest.isLessThanMultipleOf20(18);
        assertEquals(true, test_one);
    }
}