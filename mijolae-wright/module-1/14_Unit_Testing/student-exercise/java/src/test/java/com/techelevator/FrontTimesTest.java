package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FrontTimesTest {

    private FrontTimes frontTimesTest;

    @Before
    public void setUp() throws Exception {
        frontTimesTest = new FrontTimes();
    }

    @Test
    public void returnMultipleAb() {
        String test_three = frontTimesTest.generateString("ab", 3);
        assertEquals("ababab", test_three);
    }

    @Test
    public void returnMultiplePartsOfMyName() {
        String test_five = frontTimesTest.generateString("mijolae", 5);
        assertEquals("mijmijmijmijmij", test_five);
    }

    @Test
    public void returnMultipleAbc() {
        String test_four = frontTimesTest.generateString("abc", 4);
        assertEquals("abcabcabcabc", test_four);
    }

    @Test
    public void fourS() {
        String test_two = frontTimesTest.generateString("s", 4);
        assertEquals("ssss", test_two);
    }

    @Test
    public void generateString() {
        String test_one = frontTimesTest.generateString("", 5);
        assertEquals("", test_one);
    }
}