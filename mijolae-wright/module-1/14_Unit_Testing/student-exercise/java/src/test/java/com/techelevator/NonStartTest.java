package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NonStartTest {

    private NonStart nonStartTest;

    @Before
    public void setUp() throws Exception {
        nonStartTest = new NonStart();
    }

    @Test
    public void getPartialStringOfDifferentSizedWords() {
        String test_two = nonStartTest.getPartialString("Chocolate","bear");
        assertEquals("hocolateear", test_two);
    }

    @Test
    public void getPartialStringWhenOneIsEmpty() {
        String test_three = nonStartTest.getPartialString("","ear");
        assertEquals("ar", test_three);
    }

    @Test
    public void getPartialString() {
        String test_one = nonStartTest.getPartialString("Hello", "There");
        assertEquals("ellohere", test_one);
    }
}