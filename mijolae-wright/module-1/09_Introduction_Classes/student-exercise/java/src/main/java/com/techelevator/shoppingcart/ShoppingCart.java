package com.techelevator.shoppingcart;

public class ShoppingCart {

	private int totalNumberOfItems = 0;
	private double totalAmountOwned = 0.0d;

	public ShoppingCart(){

    }

    public int getTotalNumberOfItems() {
        return totalNumberOfItems;
    }

    public double getTotalAmountOwed() {
        return totalAmountOwned;
    }

    public double getAveragePricePerItem(){
	    if(totalNumberOfItems == 0){
	        return 0.0d;
        }
	    double value = (double)totalAmountOwned / totalNumberOfItems;
	    return value;
    }

    public void addItems(int numberOfItems, double pricePerItem){
	    totalNumberOfItems += numberOfItems;
	    totalAmountOwned += (pricePerItem * numberOfItems);
    }

    public void empty(){
	    totalNumberOfItems = 0;
	    totalAmountOwned = 0.0d;
    }

}
