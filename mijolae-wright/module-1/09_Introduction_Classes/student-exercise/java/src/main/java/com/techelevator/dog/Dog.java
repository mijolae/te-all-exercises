package com.techelevator.dog;

public class Dog {

	private boolean isSleeping = false;

	public Dog(){

    }

    public Dog(boolean isSleeping){
	    this.isSleeping = isSleeping;
    }

    public boolean isSleeping() {
        return isSleeping;
    }

    public String makeSound(){
	    if(isSleeping){
	        return "Zzzzz...";
        }
	    return "Woof!";
    }

    public void sleep(){
	    isSleeping = true;
    }

    public void wakeUp(){
	    isSleeping = false;
    }
}
