package com.techelevator.entity;

public class Customer {
    /*
        Define fields of the class - usually set their scope to private (only the methods take this class)
     */

    private String name;
    private String email;

    /*
        Defining constructors in our class - always the same name as class, no return value and unique argument list
        - if more than 1 example of method overloading
     */

    public Customer(){ // default or no argument constructor - usually you want this in your class

    }

    public Customer(String name, String email){
        this.name = name;
        this.email = email;
    }
    /*
        methods - setter and getter
     */

    public String getName(){
        return name;
    }

    public String getEmail(){
        return email;
    }

    public void setName(String name){
        //protected the field if you want.. check the name passed in is not null
        if(!(name.equals(null))){
            this.name = name;
        }

    }


}
