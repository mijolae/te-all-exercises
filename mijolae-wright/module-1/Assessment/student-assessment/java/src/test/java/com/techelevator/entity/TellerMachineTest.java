package com.techelevator.entity;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TellerMachineTest {

    private TellerMachine tellerMachineTest;
    private BigDecimal bigDecimalOfZero;

    @Before
    public void setUp() throws Exception {
        tellerMachineTest = new TellerMachine("Wright Corporation", "650.00","320.74");
        bigDecimalOfZero = new BigDecimal("0.00");
    }

    @Test
    public void getBalance() {
        assertEquals(BigDecimal.valueOf(329.26), tellerMachineTest.getBalance());
    }

    @Test
    public void getBalanceOfEmptyTellerMachine() {
        TellerMachine emptyTellerMachine = new TellerMachine();
        assertEquals(bigDecimalOfZero, emptyTellerMachine.getBalance());
    }

    @Test
    public void balanceDoesNotGoPastZero() {
        TellerMachine lowBalance = new TellerMachine("Wrong Corporation", "100.00", "102.34");
        assertEquals(bigDecimalOfZero, lowBalance.getBalance());
    }

    @Test
    public void isValidCard() {
        String cardTestOne = "5638472840957368"; //true - starts with five and has 16
        assertEquals(true, tellerMachineTest.isValidCard(cardTestOne));
    }

    @Test
    public void notValidCardWithWrongNumberOfDigits() {
        String cardTestTwo = "45769302847562"; //false - wrong number of digits
        assertEquals(false, tellerMachineTest.isValidCard(cardTestTwo));
    }

    @Test
    public void isValidCardWith13Digits() {
        String cardTestThree = "4576930284756"; //true - correct number of digits 13
        assertEquals(true,tellerMachineTest.isValidCard(cardTestThree));
    }

    @Test
    public void isValidCardWith16Digits() {
        String cardTestFour = "4372910485937294"; //true - correct number of digits 16
        assertEquals(true, tellerMachineTest.isValidCard(cardTestFour));
    }

    @Test
    public void isValidCardWith3Then4() {
        String cardTestFive = "34586092030985098"; //true - starts with 3 and second digit is 4
        assertEquals(true, tellerMachineTest.isValidCard(cardTestFive));
    }

    @Test
    public void isValidCardWith3Then7() {
        String cardTestSix = "3749540298173930"; //true - starts with 3 and second number is 7
        assertEquals(true, tellerMachineTest.isValidCard(cardTestSix));
    }

    @Test
    public void isNotAValidCardIfSecondNumberIsNot4Or7() {
        String cardTestSeven = "358495645794"; //false - starts with 3 but has a different second number
        assertEquals(false, tellerMachineTest.isValidCard(cardTestSeven));
    }
}