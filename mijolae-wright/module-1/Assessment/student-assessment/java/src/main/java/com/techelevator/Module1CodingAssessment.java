package com.techelevator;

import com.techelevator.entity.TellerMachine;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Module1CodingAssessment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BigDecimal endBalance = new BigDecimal("0.00");

		//Test at #4 in Teller-Machine.md
		TellerMachine initialTest = new TellerMachine("Wright Corporation",
				"750.00","325.00");


		System.out.println("This is the manufacturer: " + initialTest.getManufacturer());
		System.out.println("This is the initial deposit: " + initialTest.getDeposits());
		System.out.println("This is the amount to withdraw: " + initialTest.getWithdrawals());
		System.out.println("This is the current balance of the account: " + initialTest.getBalance());

		//Not sure where to put isValidCard method so I put it within the TellerMachine
		//Reading in file
		File tellerMachineFile = new File("data-files\\TellerInput.csv");
		List<TellerMachine> allTellerMachines = new ArrayList<>();

		try(Scanner scan = new Scanner(tellerMachineFile)){
			while(scan.hasNext()){
				String tellerInformation = scan.nextLine();
				String[] parts = tellerInformation.split(",");
				TellerMachine newTellerObject = new TellerMachine(parts[0], parts[1], parts[2]);
				allTellerMachines.add(newTellerObject);
			}
		}catch (FileNotFoundException exception){
			System.out.println("File not found.");
			System.exit(1);
		}

		//Adding up total Balance
		BigDecimal totalBalance = BigDecimal.ZERO;
		for(TellerMachine value : allTellerMachines){
			totalBalance = totalBalance.add(value.getBalance());
		}

		//Here, I had an issue with the for loop saying there was an object and not a TellerMachine
		System.out.println("Total sum of balances is: " + totalBalance);

	}

}
