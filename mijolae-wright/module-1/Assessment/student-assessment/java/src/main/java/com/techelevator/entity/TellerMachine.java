package com.techelevator.entity;

import java.math.BigDecimal;

public class TellerMachine {
    private String manufacturer;
    private BigDecimal deposits;
    private BigDecimal withdrawals;
    private BigDecimal balance;

    public TellerMachine() {
        this.deposits = new BigDecimal("0.00");
        this.withdrawals = new BigDecimal("0.00");
        this.balance = deposits.subtract(withdrawals);
    }

    public TellerMachine(String manufacturer, String deposits, String withdrawals) {
        this.manufacturer = manufacturer;
        this.deposits = new BigDecimal(String.valueOf(deposits));
        this.withdrawals = new BigDecimal(String.valueOf(withdrawals));
        balance = this.deposits.subtract(this.withdrawals);
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public BigDecimal getDeposits() {
        return deposits;
    }

    public BigDecimal getWithdrawals() {
        return withdrawals;
    }

    public BigDecimal getBalance() {
        /*if(balance.compareTo(new BigDecimal("0.00")) == -1){
            return new BigDecimal("0.00");
        }*/
        return balance;
    }

    public boolean isValidCard(String cardNumber){
        String firstNumber = cardNumber.substring(0,1);
        int cardLength = cardNumber.length();
        String secondNumber = cardNumber.substring(1,2);

        if(firstNumber.equals("5") && cardLength == 16){
            return true;
        }else if (firstNumber.equals("4") && (cardLength == 13 || cardLength == 16)){
            return true;
        }else if (firstNumber.equals("3") && (secondNumber.equals("4") || secondNumber.equals("7"))){
            return true;
        }

        return false;
    }

    public String toString(){
        return "ATM - " + manufacturer + " - " + balance;
    }
}
