package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("What is the file that should be searched? ");
		String fileName = scan.nextLine();
		System.out.println("What is the search word you are looking for?");
		String word = scan.nextLine();
		System.out.println("Should the search be case sensitive? (Y/N)");
		String caseSensitive = scan.nextLine();


		File file = new File(fileName);

		try(Scanner input = new Scanner(file)){
			int lineNumber = 1;
			while(input.hasNext()){
				String nextLine = input.nextLine();
				String originalCase = nextLine;
				if(caseSensitive.equalsIgnoreCase("n")){
					nextLine = nextLine.toLowerCase();
					word = word.toLowerCase();
				}
				if(nextLine.contains(word)){
					System.out.println(lineNumber +") " + originalCase);
				}
				lineNumber++;
			}
		}catch (FileNotFoundException ex){
			System.out.println("The file could not be found.");
			System.exit(1);
		}

	}

}
