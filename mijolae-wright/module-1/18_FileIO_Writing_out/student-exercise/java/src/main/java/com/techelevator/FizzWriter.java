package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class FizzWriter {

	public static void main(String[] args) {
		File file = newFizzBuzzFile();
		File finalFile = fizzBuzzMethod(file, Integer.parseInt(args[0]));
		System.out.println("FizzBuzz.txt has been created.");
	}

	//Check the java folder for the FizzBuzz.txt file

	public static File newFizzBuzzFile(){
		File filetoCreate = new File("FizzBuzz.txt");
		try {
			if (!filetoCreate.exists()) {
				filetoCreate.createNewFile();
			} else if (!filetoCreate.isFile()) {
				System.out.println("FizzBuzz is not a file.");
				System.exit(1);
			}
		} catch (FileNotFoundException exception){
			System.out.println("File not found.");
			System.exit(1);
		} catch (IOException exception){
			System.out.println("IO exception found.");
			System.exit(1);
		}

		return filetoCreate;
	}

	private static File fizzBuzzMethod(File outputFile, int numberOfIterations){
		try(PrintWriter file = new PrintWriter(outputFile);){
			for(int i = 1; i < numberOfIterations + 1; i++){
				boolean containsThree = (i / 10 == 3 || i % 10 == 3);
				boolean containsFive = (i / 10 == 5 || i % 10 == 5);
				if(i % 3 == 0 && i % 5 == 0){
					file.println("FizzBuzz");
				} else if (i % 3 == 0 || containsThree){
					file.println("Fizz");
				} else if (i % 5 == 0 || containsFive){
					file.println("Buzz");
				}else {
					file.println(i);
				}
			}
		} catch (FileNotFoundException exception){
			System.out.println("File not found.");
			System.out.println(1);
		}
		return outputFile;
	}

}
