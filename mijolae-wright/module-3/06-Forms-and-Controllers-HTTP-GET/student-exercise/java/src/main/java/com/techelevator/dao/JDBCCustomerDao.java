package com.techelevator.dao;

import com.techelevator.dao.model.Customer;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * JDBCCustomerDao
 */
@Component
public class JDBCCustomerDao implements CustomerDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCCustomerDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Customer> searchAndSortCustomers(String search, String sort) {
        List<Customer> matchingCustomers = new ArrayList<>();
        String sqlSearch = "SELECT first_name, last_name, email, activebool"+
                        " FROM customer"+
                        " WHERE last_name ILIKE ?"+
                        " OR first_name ILIKE ?"+
                        " order by " + sort;
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearch, "%" + search + "%","%" + search + "%");
        while (results.next()) {
            matchingCustomers.add(mapRowToCustomer(results));
        }
        return matchingCustomers;
    }

    private Customer mapRowToCustomer(SqlRowSet result){
        Customer customer = new Customer(result.getString("first_name"),
                result.getString("last_name"),
                result.getString("email"),
                result.getBoolean("activebool"));

        return customer;
    }
}