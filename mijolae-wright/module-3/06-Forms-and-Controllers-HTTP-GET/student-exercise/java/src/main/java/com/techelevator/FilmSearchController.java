package com.techelevator;

import com.techelevator.dao.FilmDao;
import com.techelevator.dao.model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * FilmSearchController
 */
@Controller
public class FilmSearchController {

    @Autowired
    FilmDao filmDao;

    @RequestMapping("/searchFilmsForm")
    public String showFilmSearchForm() {
        return "filmList";
    }

    @RequestMapping("/searchFilms")
    public String searchFilms(@RequestParam String category,
                              @RequestParam int minLength,
                              @RequestParam int maxLength,
                              ModelMap modelMap) {
        List<Film> films = filmDao.getFilmsBetween(category, minLength, maxLength);

        modelMap.put("films", films);

        return "filmList";
    }

}