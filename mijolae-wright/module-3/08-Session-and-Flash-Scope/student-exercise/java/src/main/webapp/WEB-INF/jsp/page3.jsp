<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp">
    <c:param name="title" value="Favorite Food" />
</c:import>

<form method="POST" action = "Page3">
    <label>What is your favorite food?</label>
    <label class="radio-inline">
        <input type="radio" name="favoriteSeason" value = "Spring" checked>Spring
        <input type="radio" name="favoriteSeason" value = "Summer">Summer
        <input type="radio" name="favoriteSeason" value = "Fall">Fall
        <input type="radio" name="favoriteSeason" value = "Winter">Winter
    </label>
    <button type="submit">Next >>></button>
</form>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />
