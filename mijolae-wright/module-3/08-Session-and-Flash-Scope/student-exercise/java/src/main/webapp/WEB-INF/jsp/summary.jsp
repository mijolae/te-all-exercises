<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp">
    <c:param name="subTitle">Favorite Things Summary</c:param>
</c:import>

<h2>Summary</h2>

<div class='summaryBlock'>
    <p class='summaryElement'>
        <b>Favorite Color:</b> ${favorites.color}
    </p>

    <p class='summaryElement'>
        <b>Favorite Food:</b> ${favorites.food}
    </p>

    <p class='summaryElement'>
        <b>Favorite Season:</b> ${favorites.season}
    </p>

</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />
