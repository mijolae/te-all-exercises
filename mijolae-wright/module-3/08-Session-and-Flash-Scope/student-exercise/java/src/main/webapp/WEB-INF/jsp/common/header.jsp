<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Favorite Things
        <c:if test="${param.subTitle != null}"> - <c:out value="${param.subTitle}"/></c:if>
    </title>
    <c:url var="stylesheetHref" value="/css/styles.css" />
    <link rel="stylesheet" href="${stylesheetHref}">
</head>
<body>
<h1>Favorite Things Exercise</h1>