<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <title>Hello Spring MVC</title>
    <link rel="stylesheet" href="webapp/site.css">
</head>
<body>
<div id="main_content">
    <h1>Please enter your review</h1>
    <form action="reviewInput" method="POST">
        <div class="formGroup">
            <label for="username">Username:</label>
            <input type="text" name="username" id="username" required/>
        </div>
        <div class="formGroup">
            <label for="rating">Rating:</label>
            <input type="text" name="rating" id="rating" />
        </div>
        <div class="formGroup">
            <label for="title">Title: </label>
            <input type="text" name="title" id="title" />
        </div>
        <div class="formGroup">
            <label for="text">Text: </label>
            <input type="text" name="text" id="text" />
        </div>
        <div class="formGroup">
            <input type="submit" value="Submit Review" />
        </div>
    </form>
</div>
</body>
</html>