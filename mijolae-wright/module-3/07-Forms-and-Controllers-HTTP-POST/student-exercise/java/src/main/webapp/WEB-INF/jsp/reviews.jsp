<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Hello Spring MVC</title>
	</head>
	<body>
	<p><a href="reviewInput">Add a review</a>!</p>
		<h1>Welcome to our review page!</h1>
		<table class="table">
			<tr>
				<th>Username</th>
				<th>Rating</th>
				<th>Title</th>
				<th>Date Submitted</th>
				<th>Text</th>
			</tr>
			<c:forEach items="${allReviews}" var="review">
				<tr>
					<td>${review.username}</td>
					<td>
							<c:forEach begin="1" end="5" var="counter">
								<c:if test="${review.rating >= counter}">
									<img src ="star.png">
								</c:if>
							</c:forEach>
					</td>
					<td>${review.title}</td>
					<td>${localDateTimeFormat.format(review.dateSubmitted)}</td>
					<td>${review.text}</td>
				</tr>
			</c:forEach>
		</table>

	</body>
</html>