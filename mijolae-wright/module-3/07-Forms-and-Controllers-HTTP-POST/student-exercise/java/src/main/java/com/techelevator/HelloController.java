package com.techelevator;

import javax.servlet.http.HttpSession;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class HelloController {

	@Autowired
	private ReviewDao reviewDao;

	@RequestMapping("/")
	public String displayGreeting(ModelMap modelMap) {
		List<Review> reviewList = reviewDao.getAllReviews();
		modelMap.addAttribute("localDateTimeFormat", DateTimeFormatter.ofPattern("dd/MM/yyyy"));

		modelMap.put("allReviews", reviewList);

		return "reviews";
	}
}
