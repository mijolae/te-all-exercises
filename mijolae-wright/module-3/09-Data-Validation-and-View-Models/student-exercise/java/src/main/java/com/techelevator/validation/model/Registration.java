package com.techelevator.validation.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Date;

public class Registration {
    @NotBlank(message = "First name is required")
    @Size(max = 20, message = "First name must be less than 20 characters")
    private String firstName;

    @NotBlank(message = "Last name is required")
    @Size(max = 20, message = "Last name must be less than 20 characters")
    private String lastName;

    @NotBlank(message = "Email is required")
    @Email(message = "Enter a valid email")
    private String email;

    @AssertTrue(message = "Emails must match")
    public boolean doEmailsMatch(){
        if (email != null){
            return (email.equals(confirmEmail));
        }
        return true;
    }
    private String confirmEmail;

    @Size(min = 8, message = "Password must be 8 characters")
    @NotBlank(message = "Password is required")
    private String password;

    @AssertTrue(message = "Passwords must match")
    public boolean doPasswordsMatch(){
        if (password != null){
            return (password.equals(confirmPassword));
        }
        return true;
    }
    private String confirmPassword;

    @NotNull(message = "Required")
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private LocalDate birthDate;

    @NotNull(message = "At least 1 ticket is required")
    @Min(value=1,message = "At least 1 ticket is required")
    @Max(value=10, message = "Max number of tickets is 10.")
    private Integer numOfTickets;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getNumOfTickets() {
        return numOfTickets;
    }

    public void setNumOfTickets(Integer numOfTickets) {
        this.numOfTickets = numOfTickets;
    }
}
