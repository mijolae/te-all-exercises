<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html>
<head>
    <title>Registration Page</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<c:set var="pageTitle" value="New User Registration"/>
<%@include file="common/header.jspf" %>

<h1>New User Registration</h1>

<c:url var="Registration" value="/register"/>
<form:form method="POST" action="${Registration}" modelAttribute="register">
    <div>
        <label for="firstName">First Name:</label>
        <form:input path="firstName"/>
        <form:errors path="firstName" cssClass="error"/>
    </div>
    <div>
        <label for="lastName">Last Name:</label>
        <form:input path="lastName"/>
        <form:errors path="lastName" cssClass="error"/>
    </div>
    <div>
        <label for="email">Email</label>
        <form:input path="email"/>
        <form:errors path="email" cssClass="error"/>
    </div>
    <div>
        <label for="confirmEmail">Confirm Email</label>
        <form:input path="confirmEmail"/>
        <form:errors path="confirmEmail" cssClass="error"/>
    </div>
    <div>
        <label for="password">Password</label>
        <form:input path="password" type="password"/>
        <form:errors path="password" cssClass="error"/>
    </div>
    <div>
            <label for="confirmPassword">Confirm Password</label>
        <form:input path="confirmPassword" type="password"/>
        <form:errors path="confirmPassword" cssClass="error"/>
    </div>
    <div>
        <label for="birthDate">Birth Date</label>
        <form:input path="birthDate"/>
        <form:errors path="birthDate" cssClass="error"/>
    </div>
    <div>
        <label for="numOfTickets">Number of Tickets</label>
        <form:input path="numOfTickets"/>
        <form:errors path="numOfTickets" cssClass="error"/>
    </div>

    <div>
        <input type="submit" value="Submit"/>
    </div>
</form:form>
</body>
</html>
