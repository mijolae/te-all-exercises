<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<c:set var="pageTitle" value="Login"/>
<%@include file="common/header.jspf" %>

<h1>Login</h1>

<c:url var="Login" value="/login"/>
<form:form method="POST" action="${Login}" modelAttribute="login">
    <div>
        <label for="email">Email</label>
        <form:input path="email"/>
        <form:errors path="email" cssClass="error"/>
    </div>
    <div>
        <label for="password">Password</label>
        <form:input path="password" type="password"/>
        <form:errors path="password" cssClass="error"/>
    </div>

    <div>
        <input type="submit" value="Submit"/>
    </div>
</form:form>
</body>
</html>