<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<c:set var="pageTitle" value="Login"/>
<%@include file="common/header.jspf" %>

<h1>Confirmation</h1>
<p>${message}</p>

<%@include file="common/footer.jspf" %>
</body>


</html>