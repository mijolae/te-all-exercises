<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="/WEB-INF/jsp/common/header.jsp">
    <c:param name="pageTitle" value="Product Tile View"/>
</c:import>

<div id="masonry-page">
    <c:import url="/WEB-INF/jsp/common/filters.jsp">
        <%-- Modify the baseRoute to apply filters to the current route. --%>
        <c:param name="baseRoute" value="/products/tiles/"/>
    </c:import>

    <!-- Container for all of the Products -->
    <!-- The list of products is available using the `products` variable -->
    <div id="grid" class="main-content">

        <!-- The following HTML shows different examples of what HTML
         could be rendered based on different rules. For purposes
         of demonstration we've written it out so you can see it
         when you load the page up. -->
        <c:forEach var="product" items="${requestScope.products}">

            <!-- Standard Product -->
            <div class="tile ${(product.remainingStock == 0) ? "sold-out":""} ${(product.topSeller == true) ? "top-seller" : ""}">

				<c:if test="${product.remainingStock == 0}">
					<span class="banner">Sold Out</span>
				</c:if>
				<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
				<c:url var="productPostHref" value="/products/detail">
					<c:param name="id" value="${product.id}"/>
				</c:url>
				<a class="product-image" href="${productPostHref}">
					<img src="<c:url value="/images/product-images/${product.imageName}" />"/>
				</a>

				<div class="details">
					<p class="name">
						<a href="${productPostHref}">${product.name}</a>
					</p>

					<!-- .filled will make the star solid -->
					<div class="rating">
						<c:forEach begin="1" end="5" var="counter">
							<span class="${Math.round(product.averageRating) >= counter ? 'filled': '' }">&#9734;</span>
						</c:forEach>
					</div>

					<c:if test="${product.topSeller == true}">
						<p class="product-alert">Top Seller</p>
					</c:if>

					<c:if test="${product.remainingStock > 0 && product.remainingStock <= 5}">
						<p class="product-alert">${product.remainingStock} remaining!</p>
					</c:if>

					<p class="price">$${product.price}</p>
				</div>
			</div>
		</c:forEach>
	</div>
</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp"/>