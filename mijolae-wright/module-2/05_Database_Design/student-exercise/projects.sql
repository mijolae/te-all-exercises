drop table project_employees;
drop table project;
drop table employee;
drop table department;

BEGIN TRANSACTION;

CREATE TABLE employee
(
	employeeId serial,
	first_name varchar(64) not null,
	last_name varchar(64) not null,
	job_title varchar(64) not null,
	gender varchar(64) not null,
	birth_date date,
	hire_date date,
	departmentId int not null,
	

	constraint pk_employee primary key (employeeId),
	constraint ck_gender check(gender in ('male','female'))

);

CREATE TABLE department
(
	departmentId serial,
	name varchar(64) not null,

	constraint pk_department primary key (departmentId)
);

CREATE TABLE project
(
	projectId serial,
	name varchar(64) not null,
	start_date date not null,

	constraint pk_project primary key (projectId)

);

CREATE TABLE project_employees
(
	projectId serial,
	employeeId int not null,

	constraint pk_project_employees primary key (projectId, employeeID),
	constraint fk_project_employees_projects foreign key (projectId) references project (projectId),
	constraint fk_project_employees_employees foreign key (employeeId) references employee (employeeId)
);

--alter table
alter table employee add constraint fk_employee_department foreign key (departmentId) references department (departmentId);


COMMIT TRANSACTION;

insert into department(name)
values ('Cool Kids'),('Life-Changers'),('Heartbreakers');

insert into employee(first_name,last_name,job_title,gender,birth_date,departmentid)
Values ('Cole','Besterfield','Software Engineer','male','1997-02-16',2),
        ('Clinton','Asalu','Software Engineer','male','1997-11-01',2),
        ('Heather','Marrero','Software Engineer','female','1997-04-24',1),
        ('Miriam','Bursky-Tammam','Software Engineer','female','1996-07-07',1),
        ('Naval','Ravikant','Angel Investor','male','1974-08-17',2),
        ('James','Clear','Atomic Guy','male','1981-10-26',2),
        ('Dua','Lipa','Heart Breaker','female','1993-04-23',3),
        ('Mijolae','Wright','The Baddest To Do It','female','1998-02-08',3);
        
insert into project(name, start_date)
values ('Beat The Green Team', '2020-06-15'),
        ('Work Hard, Get Money', '2020-06-30'),
        ('Revamp Instagram', '2020-07-12'),
        ('TikTok Takeover', '2020-01-01');
        
insert into project_employees(projectId, employeeId)
values (1,7), (1,3),(2,2),(2,5),(2,4),(3,1),(4,6),(4,8);
        

