-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
-- (30 rows)
select title
from film
join film_actor on film.film_id = film_actor.film_id
join actor on film_actor.actor_id = actor.actor_id
where actor.last_name = 'STALLONE';

-- 2. All of the films that Rita Reynolds has appeared in
-- (20 rows)
select title
from film
join film_actor on film.film_id = film_actor.film_id
join actor on film_actor.actor_id = actor.actor_id
where actor.last_name = 'REYNOLDS';

-- 3. All of the films that Judy Dean or River Dean have appeared in
-- (46 rows)
select title
from film
join film_actor on film.film_id = film_actor.film_id
join actor on film_actor.actor_id = actor.actor_id
where (actor.first_name = 'JUDY' AND actor.last_name = 'DEAN') OR (actor.first_name = 'RIVER' AND actor.last_name = 'DEAN');

-- 4. All of the the ‘Documentary’ films
-- (68 rows)

select title
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
where category.name = 'Documentary';

-- 5. All of the ‘Comedy’ films
-- (58 rows)
select title
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
where category.name = 'Comedy';

-- 6. All of the ‘Children’ films that are rated ‘G’
-- (10 rows)
select title
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
where category.name = 'Children' AND film.rating = 'G';

-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
-- (3 rows)
select title
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
where category.name = 'Family' AND film.rating = 'G' AND film.length <= 120;

-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
-- (9 rows)
select title
from film
join film_actor on film.film_id = film_actor.film_id
join actor on film_actor.actor_id = actor.actor_id
where actor.first_name = 'MATTHEW' AND actor.last_name = 'LEIGH' AND film.rating = 'G';

-- 9. All of the ‘Sci-Fi’ films released in 2006
-- (61 rows)
select title 
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
where film.release_year = 2006 and category.name = 'Sci-Fi';

-- 10. All of the ‘Action’ films starring Nick Stallone
-- (2 rows)
select title 
from film
join film_category on film.film_id = film_category.film_id
join category on film_category.category_id = category.category_id
join film_actor on film.film_id = film_actor.film_id
join actor on actor.actor_id = film_actor.actor_id
where actor.first_name = 'NICK' and actor.last_name = 'STALLONE' and category.name = 'Action';

-- 11. The address of all stores, including street address, city, district, and country
-- (2 rows)
select address.address, city.city, address.district, country.country
from store
join address on store.address_id = address.address_id
join city on city.city_id = address.city_id
join country on city.country_id = country.country_id;

-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
-- (2 rows)
select s.store_id, address.address, (staff.first_name || ' ' || staff.last_name) as managerName
from store as s
join address on s.address_id = address.address_id
join staff on s.manager_staff_id = staff.staff_id;

-- 13. The first and last name of the top ten customers ranked by number of rentals 
-- (#1 should be “ELEANOR HUNT�? with 46 rentals, #10 should have 39 rentals)
select (customer.first_name || ' ' || customer.last_name) as customerName, count(rental.customer_id)
from customer
join rental on rental.customer_id = customer.customer_id
GROup by customerName
ORder by count(rental.customer_id) DESC
Limit 10;

-- 14. The first and last name of the top ten customers ranked by dollars spent 
-- (#1 should be “KARL SEAL�? with 221.55 spent, #10 should be “ANA BRADLEY�? with 174.66 spent)
select (customer.first_name || ' ' || customer.last_name) as customerName, sum(payment.amount)
from customer
join payment on payment.customer_id = customer.customer_id
GROup by customerName
ORder by sum(payment.amount) DESC
Limit 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store.
-- (NOTE: Keep in mind that an employee may work at multiple stores.)
-- (Store 1 has 7928 total rentals and Store 2 has 8121 total rentals)
select s.store_id, a.address, count(r.inventory_id), sum(p.amount), avg(p.amount)
from store as s
join address as a on a.address_id = s.address_id
join inventory as i on i.store_id = s.store_id
join rental as r on r.inventory_id = i.inventory_id
join payment as p on p.rental_id = r.rental_id
group by s.store_id, a.address;

-- 16. The top ten film titles by number of rentals
-- (#1 should be “BUCKET BROTHERHOOD�? with 34 rentals and #10 should have 31 rentals)
select film.title, count(r.inventory_id)
from film
join inventory as i on film.film_id = i.film_id
join rental as r on i.inventory_id = r.inventory_id
group by film.title
Order by count(r.inventory_id) DESC
Limit 10;

-- 17. The top five film categories by number of rentals 
-- (#1 should be “Sports�? with 1179 rentals and #5 should be “Family�? with 1096 rentals)
select category.name, count(r.inventory_id)
from category
join film_category as fc on fc.category_id = category.category_id
join film as f on f.film_id = fc.film_id
join inventory as i on i.film_id = f.film_id
join rental as r on i.inventory_id = r.inventory_id
group by category.name
order by count(r.inventory_id) DESC
Limit 5;

-- 18. The top five Action film titles by number of rentals 
-- (#1 should have 30 rentals and #5 should have 28 rentals)
select film.title, count(r.inventory_id)
from film
join film_category as fc on film.film_id = fc.film_id
join category as c on fc.category_id = c.category_id
join inventory as i on i.film_id = film.film_id
join rental as r on i.inventory_id = r.inventory_id
where c.name = 'Action'
group by film.title
order by count(r.inventory_id) DESC
Limit 5;

-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
-- (#1 should be “GINA DEGENERES�? with 753 rentals and #10 should be “SEAN GUINESS�? with 599 rentals)
select (actor.first_name || ' ' || actor.last_name) as actorName, count(r.*) as numberofrentals
from actor
join film_actor as fa on fa.actor_id = actor.actor_id
join film as f on f.film_id = fa.film_id
 join inventory as i on i.film_id = f.film_id
join rental as r on i.inventory_id = r.inventory_id
group by actor.actor_id
order by numberofrentals DESC
Limit 10;

-- 20. The top 5 “Comedy�? actors ranked by number of rentals of films in the “Comedy�? category starring that actor 
-- (#1 should have 87 rentals and #5 should have 72 rentals)
select (actor.first_name || ' ' || actor.last_name) as actorName, count(r.*) as numberofrentals
from actor
join film_actor as fa on fa.actor_id = actor.actor_id
join film as f on f.film_id = fa.film_id
join film_category as fc on fc.film_id = f.film_id
join category as c on fc.category_id = c.category_id
 join inventory as i on i.film_id = f.film_id
join rental as r on i.inventory_id = r.inventory_id
where c.name LIKE '%Comedy%'
group by actor.actor_id
order by numberofrentals DESC
Limit 5;