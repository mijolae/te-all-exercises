-- Select all columns from users where the user's role is admin
select *
From users
where role = 'admin';

-- Select name and description from items that were created after Sept. 20, 2019 and the description isn't null
select name, description
from items
where created >= '2019-09-20'
and description is not null;

-- Select first_name and last_name from users and order by when they were created, latest first
select first_name, last_name
from users
order by created desc;

-- Select finished and a count all the items that are finished/not finished
select finished, count(finished)
from items
group by finished;

-- Select a user's first_name and last_name and the item's name for every finished item
select u.first_name, u.last_name, i.name
from users as u
join items as i on i.user_id = u.id
where i.finished = true;