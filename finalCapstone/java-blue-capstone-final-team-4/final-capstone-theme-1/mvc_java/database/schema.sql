BEGIN;

-- CREATE statements go here
DROP TABLE IF EXISTS app_user, book, family, family_members, book_user, prize, book_family, record_reading CASCADE;

CREATE TABLE app_user (
  id SERIAL PRIMARY KEY,
  user_name varchar(32) NOT NULL UNIQUE,
  password varchar(32) NOT NULL,
  first_name varchar(32) NOT NULL,
  last_name varchar(32) NOT NULL,
  role varchar(32) NOT NULL,
  salt varchar(255) NOT NULL
  
);

CREATE TABLE book (
id SERIAL PRIMARY KEY,
title varchar(128) NOT NULL,
author varchar(32) NOT NULL,
isbn varchar(32) NOT NULL unique
);

CREATE TABLE book_user(
book_id bigint NOT NULL,
user_id bigint NOT NULL,

constraint fk_book_id foreign key (book_id) references book (id),
constraint fk_user_id foreign key (user_id) references app_user (id)
);

CREATE TABLE family (
id SERIAL,
family_name varchar(32) NOT NULL,

constraint pk_id primary key (id)
);

CREATE TABLE family_members (
family_id bigint NOT NULL,
user_id bigint NOT NULL,
constraint fk_family_id foreign key (family_id) references family(id),
constraint fk_user_id foreign key (user_id) references app_user(id)
);

CREATE Table prize (
prize_id serial NOT NULL,
prize_name varchar(64) NOT NULL, 
description varchar(64) NOT NULL,
minutes_required int NOT NULL,
user_group varchar(32) NOT NULL,
number_of_prizes int,
start_date date NOT NULL,
end_date date NOT NULL
);

CREATE TABLE record_reading
(
	recordReadingId serial primary key,
	userId int not null,
	firstName varchar(32) null,
	userName varchar(32) NULL,
	role varchar(32) not null,
	format varchar(32) not null,
	hoursRead int not null,
	minutesRead int not null,
	isbn varchar(32) not null,
	notes varchar(240),
	doneReading boolean NOT NULL,
	
	CONSTRAINT fk_userId FOREIGN KEY (userId) REFERENCES app_user (id),
        CONSTRAINT fk_isbn FOREIGN KEY (isbn) REFERENCES book (isbn),
        CONSTRAINT fk_userName FOREIGN KEY (userName) REFERENCES app_user (user_name)
        
        );


COMMIT;