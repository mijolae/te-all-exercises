--Schema for fake data
----------------------------------------------------------
INSERT INTO app_user(user_name,password,first_name,last_name,role,salt)
VALUES('mijolae@gmail.com','pass','mijolae','deen','parent','jdk'),
        ('mimiriff@gmail.com','pass','matt','adams','parent','jdk'),
        ('try@gmail.com','pass','desi','jayson','parent','jdk'),
        ('why@gmail.com','pass','john','deen','child','jdk'),
        ('tell@gmail.com','pass','jane','adams','child','jdk'),
        ('sleep@gmail.com','pass','elmo','jayson','child','jdk');
        
insert into family(family_name, parent_id)
VALUES('Adams', 2),
        ('Deen', 1),
        ('Jayson', 3);
        
insert into family_members(family_id,user_id)
VALUES(1,2),
        (1,5),
        (2,1),
        (2,4),
        (3,3),
        (3,6);
        
insert into book(title,author,isbn)
VALUES('harry potter', 'jk rowling',3289),
        ('goose girl','shannon hale', 3283),
        ('vampire diares', 'huh',3820),
        ('throne of glass', 'sarah j maas',3782);
        
insert into book_user(book_id, user_id)
VALUES(1,3),
        (2,3),
        (1,2),
        (1,4),
        (1,5),
        (2,5),
        (2,6),
        (2,4);
---------------------------------------------------------


/*ADDING NEW USER ACCOUNT TO FAMILY
INSERT INTO app_user(user_name,password,role,salt)
VALUES('make@gmail.com','pass','child','jdk');*/

--SELECT id FROM app_user WHERE user_name = 'make@gmail.com';
--select id from family where parent_id = 2;

--insert into family_members(family_id, user_id)
--VALUES ((select id from family where parent_id = 2),(SELECT id FROM app_user WHERE user_name = 'make@gmail.com'));

--select id from family where parent_id = 2;



/* GETTING ALL FAMILY MEMBERS
Select a.*, f.family_name
from app_user as a
join family_members as fm on fm.user_id = a.id
join family as f on fm.family_id = f.id
where f.parent_id = 1; 
*/


insert into family(family_name,parent_id)
VALUES(?,?)

insert into family_members(family_id, user_id)
VALUES(?,?)