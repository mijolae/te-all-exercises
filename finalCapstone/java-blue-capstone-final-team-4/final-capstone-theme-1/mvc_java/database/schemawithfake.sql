-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

-- CREATE statements go here
DROP TABLE IF EXISTS app_user, book, family, family_members, book_user, book_family CASCADE;

CREATE TABLE app_user (
  id SERIAL PRIMARY KEY,
  user_name varchar(32) NOT NULL UNIQUE,
  password varchar(32) NOT NULL,
  first_name varchar(32) NOT NULL,
  last_name varchar(32) NOT NULL,
  role varchar(32) NOT NULL,
  salt varchar(255) NOT NULL
  
);

CREATE TABLE book (
id SERIAL PRIMARY KEY,
title varchar(32) NOT NULL,
author varchar(32) NOT NULL,
isbn int NOT NULL
);

CREATE TABLE book_user(
book_id bigint NOT NULL,
user_id bigint NOT NULL,

constraint fk_book_id foreign key (book_id) references book (id),
constraint fk_user_id foreign key (user_id) references app_user (id)
);

CREATE TABLE family (
id SERIAL,
family_name varchar(32) NOT NULL,
parent_id bigint NOT NULL,

constraint pk_id primary key (id),
constraint fk_parent_id foreign key (parent_id) references app_user(id)
);

CREATE TABLE family_members (
family_id bigint NOT NULL,
user_id bigint NOT NULL,
constraint fk_family_id foreign key (family_id) references family(id),
constraint fk_user_id foreign key (user_id) references app_user(id)
);




COMMIT;