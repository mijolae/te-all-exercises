package com.techelevator.dao;

import com.techelevator.entity.User;
import com.techelevator.security.PasswordHasher;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class JDBCFamilyDAOTest {

    private static SingleConnectionDataSource dataSource;
    private JDBCFamilyDAO dao;
    private JDBCUserDAO userDao;
    private PasswordHasher hashMaster = new PasswordHasher();

    /* Before any tests are run, this method initializes the datasource for testing. */
    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/capstone");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);

    }

    /* After all tests have finished running, this method will close the DataSource */
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        dao = new JDBCFamilyDAO(dataSource);

        userDao = new JDBCUserDAO(dataSource, hashMaster);
    }

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void addUserToFamily() {
        User newUser = new User();
        newUser.setFirstName("Elmo");
        newUser.setLastName("BigBird");
        newUser.setUserName("practice@gmail.com");
        newUser.setPassword("PASSSwheeehrheu");
        newUser.setRole("child");

        userDao.saveUser(newUser);

        User parent = userDao.getUserByUserName("mijolae@gmail.com");

        dao.addUserToFamily(parent.getId(),newUser.getUserName());

        assertNotEquals(null, dao.getFamilyMembersByParentId(parent.getId()));
        assertEquals(3, dao.getFamilyMembersByParentId(parent.getId()).size());

    }

    @Test
    public void getFamilyByParentId(){
        ArrayList<User> members = dao.getFamilyMembersByParentId(2l);
        assertNotEquals(null, members);
        assertEquals(2, members.size());
    }

   /* @Test
    public void createNewFamily(){
        User newUser = new User();
        newUser.setFirstName("Sasuke");
        newUser.setLastName("Naruto");
        newUser.setUserName("saucy@gmail.com");
        newUser.setPassword("PASSSwheeehrheu");
        newUser.setRole("parent");

        userDao.saveUser(newUser);

        User parent = userDao.getUserByUserName("saucy@gmail.com");

        dao.createNewFamily(parent.getId(), newUser.getLastName());

        assertNotEquals(null, dao.getFamilyMembersByParentId(parent.getId()));
        assertEquals(1, dao.getFamilyMembersByParentId(parent.getId()).size());
    }*/

}