package com.techelevator.entity;

import java.util.ArrayList;

public class Family {
    private String lastName;
    private long familyId;

    public Family() {

    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFamilyId(long familyId) {
        this.familyId = familyId;
    }

    public String getLastName() {
        return lastName;
    }


    public long getFamilyId() {
        return familyId;
    }

}
