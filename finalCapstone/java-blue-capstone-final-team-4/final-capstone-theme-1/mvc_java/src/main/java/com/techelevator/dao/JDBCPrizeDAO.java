package com.techelevator.dao;

import com.techelevator.entity.Prize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCPrizeDAO implements PrizeDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCPrizeDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void savePrize(Prize prize) {
        jdbcTemplate.update("INSERT into Prize (prize_name, description, minutes_required, user_group, number_of_prizes, start_date, end_date) VALUES (?, ?, ?, ?, ?, ?, ?)",
                prize.getName(), prize.getDescription(), prize.getMinutesReading(), prize.getUserGroup(), prize.getMaxPrizes(),
                prize.getStartDate(), prize.getEndDate());
    }

    @Override
    public List<Prize> getAllPrizes() {
       List<Prize> prizeList = new ArrayList<>();
       String sqlSelectAllPrizes = "SELECT * FROM prize";
       Prize thePrize;
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllPrizes);
        while (results.next()) {
            thePrize = mapRowToPrize(results);
            prizeList.add(thePrize);
        }
        return prizeList;
    }

    @Override
    public void removeInactivePrizes() {
        jdbcTemplate.update("DELETE FROM prize WHERE (SELECT NOW()) > end_date");
    }

    private Prize mapRowToPrize(SqlRowSet results) {
        Prize thePrize = new Prize();
        thePrize.setName(results.getString("prize_name"));
        thePrize.setDescription(results.getString("description"));
        thePrize.setMinutesReading(results.getInt("minutes_required"));
        thePrize.setUserGroup(results.getString("user_group"));
        thePrize.setMaxPrizes(results.getInt("number_of_prizes"));
        thePrize.setStartDate(results.getDate("start_date").toLocalDate());
        thePrize.setEndDate(results.getDate("end_date").toLocalDate());

        return thePrize;
    }

    @Override
    public ArrayList<String> getPrizeNameList(){
        ArrayList<String> allPrizeNames = new ArrayList<>();
        String sqlGetAllPrizeNames = "SELECT * FROM prize";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllPrizeNames);
        while (results.next()){
            allPrizeNames.add(mapRowToPrize(results).getName());
        }
        return allPrizeNames;
    }

    public Prize getPrizeByName(String prizeName){
        String sqlGetPrizeIdByName = "SELECT prize_id FROM prize WHERE prize_name = ?";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetPrizeIdByName, prizeName);
        if(results.next()){
            return mapRowToPrize(results);
        }
        return null;
    }



    @Override
    public void updatePrizeByName(String prize_name, String newPrizeName, String newDescription, int newMinutesRequired,
                                  String newUserGroup, int newNumberOfPrizes, LocalDate startDate, LocalDate endDate){
        Prize oldPrize = getPrizeByName(prize_name);
        StringBuffer columns = new StringBuffer(255);
        if(oldPrize != null){

            if(newPrizeName != null && !newPrizeName.equals(oldPrize.getName())){

                columns.append("prize_name = '" + newPrizeName + "'");
            }


            if(newDescription != null && !newDescription.equals(oldPrize.getDescription())){
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("description = '"+ newDescription +"'");
            }

            if((Integer.valueOf(newMinutesRequired) != null) && !(newMinutesRequired ==oldPrize.getMinutesReading())) {
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("minutes_required = '" + newMinutesRequired + "'");
            }

            if(newUserGroup != null && (!newUserGroup.equals(oldPrize.getUserGroup()))){
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("user_group = '" + newUserGroup + "'");
            }

            if(Integer.valueOf(newNumberOfPrizes) != null &&  (newNumberOfPrizes != oldPrize.getMaxPrizes())){
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("number_of_prizes = '" + newNumberOfPrizes + "'");
            }

            if(startDate != null && (!startDate.equals(oldPrize.getStartDate()))){
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("start_date = '" + startDate + "'");
            }

            if(endDate != null && (!endDate.equals(oldPrize.getEndDate()))){
                if ( columns.length() > 0 ) {
                    columns.append( ", " );
                }
                columns.append("end_date = '" + endDate + "'");
            }
        }

        if(columns.length() > 0){
            String sqlUpdatePrize = "UPDATE prize SET "+ columns.toString() + " WHERE prize_name = ?";
            jdbcTemplate.update(sqlUpdatePrize, prize_name);
        }
    }
}
