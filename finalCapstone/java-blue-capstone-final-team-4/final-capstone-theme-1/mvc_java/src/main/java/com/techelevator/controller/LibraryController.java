package com.techelevator.controller;

import com.techelevator.dao.BookDAO;
import com.techelevator.dao.RecordReadingDAO;
import com.techelevator.entity.Book;
import com.techelevator.entity.RecordReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class LibraryController {

    private BookDAO bookDAO;
    private RecordReadingDAO recordReadingDAO;

    @Autowired
    public LibraryController(BookDAO bookDAO, RecordReadingDAO recordReadingDAO) {
        this.recordReadingDAO = recordReadingDAO;
        this.bookDAO = bookDAO;
    }

    @RequestMapping(path="/library", method= RequestMethod.GET)
    public String displayLibrary(ModelMap modelHolder) {
        if( ! modelHolder.containsAttribute("book")) {
            modelHolder.addAttribute("book", new Book());
        }
        List<Book> allBooks = bookDAO.getAllBooks();
        modelHolder.put("allBooks", allBooks);
        return "library";
    }


    @RequestMapping(path="/addABook", method=RequestMethod.POST)
    public String addBook(@Valid @ModelAttribute Book book, BindingResult result, RedirectAttributes flash) {
        if(result.hasErrors()) {
            flash.addFlashAttribute("book", book);
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "book", result);
            return "library";
        }
        try {
            bookDAO.saveBook(book);
        } catch (Exception exc){
            // good place to log
            return "redirect:/error";
        }
        return "redirect:/library";
        //TODO: Figure out how to send the correct error message for invalid form inputs
    }

    @RequestMapping(path="/recordReading/book", method= RequestMethod.GET)
    public String displayRecordThisBook(HttpServletRequest request, ModelMap modelHolder) {
        if( ! modelHolder.containsAttribute("recordReading")) {
            modelHolder.addAttribute("recordReading", new RecordReading());
        }
        String bookIsbn = request.getParameter("isbn");
        Book book = bookDAO.getBookByIsbn(bookIsbn);
        request.setAttribute("book", book);
        return "recordReading";
    }

    @RequestMapping(path="parent/recordReading/book", method= RequestMethod.GET)
    public String displayRecordThisBookParent(HttpServletRequest request, ModelMap modelHolder) {
        if( ! modelHolder.containsAttribute("recordReading")) {
            modelHolder.addAttribute("recordReading", new RecordReading());
        }
        String bookIsbn = request.getParameter("isbn");
        Book book = bookDAO.getBookByIsbn(bookIsbn);
        request.setAttribute("book", book);
        return "recordReadingParent";
    }

    @RequestMapping(path="/recordReading/book", method= RequestMethod.POST)
    public String addBook(@Valid @ModelAttribute RecordReading recordReading, BindingResult result, RedirectAttributes flash) {
        if(result.hasErrors()) {
            flash.addFlashAttribute("recordReading", recordReading);
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "recordReading", result);
            return "recordReading";
        }
        try {
            recordReadingDAO.saveReadingRecord(recordReading);
            if (recordReading.isDoneReading() == true)
                recordReadingDAO.updateToIsDoneReading(recordReading.getIsbn(), recordReading.getFirstName(), recordReading.getUserId(), recordReading.getRole());
        } catch (Exception exc){
            // good place to log
            return "redirect:/error";
        }
        return "redirect:/";
        //TODO: Figure out how to send the correct error message for invalid form inputs
    }

    //TODO: parent mapping constraining

}

