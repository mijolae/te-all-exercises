package com.techelevator.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class RecordReading {

    private long recordReadingId;
    private long userId;
    private String userName;
    private String title;
    private String author;
    private String isbn;
    private String firstName;
    private String role;
    private String format;
    private int hoursRead;
    private int minutesRead;
    private String notes;
    private boolean doneReading;

    public RecordReading() {
    }

    public RecordReading(long recordReadingId, long userId, String userName, String title, String author, String isbn, String firstName, String role, String format, int hoursRead, int minutesRead, String notes, boolean doneReading) {
        this.recordReadingId = recordReadingId;
        this.userId = userId;
        this.userName = userName;
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.firstName = firstName;
        this.role = role;
        this.format = format;
        this.hoursRead = hoursRead;
        this.minutesRead = minutesRead;
        this.notes = notes;
        this.doneReading = doneReading;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getRecordReadingId() {
        return recordReadingId;
    }

    public void setRecordReadingId(long recordReadingId) {
        this.recordReadingId = recordReadingId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getHoursRead() {
        return hoursRead;
    }

    public void setHoursRead(int hoursRead) {
        this.hoursRead = hoursRead;
    }

    public int getMinutesRead() {
        return minutesRead;
    }

    public void setMinutesRead(int minutesRead) {
        this.minutesRead = minutesRead;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isDoneReading() {
        return doneReading;
    }

    public void setDoneReading(boolean doneReading) {
        this.doneReading = doneReading;
    }
}
