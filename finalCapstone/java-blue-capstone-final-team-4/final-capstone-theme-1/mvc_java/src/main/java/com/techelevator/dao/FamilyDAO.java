package com.techelevator.dao;

import com.techelevator.entity.Family;
import com.techelevator.entity.User;

import java.util.ArrayList;

public interface FamilyDAO {
    public void addUserToFamily(long currentUserId, String username);
    public ArrayList<User> getFamilyMembersByParentId(long user_id);
    public void createNewFamily(Family family);
}
