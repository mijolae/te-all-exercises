package com.techelevator.controller;

import com.techelevator.dao.PrizeDAO;
import com.techelevator.entity.Prize;
import com.techelevator.entity.RecordReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
public class PrizeController {

    private PrizeDAO prizeDAO;

    @Autowired
    public PrizeController(PrizeDAO prizeDAO) {
        this.prizeDAO = prizeDAO;
    }

    @RequestMapping(path="/viewPrizes", method= RequestMethod.GET)
    public String displayPrizes(ModelMap modelHolder) {
        if( ! modelHolder.containsAttribute("prizes")) {
            modelHolder.addAttribute("prizes", new Prize());
        }
        List<Prize> allPrizes = prizeDAO.getAllPrizes();
        modelHolder.put("allPrizes", allPrizes);
        return "viewPrizes";
    }

    @RequestMapping(path="/viewPrizes", method=RequestMethod.POST)
    public String addPrizes(@Valid @ModelAttribute Prize prize, BindingResult result, RedirectAttributes flash) {
        if(result.hasErrors()) {
            flash.addFlashAttribute("prizes", prize);
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "prizes", result);
            return "viewPrizes";
        }
        try {
            prizeDAO.savePrize(prize);
        } catch (Exception exc){
            // good place to log
            return "redirect:/error";
        }
        return "redirect:/viewPrizes";
    }

    @RequestMapping(path="/success", method= RequestMethod.GET)
    public String displayPrizeSuccess() {

        return "/success";
    }


    @RequestMapping(path="/success", method=RequestMethod.POST)
    public String removePrizeSuccess() {
        try {
            prizeDAO.removeInactivePrizes();
        } catch (Exception exc){
            // good place to log
            return "redirect:/error";
        }
        return "redirect:/success";
    }

    @RequestMapping(path="/editAPrize", method = RequestMethod.GET)
    public String displayEditForm(ModelMap modelMap){
        if( ! modelMap.containsAttribute("prize")){
            modelMap.addAttribute("prize", new Prize());
        }
        List<String> prizeNameList = prizeDAO.getPrizeNameList();
        modelMap.addAttribute("allPrizeNames", prizeNameList);
        return "editAPrize";
    }

    @RequestMapping(path="/editAPrize", method = RequestMethod.POST)
    public String editAPrize(@Valid @ModelAttribute Prize prize,
                             @RequestParam("prize_name") String prize_name,
                             @RequestParam("newPrizeName") String newPrizeName,
                             @RequestParam("newDescription") String newDescription,
                             @RequestParam("newMinutesReading") int newMinutesReading,
                             @RequestParam("newUserGroup") String newUserGroup,
                             @RequestParam("newNumberOfPrizes") int newNumberOfPrizes,
                             @RequestParam("newStartDate") LocalDate newStartDate,
                             @RequestParam("newEndDate") LocalDate newEndDate,
                             BindingResult result,
                             RedirectAttributes flash){
        if(result.hasErrors()){
            flash.addFlashAttribute("prizeEdit", prize);
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "prizeEdit", result);
            return "editAPrize";
        }
        try{
            prizeDAO.updatePrizeByName(prize_name,newPrizeName,newDescription,newMinutesReading,newUserGroup,
                    newNumberOfPrizes,newStartDate,newEndDate);
        }catch(Exception exc){
            return "redirect:/error";
        }
        return "redirect:/editSuccess";
    }
}