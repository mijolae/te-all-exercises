package com.techelevator.dao;

import com.techelevator.entity.Book;
import com.techelevator.entity.RecordReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCRecordReadingDAO implements RecordReadingDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCRecordReadingDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveReadingRecord(RecordReading recordReading) {
        jdbcTemplate.update("insert into record_reading (userid, userName, firstname, role, format, hoursread, minutesread, isbn, notes, donereading) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                recordReading.getUserId(), recordReading.getUserName(), recordReading.getFirstName(), recordReading.getRole(), recordReading.getFormat(), recordReading.getHoursRead(),
                recordReading.getMinutesRead(), recordReading.getIsbn(), recordReading.getNotes(), recordReading.isDoneReading());
    }

    @Override
    public List<RecordReading> getAllReadingActivity(String username) {
        List<RecordReading> readingActivityList = new ArrayList<>();
        RecordReading theRecordReading;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM record_reading WHERE username = ?", username);
        while (results.next()) {
            theRecordReading = mapRowToRecordReading(results);
            readingActivityList.add(theRecordReading);
        }
        return readingActivityList;
    }

    @Override
    public List<RecordReading> getAllOthersReadingActivity(String username) {
        List<RecordReading> othersReadingActivityList = new ArrayList<>();
        RecordReading theRecordReading;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM record_reading WHERE username != ?", username);
        while (results.next()) {
            theRecordReading = mapRowToRecordReading(results);
            othersReadingActivityList.add(theRecordReading);
        }
        return othersReadingActivityList;
    }

    @Override
    public List<Book> getAllReadingDone(String username) {
        List<Book> readingActivityList = new ArrayList<>();
        Book theBook;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT book.title, book.author, book.isbn" +
                " FROM book" +
                " JOIN record_reading ON record_reading.isbn = book.isbn" +
                " WHERE record_reading.donereading = TRUE AND username = ?" +
                " GROUP BY book.title, book.author, book.isbn;" , username);
        while (results.next()) {
            theBook = mapRowToBook(results);
            readingActivityList.add(theBook);
        }
        return readingActivityList;
    }

    @Override
    public List<Book> getAllOtherReadingDone(String username) {
        List<Book> readingActivityList = new ArrayList<>();
        Book theBook;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT book.title, book.author, book.isbn" +
                " FROM book" +
                " JOIN record_reading ON record_reading.isbn = book.isbn" +
                " WHERE record_reading.donereading = TRUE AND username != ?" +
                " GROUP BY book.title, book.author, book.isbn;" , username);
        while (results.next()) {
            theBook = mapRowToBook(results);
            readingActivityList.add(theBook);
        }
        return readingActivityList;
    }

    @Override
    public List<Book> getAllReadingNotDone(String username) {
        List<Book> readingActivityListNotDone = new ArrayList<>();
        Book theBook;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT book.title, book.author, book.isbn" +
                " FROM book" +
                " JOIN record_reading ON record_reading.isbn = book.isbn" +
                " WHERE record_reading.donereading = False AND username = ?" +
                " GROUP BY book.title, book.author, book.isbn;" , username);
        while (results.next()) {
            theBook = mapRowToBook(results);
            readingActivityListNotDone.add(theBook);
        }
        return readingActivityListNotDone;
    }

    @Override
    public List<Book> getAllOtherReadingNotDone(String username) {
        List<Book> readingActivityListNotDone = new ArrayList<>();
        Book theBook;
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT book.title, book.author, book.isbn" +
                " FROM book" +
                " JOIN record_reading ON record_reading.isbn = book.isbn" +
                " WHERE record_reading.donereading = False AND username != ?" +
                " GROUP BY book.title, book.author, book.isbn;" , username);
        while (results.next()) {
            theBook = mapRowToBook(results);
            readingActivityListNotDone.add(theBook);
        }
        return readingActivityListNotDone;
    }

    @Override
    public void updateToIsDoneReading(String isbn, String firstName, long userId,  String role) {
        jdbcTemplate.update("UPDATE record_reading SET donereading = TRUE WHERE isbn = ? AND firstname = ? AND userid = ? AND role = ? ", isbn, firstName, userId, role);
    }

    private RecordReading mapRowToRecordReading(SqlRowSet results) {
        RecordReading theRecordReading = new RecordReading();
        theRecordReading.setUserId(results.getLong("userid"));
        theRecordReading.setIsbn(results.getString("isbn"));
        theRecordReading.setFirstName(results.getString("firstName"));
        theRecordReading.setRole(results.getString("role"));
        theRecordReading.setFormat(results.getString("format"));
        theRecordReading.setHoursRead(results.getInt("hoursRead"));
        theRecordReading.setMinutesRead(results.getInt("minutesRead"));
        theRecordReading.setNotes(results.getString("notes"));
        theRecordReading.setDoneReading(results.getBoolean("doneReading"));

        return theRecordReading;
    }

    private Book mapRowToBook(SqlRowSet results) {
        Book book = new Book();
        book.setTitle(results.getString("title"));
        book.setAuthor(results.getString("author"));
        book.setIsbn(results.getString("isbn"));
        return book;
    }

}



