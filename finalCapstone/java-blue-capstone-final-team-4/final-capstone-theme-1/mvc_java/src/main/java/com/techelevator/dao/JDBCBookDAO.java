package com.techelevator.dao;


import com.techelevator.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCBookDAO implements BookDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCBookDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void saveBook(Book book) {
        jdbcTemplate.update("INSERT INTO book(title, author, isbn) " + "VALUES (?, ?, ?)", book.getTitle(),book.getAuthor(),book.getIsbn());

    }

    public Book getBookByIsbn(String isbn){
        Book thisBook = new Book();
        SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM book WHERE isbn = ?", isbn);

        if(results.next()){
            return mapRowToBook(results);
        }

        return null;
    }

    public List<Book> getAllBooks() {
        List<Book> allBooks = new ArrayList<>();
        String sqlSelectAllBooks = "SELECT * FROM book";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllBooks);

        while (results.next()) {
            Book book = new Book();
            book.setTitle(results.getString("title"));
            book.setAuthor(results.getString("author"));
            book.setIsbn(results.getString("isbn"));
            allBooks.add(book);
        }
        return allBooks;
    }

    private Book mapRowToBook(SqlRowSet results) {
        Book book = new Book();
        book.setTitle(results.getString("title"));
        book.setAuthor(results.getString("author"));
        book.setIsbn(results.getString("isbn"));
        return book;
    }

}
