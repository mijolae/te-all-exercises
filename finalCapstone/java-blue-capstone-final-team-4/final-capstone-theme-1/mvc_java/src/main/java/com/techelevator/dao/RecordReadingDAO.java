package com.techelevator.dao;

import com.techelevator.entity.Book;
import com.techelevator.entity.RecordReading;

import java.util.List;

public interface RecordReadingDAO {

    public void saveReadingRecord(RecordReading recordReading);

    public List<RecordReading> getAllReadingActivity(String username);

    List<RecordReading> getAllOthersReadingActivity(String username);

    public List<Book> getAllReadingDone(String username);

    List<Book> getAllOtherReadingDone(String username);

    List<Book> getAllReadingNotDone(String username);

    List<Book> getAllOtherReadingNotDone(String username);

    public void updateToIsDoneReading(String isbn, String firstName, long userId, String role);
}
