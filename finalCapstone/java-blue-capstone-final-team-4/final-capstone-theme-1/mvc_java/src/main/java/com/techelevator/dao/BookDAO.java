package com.techelevator.dao;

import com.techelevator.entity.Book;

import java.util.List;

public interface BookDAO {

    public void saveBook(Book book);

    public List<Book> getAllBooks();

    public Book getBookByIsbn(String isbn);

}
