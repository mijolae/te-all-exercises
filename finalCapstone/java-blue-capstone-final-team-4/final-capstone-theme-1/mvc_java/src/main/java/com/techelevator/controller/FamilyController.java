package com.techelevator.controller;

import com.techelevator.dao.FamilyDAO;
import com.techelevator.entity.Book;
import com.techelevator.entity.Family;
import com.techelevator.entity.User;
import com.techelevator.example.FormExample;
import com.techelevator.security.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.techelevator.security.AuthorizationFilter.LOGGED_USER;

@Controller
public class FamilyController {
    private FamilyDAO familyDAO;

    @Autowired
    public FamilyController(FamilyDAO familyDAO) {
        this.familyDAO = familyDAO;
    }

    @RequestMapping(path="/parent/createAFamily", method = RequestMethod.GET)
    public String displayCreateFamilyForm(ModelMap modelHolder) {
        if( ! modelHolder.containsAttribute("family")) {
            modelHolder.addAttribute("family", new Family());
        }
        return "createAFamily";
    }

    @RequestMapping(path = "/parent/createAFamily", method = RequestMethod.POST)
    public String processNewFamily(
            @Valid @ModelAttribute Family family,
            BindingResult result, ModelMap modelHolder,
            RedirectAttributes flash) {

        flash.addFlashAttribute("family", family);

        if (result.hasErrors()) {
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "family", result);
            return "redirect:/parent/createAFamily";
        }
        // This is where the Business or DAO object used to insert or update form data
        familyDAO.createNewFamily(family);
        modelHolder.put("userId", family.getFamilyId());
        flash.addFlashAttribute("userId", family.getFamilyId());

        return "redirect:/parent/addToFamily";
    }

    @RequestMapping(path="/parent/addToFamily", method = RequestMethod.GET)
    public String displayFamily(ModelMap modelHolder, HttpServletRequest request, RedirectAttributes flash) {
       Map<String,?> flashMap = RequestContextUtils.getInputFlashMap(request);
       Long parentId = 0l;
       if(flashMap != null){
           parentId = (Long) flashMap.get("userId");
       }
       if( ! modelHolder.containsAttribute("newFamilyMember")){
           modelHolder.addAttribute("newFamilyMember", new User());
       }

       modelHolder.addAttribute("currentUserId",parentId);

        ArrayList<User> allFamilyMembers = familyDAO.getFamilyMembersByParentId(parentId);
        modelHolder.put("familyMembers", allFamilyMembers);
        return "addToFamily";
    }

    @RequestMapping(path = "/parent/addToFamily", method = RequestMethod.POST)
    public String processNewFamilyMember(@Valid @ModelAttribute User user,
            @RequestParam("username") String username,
            BindingResult result,
            RedirectAttributes flash,
            ModelMap modelHolder) {

        flash.addFlashAttribute("newFamilyMember", user);
        flash.addFlashAttribute("family", modelHolder.get("currentUserId"));

        if (result.hasErrors()) {
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "family", result);
            return "redirect:/parent/addToFamily";
        }
        // This is where the Business or DAO object used to insert or update form data
        familyDAO.addUserToFamily((Long)modelHolder.get("currentUserId"),user.getUserName());

        return "redirect:/parent/addToFamily";
    }

}
