package com.techelevator.dao;

import com.techelevator.entity.Prize;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public interface PrizeDAO {
    public void savePrize(Prize prize);
    public List<Prize> getAllPrizes();
    public void removeInactivePrizes();
    public void updatePrizeByName(String prize_name, String newPrizeName, String newDescription, int newMinutesRequired,
                                  String newUserGroup, int newNumberOfPrizes, LocalDate startDate, LocalDate endDate);
    public ArrayList<String> getPrizeNameList();

}
