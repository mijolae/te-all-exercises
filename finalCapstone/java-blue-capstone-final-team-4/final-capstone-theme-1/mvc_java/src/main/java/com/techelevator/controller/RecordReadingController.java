package com.techelevator.controller;

import com.techelevator.dao.BookDAO;
import com.techelevator.dao.PrizeDAO;
import com.techelevator.dao.RecordReadingDAO;
import com.techelevator.entity.Book;
import com.techelevator.entity.Prize;
import com.techelevator.entity.RecordReading;
import com.techelevator.entity.User;
import com.techelevator.security.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RecordReadingController {

    private BookDAO bookDAO;
    private RecordReadingDAO recordReadingDAO;
    private RecordReading recordReading;
    private PrizeDAO prizeDAO;

    @Autowired
    public RecordReadingController(BookDAO bookDAO, RecordReadingDAO recordReadingDAO, PrizeDAO prizeDAO) {
        this.bookDAO = bookDAO;
        this.recordReadingDAO = recordReadingDAO;
        this.prizeDAO = prizeDAO;
    }

/*    @RequestMapping(path = "/recordReading", method = RequestMethod.GET)
    public String recordReadingForm(ModelMap modelHolder) {
        if (!modelHolder.containsAttribute("book")) {
            modelHolder.addAttribute("book", new RecordReading());
        }

        return "recordReading";
    }

    @RequestMapping(path = "/parent/recordReading", method = RequestMethod.GET)
    public String recordReadingFormParent(ModelMap modelHolder) {
        if (!modelHolder.containsAttribute("book")) {
            modelHolder.addAttribute("book", new RecordReading());
        }

        return "recordReading";
    }

    @RequestMapping(path="/recordReading", method=RequestMethod.POST)
    public String recordReading(@Valid @ModelAttribute RecordReading recordReading, BindingResult result, RedirectAttributes flash) {
        if(result.hasErrors()) {
            flash.addFlashAttribute("recordReading", recordReading);
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "recordReading", result);
            return "recordReading";
        }
        try {
            recordReadingDAO.saveReadingRecord(recordReading);
            if (recordReading.isDoneReading() == true)
                recordReadingDAO.updateToIsDoneReading(recordReading.getIsbn(), recordReading.getFirstName(), recordReading.getUserId(), recordReading.getRole());
        } catch (Exception exc){
            // good place to log
            return "redirect:/error";
        }
        return "redirect:/";
        //TODO: Figure out how to send the correct error message for invalid form inputs
    }*/


    @RequestMapping(path="/readingActivity", method= RequestMethod.GET)
    public String displayReadingActivity(ModelMap modelHolder, HttpSession session) {
        if( ! modelHolder.containsAttribute("readingActivity")) {
            modelHolder.addAttribute("readingActivity", new RecordReading());
        }
        User user = (User)session.getAttribute(AuthorizationFilter.LOGGED_USER);

        List<RecordReading> allReadingRecords = recordReadingDAO.getAllReadingActivity(user.getUserName());
        modelHolder.put("allReadingRecords", allReadingRecords);

        List<RecordReading> allOthersReadingRecords = recordReadingDAO.getAllOthersReadingActivity(user.getUserName());
        modelHolder.put("allOthersReadingRecords", allOthersReadingRecords);

        List<Prize> allPrizes = prizeDAO.getAllPrizes();
        modelHolder.put("allPrizes", allPrizes);

        List<Book> allReadingNotDone = recordReadingDAO.getAllReadingNotDone(user.getUserName());
        modelHolder.put("allReadingNotDone", allReadingNotDone);

        List<Book> allOtherReadingNotDone = recordReadingDAO.getAllOtherReadingNotDone(user.getUserName());
        modelHolder.put("allOtherReadingNotDone", allOtherReadingNotDone);

        List<Book> allReadingDone = recordReadingDAO.getAllReadingDone(user.getUserName());
        modelHolder.put("allReadingDone", allReadingDone);

        List<Book> allOtherReadingDone = recordReadingDAO.getAllOtherReadingDone(user.getUserName());
        modelHolder.put("allOtherReadingDone", allOtherReadingDone);

        return "readingActivity";
    }

}

