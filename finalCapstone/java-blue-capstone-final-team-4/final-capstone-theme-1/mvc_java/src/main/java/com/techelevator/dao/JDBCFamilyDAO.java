package com.techelevator.dao;

import com.sun.rowset.internal.Row;
import com.techelevator.entity.Family;
import com.techelevator.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCFamilyDAO implements FamilyDAO{

    private UserDAO userDAO;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCFamilyDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void addUserToFamily(long currentUserId, String username) {
        String sqlGetNewUser = "insert into family_members(family_id, user_id)" +
                                " VALUES ((select f.id from family as f join family_members as fm on fm.family_id = f.id join app_user as a on a.id = fm.user_id where a.id = ?), (SELECT id FROM app_user WHERE user_name = ?))";

        jdbcTemplate.update(sqlGetNewUser, currentUserId, username);
    }

    @Override
    public ArrayList<User> getFamilyMembersByParentId(long user_id) {
        ArrayList<User> familyMembers = new ArrayList<>();
        String sqlGetFamily = "Select a.id, a.user_name, a.role, a.first_name,a.last_name" +
                " from app_user as a" +
                " join family_members as fm on fm.user_id = a.id" +
                " join family as f on fm.family_id = f.id" +
                " where a.id =? AND a.role='parent'";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetFamily, user_id);
        while(results.next()){
            User userToAdd = mapRowToUser(results);
            familyMembers.add(userToAdd);
        }
        return familyMembers;
    }

    @Override
    public void createNewFamily(Family family) {
        String sqlCreateNewFamily = "insert into family(family_name)" + " VALUES(?)";
        jdbcTemplate.update(sqlCreateNewFamily, family.getLastName());

        String sqlAddParentToFamily = "insert into family_members(family_id, user_id)" +
                                        " VALUES((select id from family where family_name = ?),?)";
        jdbcTemplate.update(sqlAddParentToFamily,family.getLastName(),family.getFamilyId());
    }


    //make sure to get the correct input AND proper authentication
    //Maybe for simplification, you could change User to a long and grab userId from the controller
    private User mapRowToUser(SqlRowSet rowSet){
        User user = new User();
        user.setUserName(rowSet.getString("user_name"));
        user.setId(rowSet.getLong("id"));
        user.setRole("role");
        user.setFirstName(rowSet.getString("first_name"));
        user.setLastName(rowSet.getString("last_name"));
        return user;
    }
}
