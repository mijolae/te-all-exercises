package com.techelevator.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Prize {

    private String name;
    private String description;
    private int minutesReading;
    private String userGroup;
    private int maxPrizes;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;


    public Prize() {
    }

    public Prize(String name, String description, int minutesReading, String userGroup, int maxPrizes, LocalDate startDate, LocalDate endDate) {
        this.name = name;
        this.description = description;
        this.minutesReading = minutesReading;
        this.userGroup = userGroup;
        this.maxPrizes = maxPrizes;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinutesReading() {
        return minutesReading;
    }

    public void setMinutesReading(int minutesReading) {
        this.minutesReading = minutesReading;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public int getMaxPrizes() {
        return maxPrizes;
    }

    public void setMaxPrizes(int maxPrizes) {
        this.maxPrizes = maxPrizes;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
