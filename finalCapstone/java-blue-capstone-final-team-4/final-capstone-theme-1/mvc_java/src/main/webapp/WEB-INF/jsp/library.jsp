<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 8/31/2020
  Time: 3:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

    <h1> Library </h1>
    <table class="table">
        <tr>
            <th scope = "col">Title</th>
            <th scope = "col">Author</th>
            <th scope = "col">ISBN</th>
        </tr>

        <c:forEach items="${allBooks}" var="book">
            <c:if test="${LOGGED_USER.role == 'child'}"><c:url var="recordActivity" value="/recordReading/book?isbn=${book.isbn}"/></c:if>
            <c:if test="${LOGGED_USER.role == 'parent'}"><c:url var="recordActivity" value="/parent/recordReading/book?isbn=${book.isbn}"/></c:if>
            <tr>
                <!-- MULTIPLE TD LINES IN ORDER TO FILL OUT THE HEADS-->
                <td>${book.title}</td>
                <td>${book.author}</td>
                <td>${book.isbn}</td>
                <td>
                    <a href="${recordActivity}" class="btn btn-info btn-icon-split lav-btn">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                        <span class="text">Record Reading</span>
                    </a>
                </td>
            </tr>
        </c:forEach>
    </table>

    <BR>

    <div class="form-container">
        <%--        <c:url var="formExampleUrl" value="/formExample"/>--%>
        <form:form action="/addABook" method="POST" modelAttribute="book">
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <fieldset>

                <div class="form">
                    <h3 class="centered">Add a Book</h3>
                    <form:input id="title" name="title" path="title" required="true"/>
                    <label for="title">Title</label>
                    <form:input id="author" name="author" path="author" required="true"/>
                    <label for="author">Author</label>
                    <form:input id="isbn" name="isbn" path="isbn" required="true"/>
                    <label for="isbn">ISBN</label>
                    <button type="submit">SUBMIT</button>
                </div>
            </fieldset>
        </form:form>
    </div>
    <!-- /.form-container -->
</div>
<!-- /.container-fluid -->
<!-- End of Page Content -->

<!-- Page level plugins -->


<c:import url="/WEB-INF/jsp/common/footer.jsp" />