<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 9/8/2020
  Time: 10:59 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<h1>You've deleted all inactive prizes!</h1>
<a href="/viewPrizes"><h2 class="center no-deco">Return to View Prizes</h2></a>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />