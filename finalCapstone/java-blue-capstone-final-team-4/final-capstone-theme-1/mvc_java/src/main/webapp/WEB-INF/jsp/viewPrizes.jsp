<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />



<%--<c:url var="prizeDelete" value="/success" />
<a href="${prizeDelete}" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
    <span class="text">Remove Inactive Prizes</span>
</a>--%>

<div class="container-fluid">
    <h1> All Prizes </h1>
    <table class="table">
        <tr>
            <th scope = "col">Name</th>
            <th scope = "col">Description</th>
            <th scope = "col">Minutes Of Reading Required</th>
            <th scope = "col">User Group</th>
            <th scope = "col">Number of Prizes</th>
            <th scope = "col">Start Date</th>
            <th scope = "col">End Date</th>
        </tr>
        <c:forEach items="${allPrizes}" var="allPrizes">
            <tr>
                <td>${allPrizes.name}</td>
                <td>${allPrizes.description}</td>
                <td>${allPrizes.minutesReading}</td>
                <td>${allPrizes.userGroup}</td>
                <td>${allPrizes.maxPrizes}</td>
                <td>${allPrizes.startDate}</td>
                <td>${allPrizes.endDate}</td>
                    <%--                <td>--%>
                    <%--                    <a href="${recordActivity}" class="btn btn-info btn-icon-split">--%>
                    <%--                    <span class="icon text-white-50">--%>
                    <%--                      <i class="fas fa-info-circle"></i>--%>
                    <%--                    </span>--%>
                    <%--                        <span class="text">Record Reading</span>--%>
                    <%--                    </a>--%>
                    <%--                </td>--%>
            </tr>
        </c:forEach>
    </table>
    <form:form action="/success" method="POST">
        <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
        <button class="lav-btn text-white" type="submit" id="prizeDelete" name="prizeDelete" >Delete All Inactive Prizes</button>
    </form:form>


    <div class="form-container">
        <%--        <c:url var="formExampleUrl" value="/formExample"/>--%>
        <form:form action="/viewPrizes" method="POST" modelAttribute="prizes">
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <fieldset>


                <div class="form">
                    <h3 class="centered">Add A Prize</h3>
                    <form:input id="name" name="name" path="name" required="true"/>
                    <label for="name">Name of Prize: </label>
                    <form:input id="description" name="description" path="description" required="true"/>
                    <label for="description">Description: </label>
                    <form:input id="minutesReading" name="minutesReading" path="minutesReading" required="true"/>
                    <label for="minutesReading">Milestone: </label>
                    <label for="userGroup">Eligible Users: </label>
                    <select id="userGroup" name="userGroup" path="userGroup">
                        <option value="parent">Parent</option>
                        <option value="child">Child</option>
                        <option value="both">Parent and Child</option>
                    </select>
                    <form:input id="maxPrizes" name="maxPrizes" path="maxPrizes" required="true"/>
                    <label for="maxPrizes">Max Prizes: </label>
                    <form:input id="startDate"  type="date" name="startDate" path="startDate" required="true"/>
                    <label for="startDate">Start Date: </label>
                    <form:input id="endDate" type="date" name="endDate" path="endDate" required="true"/>
                    <label for="endDate">End Date: </label>
                    <button type="submit">SUBMIT</button>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>



<c:import url="/WEB-INF/jsp/common/footer.jsp" />



