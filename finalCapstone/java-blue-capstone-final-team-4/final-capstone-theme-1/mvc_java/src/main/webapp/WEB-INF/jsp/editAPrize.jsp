<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<div class="form-container">
<form:form action="/editAPrize" method="POST" modelAttribute="prize">

    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>

    <fieldset>

        <legend>Edit a Prize</legend>


        <div class="form">

            <label for="prize_name">User type: </label>

            <select id="prize_name" name="prize_name" path="prizeName">

                <c:forEach items = "${allPrizeNames}" var="prize">

                    <option value="${prize.name}">${prize.name}</option>

                </c:forEach>

            </select>

            <br>

            <form:input id="newPrizeName" name="newPrizeName" path="name"/>

            <label for="newPrizeName">Edit Prize Name: </label>

            <form:input id="newDescription" name="newDescription" path="description"/>

            <label for="newDescription">Edit Description: </label>

            <form:input id="newMinutesReading" name="newMinutesReading" path="minutesReading"/>

            <label for="newMinutesReading">Edit Minutes of Reading Required: </label>

            <label for="newUserGroup">Edit User Group: </label>

            <select id="newUserGroup" name="newUserGroup" path="role">

                <option value="parent">Parents</option>

                <option value="child">Children</option>

                <option value="both">Parents and Children</option>

            </select>

            <form:input id="newNumberOfPrizes" name="newNumberOfPrizes" path="maxPrizes"/>

            <label for="newNumberOfPrizes">Edit Maximum Number of Prizes: </label>

            <form:input id="newStartDate"  type="date" name="newStartDate" path="startDate"/>

            <label for="newStartDate">Edit Start Date: </label>

            <form:input id="newEndDate" type="date" name="newEndDate" path="endDate"/>

            <label for="newEndDate">Edit End Date: </label>

            <button type="submit">SUBMIT</button>

        </div>

    </fieldset>

</form:form>

</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />