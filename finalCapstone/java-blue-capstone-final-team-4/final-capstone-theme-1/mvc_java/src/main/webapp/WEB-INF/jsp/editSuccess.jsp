<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<h1>You've updated ${prize.name}!</h1>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />
