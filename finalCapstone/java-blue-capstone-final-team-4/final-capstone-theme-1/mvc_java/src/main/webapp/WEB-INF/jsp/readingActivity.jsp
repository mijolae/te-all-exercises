<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="/WEB-INF/jsp/common/header.jsp"/>

<div class="container-fluid">

    <h2> Your List of Completed Readings </h2>
    <table class="table">
        <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
        </tr>
        <c:forEach items="${allReadingDone}" var="allReadingDone">
            <tr>
                <td>${allReadingDone.isbn}</td>
                <td>${allReadingDone.title}</td>
                <td>${allReadingDone.author}</td>
            </tr>
        </c:forEach>
    </table>

    <BR><BR>
    <h2> Currently Reading </h2>
    <table class="table">
        <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
        </tr>
        <c:forEach items="${allReadingNotDone}" var="allReadingNotDone">
            <tr>
                <td>${allReadingNotDone.isbn}</td>
                <td>${allReadingNotDone.title}</td>
                <td>${allReadingNotDone.author}</td>
            </tr>
        </c:forEach>
    </table>

    <c:set var="totalNumberOfMinutes" value="0"/>
    <c:forEach items="${allReadingRecords}" var="readingActivity">
        <c:set var="totalNumberOfMinutes" value="${totalNumberOfMinutes + readingActivity.minutesRead}"/>
    </c:forEach>


    <c:set var="totalNumberOfMinutesS" value="0"/>
    <c:forEach items="${allReadingRecords}" var="readingActivityS">
        <c:set var="totalNumberOfMinutesS" value="${totalNumberOfMinutesS + (readingActivityS.hoursRead * 60)}"/>
    </c:forEach>

    <c:set var="minutesSpent" value="${totalNumberOfMinutes + totalNumberOfMinutesS}"/>
    <BR><BR>
    <h2>Total Minutes Spent Reading: ${minutesSpent}</h2>

    <BR><BR>
    <c:set var="notCompletedReadingChart" value="15"/>
    <c:set var="notCompletedReadingChart" value="15"/>

    <c:forEach items="${allPrizes}" var="allPrizes">
        <c:set var="prizeName" value="${allPrizes.name}"/>
        <c:set var="pirzeProgress" value="${allPrizes.minutesReading - minutesSpent}"/>
        <c:choose>
            <c:when test="${pirzeProgress > 0}">
                <div class="col-xl-4 col-lg-5">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Progress to Prize</h6>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-pie pt-4">
                                <canvas id="myPieChart"></canvas>
                            </div>
                            <hr>
                            You need ${pirzeProgress} more minutes of reading to win the prize: <b>${prizeName}</b>!
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <h1 style="color:#FFD700"><b>Congrats you've read enough to win a ${prizeName}!!</b></h1>
            </c:otherwise>
        </c:choose>
    </c:forEach>

    <BR><BR><BR>
    <h2> Your Family's Reading Session List </h2>
    <table class="table">
        <tr>
            <th scope="col">Name of Family Member</th>
            <th scope="col">ISBN</th>
            <th scope="col">Format</th>
            <th scope="col">Logged Hours Read</th>
            <th scope="col">Logged Minutes Read</th>
        </tr>
        <c:forEach items="${allOthersReadingRecords}" var="allOthersReadingRecords">
            <tr>
                <td>${allOthersReadingRecords.firstName}</td>
                <td>${allOthersReadingRecords.isbn}</td>
                <td>${allOthersReadingRecords.format}</td>
                <td>${allOthersReadingRecords.hoursRead}</td>
                <td>${allOthersReadingRecords.minutesRead}</td>
            </tr>
        </c:forEach>
    </table>
</div>

<!-- Page level plugins -->
<script src="/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->

<script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    // Pie Chart Example
    var ctx = document.getElementById("myPieChart");
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Completed Reading in Minutes", "Minutes Needed to Win a Prize!"],
            datasets: [{
                data: [${minutesSpent}, ${pirzeProgress}],
                backgroundColor: ['#4e73df', '#1cc88a'],
                hoverBackgroundColor: ['#2e59d9', '#17a673'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });

</script>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />
