<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 8/31/2020
  Time: 3:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

    <h1> Current Family Members </h1>
    <table class="table">
        <tr>
            <th scope = "col">First Name</th>
            <th scope = "col">Last Name</th>
            <th scope = "col">Username</th>
        </tr>
        <c:forEach items="${familyMembers}" var="user">
            <tr>
                <!-- MULTIPLE TD LINES IN ORDER TO FILL OUT THE HEADS-->
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.userName}</td>
            </tr>
        </c:forEach>
    </table>
</div>
    <div class="form-container">
        <form:form action="/parent/addToFamily" method="POST" modelAttribute="newFamilyMember">
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <fieldset>
                <div class="form">
                    <h3 class="centered">Add to Family</h3>
                    <form:input type="hidden" id="userId" name="userId" path="id" required="true" value = "${LOGGED_USER.id}"/>
                    <form:input id="username" name="username" path="userName" required="true"/>
                    <label for="username">User's Username:</label>
                    <button type="submit">SUBMIT</button>
                </div>
            </fieldset>
        </form:form>
    </div>
    <!-- /.form-container -->
<!-- /.container-fluid -->
<!-- End of Page Content -->

<!-- Page level plugins -->


<c:import url="/WEB-INF/jsp/common/footer.jsp" />
