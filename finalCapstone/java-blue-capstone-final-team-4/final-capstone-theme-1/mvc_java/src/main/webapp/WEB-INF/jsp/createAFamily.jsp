<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 9/4/2020
  Time: 8:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<div class="form-container">
    <%--        <c:url var="formExampleUrl" value="/formExample"/>--%>
    <form:form action="/parent/createAFamily" method="POST" modelAttribute="family">
        <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
        <fieldset>
            <div class="form">
                <h3 class="centered">Create a Family</h3>
                <form:input type="hidden" id="familyId" name="familyId" path="familyId" required="true" value = "${LOGGED_USER.id}"/>
                <form:input id="lastName" name="lastName" path="lastName" required="true"/>
                <label for="lastName">Family Name:</label>
                <button type="submit">SUBMIT</button>
            </div>
        </fieldset>
    </form:form>
</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />