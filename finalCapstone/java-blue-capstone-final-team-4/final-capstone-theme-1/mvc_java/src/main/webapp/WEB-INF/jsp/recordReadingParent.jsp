<%--&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: Chase Java--%>
<%--  Date: 9/2/2020--%>
<%--  Time: 3:11 PM--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <title>Title</title>--%>
<%--    <div>${book.isbn}</div>--%>
<%--</head>--%>
<%--<body>--%>

<%--</body>--%>
<%--</html>--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

    <c:url var="recordReading" value="/recordReading/book"/>
    <div class="form-container">
        <form:form action="${recordReading}" method="POST" modelAttribute="recordReading">
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <fieldset>


                <div class="form">
                    <h3 class="centered">Record Reading Activity</h3>
                    <label for="name">First Name</label>
                    <form:input id="name" name="firstName" path="firstName" required="true"/>

                    <label for="userName">User's Email</label>
                    <form:input id="userName" name="userName" path="userName" required="true"/>

                    <form:input type="hidden" value="${book.isbn}" name="isbn" path="isbn" required="true"/>

                    <form:input type="hidden" value="${LOGGED_USER.id}" name="userId" path="userId" required="true"/>

                    <label for="role">Role</label>
                    <form:select id="role" name="role" path="role" required="true">
                        <option value="parent">Parent</option>
                        <option value="child">Child</option>
                    </form:select>


                    <label for="format">Format</label>
                    <form:select id="format" name="format" path="format" required="true">
                        <option value="paper">Paper</option>
                        <option value="digital">Digital</option>
                        <option value="audiobook">Audiobook</option>
                        <option value="readAloudReader">Read-Aloud (Reader)</option>
                        <option value="readAloudListener">Read-Aloud (Listener)</option>
                        <option value="other">Other</option>
                    </form:select>


                    <label for="hoursRead">Hours Read</label>
                    <form:input name="hoursRead" path="hoursRead" required="true"/>


                    <label for="minutesRead">Minutes Read</label>
                    <form:input name="minutesRead" path="minutesRead" required="true"/>


                    <label for="notes">Notes</label>
                    <form:input name="notes" path="notes"/>


                    <label for="doneReading">Completed Reading?</label>
                    <form:select id="doneReading" name="doneReading" path="doneReading" required="true">
                        <option value="${false}">No</option>
                        <option value="${true}">Yes</option>
                    </form:select>

                    <button type="submit">SUBMIT</button>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>

    <!-- /.form-container -->
</div>
<!-- /.container-fluid -->
<!-- End of Page Content -->

<!-- Page level plugins -->


<c:import url="/WEB-INF/jsp/common/footer.jsp" />