package com.techelevator.business;

import com.sun.xml.internal.fastinfoset.tools.TransformInputOutput;
import com.techelevator.dao.SnacksDAOFile;
import com.techelevator.data.Chip;
import com.techelevator.data.Slots;
import com.techelevator.data.Snack;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Map;

import static org.junit.Assert.*;

public class PurchaseManagerTest {

    private PurchaseManager purchaseManager;
    private SnacksDAOFile snackMap = new SnacksDAOFile();

    @Before
    public void setUp() throws Exception {
        purchaseManager = new PurchaseManager(snackMap);
    }

    @Test
    public void stockingMethod() {
        assertEquals(16, snackMap.stockingMethod().size());
    }

    @Test
    public void correctSnack() {
        Map<String, Slots> placeholderTest = snackMap.stockingMethod();
        assertEquals("Chip", placeholderTest.get("A1").getType());
        assertEquals("Candy", placeholderTest.get("B3").getType());
        assertEquals("Drink", placeholderTest.get("C2").getType());
        assertEquals("Gum", placeholderTest.get("D4").getType());
    }

    @Test
    public void feedMoney() {
        purchaseManager.feedMoney(1);
        assertEquals(1, purchaseManager.getBalance(),0.00001);
        purchaseManager.feedMoney(2);
        assertEquals(3, purchaseManager.getBalance(),0.00001);

        //Entering  invalid amount
        purchaseManager.feedMoney(3);
        assertEquals(3, purchaseManager.getBalance(), 0.0001);

        purchaseManager.feedMoney(5);
        assertEquals(8, purchaseManager.getBalance(),0.00001);
        purchaseManager.feedMoney(10);
        assertEquals(18, purchaseManager.getBalance(),0.00001);
    }

    @Test
    public void purchaseProduct() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;

        System.setOut(new PrintStream(outContent));

        String inputOne = "K1";
        String inputTwo = "B3";
        String inputThree = "C2";
        String inputFour = "D4";

        InputStream in_test_one = new ByteArrayInputStream(inputOne.getBytes());
        InputStream in_test_two = new ByteArrayInputStream(inputTwo.getBytes());
        InputStream in_test_three = new ByteArrayInputStream(inputThree.getBytes());
        InputStream in_test_four = new ByteArrayInputStream(inputFour.getBytes());



    }

    @Test
    public void dispenseItem() {
        Snack testSnackOne = new Chip("Cloud Chips", 2.75);

        //balance test
        purchaseManager.feedMoney(10);
        purchaseManager.dispenseItem(testSnackOne);
        assertEquals(7.25, purchaseManager.getBalance(), 0.0001);
    }
}