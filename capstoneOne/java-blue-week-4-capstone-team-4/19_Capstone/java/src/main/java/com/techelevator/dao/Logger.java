package com.techelevator.dao;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

public class Logger {

    public static void auditWriter(String action, double balance, double oldBalance){
        File file = new File("Log.txt");

        try(PrintWriter finalLog = new PrintWriter(new FileWriter(file, true))){

            DateTimeFormatter logOfAction = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss a");
            LocalDateTime newLog = LocalDateTime.now();

            finalLog.println(">" + logOfAction.format(newLog) +" "+ action +
                    " $"+ oldBalance +" $"+ balance);
        }catch (FileNotFoundException exception){
            System.out.println("Vending Machine cannot build a log.");
            return;
        }catch (IOException exception){
            System.out.println("Vending log not available.");
            return;
        }
    }

    public static void salesReport(Map<String, Integer> privateLoggerMap){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy--HH-mm-ss");
        String newLog = dateFormat.format(date);
        String fileName = newLog + "_Sales_Report.txt";
        File file = new File(fileName);

        try(
            PrintWriter sales = new PrintWriter(file)){
            for(String key : privateLoggerMap.keySet()){
                sales.println(key + " | " + privateLoggerMap.get(key));
            }
        }catch (FileNotFoundException exception) {
            System.out.println("Sales Report does not exist.");
            return;
        }
    }
}
