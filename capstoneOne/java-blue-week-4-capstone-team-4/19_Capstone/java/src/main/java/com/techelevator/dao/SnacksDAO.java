package com.techelevator.dao;

import com.techelevator.data.Slots;
import com.techelevator.data.Snack;

import java.util.Map;

public interface SnacksDAO {
    public Map<String, Slots> stockingMethod();
    public Snack correctSnack(String name, double price, String type);
}
