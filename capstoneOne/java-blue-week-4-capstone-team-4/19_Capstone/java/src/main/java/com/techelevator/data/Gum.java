package com.techelevator.data;

public class Gum extends Snack{
    public Gum(String name, double price) {
        super(name, "Chew Chew, Yum!", price);
    }
}
