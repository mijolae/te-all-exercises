package com.techelevator.dao;

import com.techelevator.data.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SnacksDAOFile implements SnacksDAO{
    private Map<String, Slots> map;
    private static Map<String, Integer> privateLoggerMap;

    public SnacksDAOFile() {
        File file = new File("Log.txt");
        if(file.exists()){
            file.delete();
        }
        this.map = new HashMap<>();
        this.privateLoggerMap = new HashMap<>();
    }

    public static Map<String, Integer> getPrivateLoggerMap() {
        return privateLoggerMap;
    }

    public Map<String, Slots> stockingMethod(){
        File file = new File("vendingmachine.csv");

        try(Scanner scan = new Scanner(file)){
            while(scan.hasNext()){
                String snackInput = scan.nextLine();
                String[] partsOfSnack = snackInput.split("\\|");
                Snack snack = correctSnack(partsOfSnack[1],Double.parseDouble(partsOfSnack[2]),partsOfSnack[3]); //will this return the right snack type??
                Slots newSlot = new Slots(snack,partsOfSnack[3],partsOfSnack[0]);
                map.put(partsOfSnack[0], newSlot);
            }

        }catch (FileNotFoundException exception){
            System.out.println("Vending machine does not exist. There is nothing here.");
            System.exit(1);
        }
        return this.map;
    }

    public Snack correctSnack(String name, double price, String type){
        if(type.equals("Chip")){
            return new Chip(name, price);
        }else if(type.equals("Candy")){
            return new Candy(name, price);
        }else if(type.equals("Drink")){
            return new Drink(name, price);
        }else{
            return new Gum(name, price);
        }
    }
}
