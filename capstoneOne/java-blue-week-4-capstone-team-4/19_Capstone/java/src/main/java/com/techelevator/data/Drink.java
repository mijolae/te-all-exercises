package com.techelevator.data;

public class Drink extends Snack{
    public Drink(String name, double price) {
        super(name, "Glug Glug, Yum!", price);
    }
}
