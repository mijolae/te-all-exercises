package com.techelevator;

import com.techelevator.business.PurchaseManager;
import com.techelevator.dao.SnacksDAOFile;
import com.techelevator.data.Slots;
import com.techelevator.view.Menu;

import java.util.Map;

public class VendingMachineCLI {

	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String MAIN_MENU_OPTION_EXIT = "Exit";
	private static final String MAIN_MENU_OPTION_HIDDEN_SALES_REPORT = "Hidden Sales Report";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_OPTION_DISPLAY_ITEMS, MAIN_MENU_OPTION_PURCHASE, MAIN_MENU_OPTION_EXIT, MAIN_MENU_OPTION_HIDDEN_SALES_REPORT };

	private static final String SUB_MENU_OPTION_FEED_MONEY = "Feed Money";
	private static final String SUB_MENU_OPTION_SELECT_PRODUCT = "Select Product";
	private static final String SUB_MENU_OPTION_FINISH_TRANSACTION = "Finish Transaction";
	private static final String[] SUB_MENU_OPTIONS = { SUB_MENU_OPTION_FEED_MONEY, SUB_MENU_OPTION_SELECT_PRODUCT, SUB_MENU_OPTION_FINISH_TRANSACTION};

	private static final String[] MONEY_MENU_OPTIONS = { "$1", "$2", "$5", "$10"};




	private Menu menu;

	public VendingMachineCLI(Menu menu) {
		this.menu = menu;
	}

	public void run() {
		SnacksDAOFile snackMap = new SnacksDAOFile();

		PurchaseManager purchaseManager = new PurchaseManager(snackMap);
		while (true) {
			String choice = (String) menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);
			if (choice.equals(MAIN_MENU_OPTION_DISPLAY_ITEMS)) {
				displayStock(snackMap.stockingMethod());
			} else if (choice.equals(MAIN_MENU_OPTION_PURCHASE)) {
				while(true) {
					System.out.println("Money left: $" + purchaseManager.getBalance());
					String subChoice = (String) menu.getChoiceFromOptions(SUB_MENU_OPTIONS);

					if (subChoice.equals(SUB_MENU_OPTION_FEED_MONEY)) {
						String moneyChoice = (String) menu.getChoiceFromOptions(MONEY_MENU_OPTIONS);
						purchaseManager.feedMoney(Integer.parseInt(moneyChoice.substring(1)));

					}if (subChoice.equals(SUB_MENU_OPTION_SELECT_PRODUCT)) {
						purchaseManager.purchaseProduct();
					}
					if (subChoice.equals(SUB_MENU_OPTION_FINISH_TRANSACTION)) {
						System.out.println(purchaseManager.coinReturn());
						break;
					}
				}
			} else if (choice.equals(MAIN_MENU_OPTION_EXIT)) {
				System.exit(0);
			} else if (choice.equals(MAIN_MENU_OPTION_HIDDEN_SALES_REPORT)) {
				purchaseManager.salesReportWriter();
				System.out.println("SALES REPORT GENERATED");
		    }
		}
	}

	public static void main(String[] args) {
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
		cli.run();
	}

	public static void displayStock(Map<String, Slots> inMap){
		for(String location : inMap.keySet()){
			int stockOut = inMap.get(location).getQuantity();
			String stockMessage;
			if(stockOut == 0){
				stockMessage = "OUT OF STOCK";
			} else {
				stockMessage = String.valueOf(stockOut);
			}
			System.out.println("Slot: " + location + " Name: "+ inMap.get(location).getSnackObject().getName() +
					" Price: $" + inMap.get(location).getSnackObject().getPrice() +
					" Quantity: " + stockMessage);
		}
	}

}
