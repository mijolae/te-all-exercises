package com.techelevator.data;

public class Snack {
    private String name;
    private String foodString;
    private double price;


    public Snack(String name, String foodString, double price) {
        this.name = name;
        this.foodString = foodString;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getFoodString() {
        return foodString;
    }

    public double getPrice() {
        return price;
    }
}
