package com.techelevator.data;

public class Slots {
    private Snack snackObject;
    private String type;
    private String location;
    private int quantity;

    public Slots(Snack snackObject, String type, String location){
        this.snackObject = snackObject;
        this.type = type;
        this.location = location;
        this.quantity = 5;
    }

    public Snack getSnackObject() {
        return snackObject;
    }

    public String getType() {
        return type;
    }

    public String getLocation() {
        return location;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setNewQuantity() {
        this.quantity -= 1;
    }
}
