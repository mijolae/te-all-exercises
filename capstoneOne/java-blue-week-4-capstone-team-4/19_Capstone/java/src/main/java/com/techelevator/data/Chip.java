package com.techelevator.data;

public class Chip extends Snack{
    public Chip(String name, double price) {
        super(name, "Crunch, Crunch, Yum!", price);
    }
}
