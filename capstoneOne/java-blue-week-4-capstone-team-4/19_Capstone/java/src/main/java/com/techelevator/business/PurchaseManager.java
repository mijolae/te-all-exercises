package com.techelevator.business;

import com.techelevator.VendingMachineCLI;
import com.techelevator.dao.Logger;
import com.techelevator.dao.SnacksDAOFile;
import com.techelevator.data.*;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PurchaseManager {
    private double balance;
    private double oldBalance;
    private Map<String, Slots> map;
    private Map<String, Integer> privateLoggerMap;


    public PurchaseManager(SnacksDAOFile snackFile) {
        this.map = snackFile.stockingMethod();
        this.privateLoggerMap = snackFile.getPrivateLoggerMap();
        balance = 0.00;
    }


    public double getBalance() {
        return balance;
    }

    public void feedMoney(int money){  
        if(money != 1 && money != 2 && money != 5 && money != 10){
            System.out.println("Invalid amount. Please enter one of the following amounts: $1, $2, $5, $10 ");
            return;
        }
        oldBalance = balance;
        balance += money;
        balance = Math.round(balance *100) / 100.0;
        Logger.auditWriter("FEED MONEY:", balance, oldBalance);
        oldBalance = balance;
    }

    public void purchaseProduct(){
        Scanner scan = new Scanner(System.in);
        VendingMachineCLI.displayStock(map);
        System.out.print("\nPlease choose an option >>> ");
        String input = scan.next().toUpperCase();
        if(!map.containsKey(input)){
            System.out.println("The vending machine does not recognize that code.");
            return;
        }
        Snack snack = map.get(input).getSnackObject();

        if(map.get(input).getQuantity() == 0){
            System.out.println("This item is sold out.");
            return;
        }else if(balance >= snack.getPrice()){
            dispenseItem(snack);
            Logger.auditWriter(snack.getName()+ " " + input, balance, oldBalance);
            map.get(input).setNewQuantity();
        }else if(balance < snack.getPrice()){
            System.out.println("Balance is too low. Please add more money.");
            return;
        }
    }

    public void dispenseItem(Snack snack){
        oldBalance = balance;
        balance -= snack.getPrice();
        balance = Math.round(balance *100) / 100.0;
        logTheSale(snack);

        System.out.println("Name: " + snack.getName() +
                "\nCost: " + snack.getPrice()+
                "\nMoney Remaining: " + balance +
                "\n" + snack.getFoodString() + "\n");
    }

    public void logTheSale(Snack snack){
        if(privateLoggerMap.containsKey(snack.getName())){
            privateLoggerMap.put(snack.getName(), privateLoggerMap.get(snack.getName()) + 1);
        } else {
            privateLoggerMap.put(snack.getName(), 1);
        }
    }

    public String coinReturn(){
        String coins = "";
        int quarterCount = 0;
        int dimeCount = 0;
        int nickelCount = 0;
        oldBalance = balance;

        while(balance >= 0.25){
            quarterCount++;
            balance -= 0.25;
            balance = Math.round(balance *100) / 100.0;
        }
        while(balance >= 0.10){
            dimeCount++;
            balance -= 0.10;
            balance = Math.round(balance *100) / 100.0;
        }
        while(balance >= 0.05){
            nickelCount++;
            balance -= 0.05;
            balance = Math.round(balance *100) / 100.0;
        }
        balance = 0.00;
        Logger.auditWriter("GIVE CHANGE:", balance, oldBalance);

        return "Quarters: " + quarterCount + "\nDimes: " + dimeCount + "\nNickels: " + nickelCount;
    }

    public void salesReportWriter(){
        Logger.salesReport(privateLoggerMap);
    }

}
