package com.techelevator.data;

public class Candy extends Snack{
    public Candy(String name, double price) {
        super(name, "Munch Munch, Yum!", price);
    }
}
