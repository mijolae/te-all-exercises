package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FindAndReplace {

	public static void main(String[] args) {
		File inputFile = inputFileFromUser();
		File outputFile = outputFileFromUser();
		String findWord = findWord();
		String replacementWord = replacementWord(findWord);
		replacement(inputFile, findWord, replacementWord, outputFile);

	}

	private static File inputFileFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.print("Please enter path to input file: ");
		String path = userInput.nextLine();

		File inputFile = new File(path);
		if(inputFile.exists() == false) {
			System.out.println(path+" does not exist");
			System.exit(1);
		} else if(inputFile.isFile() == false) {
			System.out.println(path+" is not a file");
			System.exit(1);
		}
		return inputFile;
	}

	private static String findWord() {
		Scanner userInput = new Scanner(System.in);
		System.out.print("Please enter a word you'd like to find: ");
		String findWord = userInput.nextLine();

		return findWord;
	}

	private static String replacementWord(String findWord) {
		Scanner userInput = new Scanner(System.in);
		System.out.print("Please enter a replacement word for " + findWord + ": " );
		String replacementWord = userInput.nextLine();

		return replacementWord;
	}

	private static File outputFileFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter path to output file: ");
		String outputPath = userInput.nextLine();

		File outputFile = new File(outputPath);

		try{
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			} else if (outputFile.isFile() == false) {
				System.out.println(outputPath + " is not a file.");
				System.exit(1);
			}
		} catch(IOException exception){
			System.out.println("There was an IO Exception.");
			System.exit(1);
		}

		return outputFile;
	}

	private static void replacement(File inputFile, String findWord, String replacementWord, File outputFile) {
		try (
				Scanner read = new Scanner(inputFile);
				PrintWriter write = new PrintWriter(outputFile)
		) {
			while(read.hasNextLine()) {
				String line = read.nextLine();
				if (line.contains(findWord)) {
					line = line.replace(findWord, replacementWord);
				}
				write.println(line);
			}
		} catch (FileNotFoundException exc) {
			exc.printStackTrace();
		}


	}


}
