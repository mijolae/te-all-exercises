package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) throws FileNotFoundException {

		int wordCount = 0;
		int sentenceCount = 0;
		File inputFile = getFileFromUser();

		try (Scanner fileIn = new Scanner(inputFile)) {

			while (fileIn.hasNextLine()) {
				String line = fileIn.next();
				if (line.endsWith(".")) {
					sentenceCount++;
				}
				if (line.endsWith("!")) {
					sentenceCount++;
				}
				if (line.endsWith("?")) {
					sentenceCount++;
				}
				wordCount++;

			}
			System.out.print("Word Count: " + wordCount + " ");
			System.out.print("Sentence Count: " + sentenceCount);
		}

	}
	private static File getFileFromUser() {

		Scanner filePathFromUser = new Scanner(System.in);
		System.out.println("Enter file path: ");
		File file = new File(filePathFromUser.nextLine());
		if(!(file.exists())){
			System.out.println("File does not exist.");
			System.exit(0);
		}
		return file;
	}
}