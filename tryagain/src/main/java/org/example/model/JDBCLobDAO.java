package org.example.model;

import org.example.entity.Employee;
import org.example.entity.LineOfBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sound.sampled.Line;
import javax.sql.DataSource;
import java.util.ArrayList;

@Component
public class JDBCLobDAO implements LineOfBusinessDAO{
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCLobDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public ArrayList<LineOfBusiness> getAllLOBs() {
        ArrayList<LineOfBusiness> listOfLobs = new ArrayList<>();

        //Security Issue here?
        String sqlGetLobs = "SELECT * FROM lineofbusiness";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetLobs);
        while(results.next()){
            listOfLobs.add(mapRowToLob(results));
        }
        return listOfLobs;
    }

    @Override
    public ArrayList<Employee> getAllEmployeesFromLob(String lobName) {
        ArrayList<Employee> listOfEmployeesFromSpecificLob = new ArrayList<>();

        String sqlGetLobs = "select e.*" +
                " from employee as e" +
                " join lob_employee as le on le.employee_id = e.employee_id" +
                " join lineofbusiness as l on l.lob_id = le.lob_id" +
                " where l.lob_name = ?";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetLobs, lobName);
        while(results.next()){
            listOfEmployeesFromSpecificLob.add(mapRowToEmployee(results));
        }
        return listOfEmployeesFromSpecificLob;
    }

    private LineOfBusiness mapRowToLob(SqlRowSet result){
        LineOfBusiness lob = new LineOfBusiness(result.getString("lob_name"));
        return lob;
    }

    private Employee mapRowToEmployee(SqlRowSet result){
        Employee employeeToReturn = new Employee(result.getString("first_name"),
                result.getString("last_name"),
                result.getString("email"),
                result.getString("location"),
                result.getString("city"),
                result.getString("role"),
                result.getInt("year_hired"));
        return employeeToReturn;
    }
}
