package org.example.model;

import org.example.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;


@Component
public class JDBCEmployeeDAO implements EmployeeDAO{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCEmployeeDAO(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public int addNewEmployee(Employee employee) {
        if(employeeAlreadyInDatabase(employee)){
            return -1;
        }
        String sqlAddEmployeeToDatabase = "INSERT INTO employee(first_name, last_name, email, location, city, role, year_hired)"+
                " VALUES (?,?,?,?,?,?,?)";

        jdbcTemplate.update(sqlAddEmployeeToDatabase,
                employee.getFirstName(),
                employee.getLastName(),
                employee.getEmail(),
                employee.getLocation(),
                employee.getCity(),
                employee.getRole(),
                employee.getYear());

        setEmployeeNewId(employee);
        return 0;
    }

    @Override
    public Employee searchForEmployeeByEmail(String email) {
        String sqlSearchForEmployee = "SELECT *"+
                                        " FROM employee"+
                                        " WHERE email = ?";

        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSearchForEmployee, email);
        if(result.next()){
            Employee employee = mapRowToEmployee(result);
            return employee;
        }
        return null;

    }

    @Override
    public Employee searchForEmployeeByName(String firstName, String lastName) {
        String sqlSearchForEmployee = "SELECT *"+
                " FROM employee"+
                " WHERE first_name ILIKE ? OR last_name ILIKE ?";

        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSearchForEmployee, "%"+ firstName +"%","%"+ lastName +"%");
        if(result.next()){
            Employee employee = mapRowToEmployee(result);
            return employee;
        }
        return null;
    }

    @Override
    public ArrayList<Employee> getEmployeeList(){
        ArrayList<Employee> allEmployees = new ArrayList<>();
        //Security Risk
        String sqlGetListOfAllEmployees = "SELECT *"+
                " FROM employee";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetListOfAllEmployees);
        while(results.next()){
            allEmployees.add(mapRowToEmployee(results));
        }
        return allEmployees;
    }

    @Override
    public void addEmployeeToLob(Employee employee, String lob_name) {
        String sqlAddEmployeeToLob = "INSERT into lob_employee(lob_id, employee_id)" +
                " Values((select lob_id" +
                " from lineofbusiness as lob" +
                " where lob.lob_name ILIKE ?), (select employee_id from employee as e where e.employee_id = ?))";
        jdbcTemplate.update(sqlAddEmployeeToLob,"%" + lob_name + "%", employee.getEmployeeId());
    }

    private Employee mapRowToEmployee(SqlRowSet result){
        Employee employeeToReturn = new Employee(result.getString("first_name"),
                result.getString("last_name"),
                result.getString("email"),
                result.getString("location"),
                result.getString("city"),
                result.getString("role"),
                result.getInt("year_hired"));
        employeeToReturn.setEmployeeId(result.getLong("employee_id"));
        return employeeToReturn;
    }

    private void setEmployeeNewId(Employee employee){
        String sqlGetEmployeeId = "Select employee_id from employee where first_name ILIKE ? AND last_name ILIKE ?";

        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetEmployeeId, "%" + employee.getFirstName() + "%", "%" + employee.getLastName() + "%" );
        if(!result.next()){
            System.out.println(result);
        }
        employee.setEmployeeId(result.getLong("employee_id"));
    }

    private boolean employeeAlreadyInDatabase(Employee employee){
        String sqlSearchIfEmployeeExists = "Select * from employee where first_name ILIKE ? AND last_name ILIKE ? AND email ILIKE ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSearchIfEmployeeExists, employee.getFirstName(),
                employee.getLastName(), employee.getEmail());

        if(!result.next()){
            return false;
        }
        return true;
    }
}
