package org.example.model;

import org.example.entity.Employee;

import java.util.ArrayList;

public interface EmployeeDAO {
    public int addNewEmployee(Employee employee);
    public Employee searchForEmployeeByEmail(String email);
    public Employee searchForEmployeeByName(String firstName, String lastName);
    public ArrayList<Employee> getEmployeeList();
    public void addEmployeeToLob(Employee employee, String lob_name);
}

