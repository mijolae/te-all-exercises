package org.example.model;

import org.example.entity.Employee;
import org.example.entity.LineOfBusiness;

import java.util.ArrayList;

public interface LineOfBusinessDAO {
    public ArrayList<LineOfBusiness> getAllLOBs();
    public ArrayList<Employee> getAllEmployeesFromLob(String lobName);
}
