package org.example.controllers;

import org.example.entity.Employee;
import org.example.model.EmployeeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class EmployeeController {

    private EmployeeDAO employeeDAO;

    @Autowired
    public EmployeeController(EmployeeDAO employeeDAO){
        this.employeeDAO = employeeDAO;
    }

    @RequestMapping(path="/add", method = RequestMethod.GET)
    public String showAddForm(ModelMap modelHolder){
        if(!modelHolder.containsAttribute("employee")){
            modelHolder.put("employee", new Employee());
        }
        modelHolder.addAttribute("employee", new Employee());
        return "add";
    }

    @RequestMapping(path="/add", method = RequestMethod.POST)
    public String processNewEmployee(
            @Valid @ModelAttribute Employee employee,
            BindingResult result,
            RedirectAttributes flash){
        flash.addFlashAttribute("employee", employee);

        if(result.hasErrors()){
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "add", result);
            return "redirect:/add";
        }
        //re test this
        int num = employeeDAO.addNewEmployee(employee);
        if(num == -1){
            return "addNewEmployee";
        }
        return "success";
    }

    @RequestMapping(path="/search", method = RequestMethod.GET)
    public String showSearchPage(ModelMap modelHolder){
        if(!modelHolder.containsAttribute("search")){
            modelHolder.addAttribute("search", new Employee());
        }
        return "search";
    }

    @RequestMapping(path="/search", method = RequestMethod.POST)
    public String searchForEmployee(
            @Valid @ModelAttribute Employee employee,
            BindingResult result,
            RedirectAttributes flash,
            @RequestParam(name="emailSearch") String email,
            @RequestParam(name="nameSearch") String name,
            ModelMap modelMap) {
        flash.addFlashAttribute("search", employee);

        if (result.hasErrors()) {
            flash.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "search", result);
            return "redirect:/search";
        }

        if(email != null){
            modelMap.addAttribute("foundEmployee", employeeDAO.searchForEmployeeByEmail(employee.getEmail()));
        }else if(name != null){
            modelMap.addAttribute("foundEmployee", employeeDAO.searchForEmployeeByName(employee.getFirstName(), employee.getLastName()));
        }

        return "profile";
    }

    @RequestMapping(path = "/allTechConnectors", method = RequestMethod.GET)
    public String showAllEmployees(ModelMap modelMap){
        if(!modelMap.containsAttribute("allEmployees")){
            modelMap.put("allEmployees", new Employee());
        }
        modelMap.addAttribute("allEmployees", employeeDAO.getEmployeeList());
        return "allTechConnectors";
    }
}
