package org.example.entity;

import java.util.ArrayList;

public class LineOfBusiness {
    private String lineOfBusinessName;


    public LineOfBusiness(String lineOfBusinessName) {
        this.lineOfBusinessName = lineOfBusinessName;
    }

    public String getLineOfBusinessName() {
        return lineOfBusinessName;
    }

    public void setLineOfBusinessName(String lineOfBusinessName) {
        this.lineOfBusinessName = lineOfBusinessName;
    }

}
