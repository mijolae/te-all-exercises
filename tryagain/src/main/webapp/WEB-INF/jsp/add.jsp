<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 9/4/2020
  Time: 9:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:url var="add" value="/add"/>
<form:form action="/add" method="POST" modelAttribute="employee">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <fieldset>
        <legend>Add A Tech Connector</legend>

        <div class="formgrid">
            <form:input id="firstName" name="firstName" path="firstName" required="true"/>
            <label for="firstName">First Name: </label>

            <form:input id="lastName" name="lastName" path="lastName" required="true"/>
            <label for="lastName">Last Name: </label>

            <form:input type ="email" id="email" name="email" path="email" required="true"/>
            <label for="email">Email: </label>

            <form:select id="location" name="location" path="location" required="true">
                <option value="namr">NAMR</option>
                <option value="emea">EMEA</option>
                <option value="apac">APAC</option>
                <option value="latam">LATAM</option>
            </form:select>
            <label for="location">Location: </label>

            <form:select id="city" name="city" path="city" required="true">
                <option value="new york metro" data-type="namr">New York Metro</option>
                <option value="delaware" data-type="namr">Delaware</option>
                <option value="tampa" data-type="namr">Tampa</option>
                <option value="chicago" data-type="namr">Chicago</option>
                <option value="plano" data-type="namr">Plano</option>
            </form:select>
            <label for="city">City: </label>

            <form:select id="role" name="role" path="role" required="true">
                <option value="admin">Administrator</option>
                <option value="employee">Tech Connector</option>
            </form:select>
            <label for="role">Role: </label>

            <form:input id="year" name="year" path="year" min = "1950" max="2100" type = "number" value="2020" required="true"/>
            <label for="year">Year Hired: </label>

            <button type="submit">SUBMIT</button>
        </div>
    </fieldset>
</form:form>
</body>
</html>
