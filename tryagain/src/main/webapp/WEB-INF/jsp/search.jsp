<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 9/4/2020
  Time: 10:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> SEARCH PAGE</title>
</head>
<body>
<c:url var="search" value="/search"/>
<form:form action="/search" method="POST" modelAttribute="search">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <fieldset>
        <legend>Search For A Tech Connector By Name</legend>

        <div class="formgrid">
            <form:input id="firstName" name="firstName" path="firstName" required="true"/>
            <label for="firstName">First Name: </label>

            <form:input id="lastName" name="lastName" path="lastName" required="true"/>
            <label for="lastName">Last Name: </label>

            <button type="submit" name="nameSearch">SUBMIT</button>
        </div>
    </fieldset>
    <fieldset>
        <legend>Search For A Tech Connector By Email</legend>

        <div class="formgrid">
            <form:input id="email" name="email" path="email" required="true"/>
            <label for="email">Email: </label>

            <button type="submit" name="emailSearch">SUBMIT</button>
        </div>
    </fieldset>
</form:form>
</body>
</html>
