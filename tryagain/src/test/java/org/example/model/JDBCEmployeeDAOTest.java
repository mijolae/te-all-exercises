package org.example.model;

import org.example.entity.Employee;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class JDBCEmployeeDAOTest {

    private static SingleConnectionDataSource dataSource;
    private JDBCEmployeeDAO dao;
    private JDBCLobDAO lobDAO;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/application");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    /* After all tests have finished running, this method will close the DataSource */
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setup() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        dao = new JDBCEmployeeDAO(dataSource);
        lobDAO = new JDBCLobDAO(dataSource);
    }

    /* After each test, we rollback any changes that were made to the database so that
     * everything is clean for the next test */
    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void addNewEmployee() {

        Employee newEmployee = new Employee("Elmo",
                "Wrin","elmo@gmail.com",
                "NAMR","Chicago","employee",2016);

        dao.addNewEmployee(newEmployee);
        assertNotEquals(null, dao.getEmployeeList());
        assertEquals(6, dao.getEmployeeList().size());

    }

    @Test
    public void searchForEmployeeByEmail() {
        Employee newEmployee = new Employee("Mijolae",
                "Wright","mijolae@gmail.com",
                null,null,"employee",2020);

        Employee checkEmployee = dao.searchForEmployeeByEmail("mijolae@gmail.com");
        assertNotEquals(null, checkEmployee);

        //How to assert objects are the same?
        assertTrue(checkEmployee.getFirstName().equals(newEmployee.getFirstName()));
    }

    @Test
    public void searchForEmployeeByName() {
        Employee newEmployee = new Employee("Mijolae",
                "Wright","mijolae@gmail.com",
                null,null,"employee",2020);

        Employee checkEmployee = dao.searchForEmployeeByName("Mijolae", "Wright");
        assertNotEquals(null, checkEmployee);

        assertTrue(checkEmployee.getFirstName().equals(newEmployee.getFirstName()));

    }

    @Test
    public void addEmployeeToLob() {
        Employee newEmployee = new Employee("Elmo",
                "Wrin","elmo@gmail.com",
                "NAMR","Chicago","employee",2016);

        dao.addNewEmployee(newEmployee);

        dao.addEmployeeToLob(newEmployee, "Private Banking");
        ArrayList<Employee> shouldBeTwoInPrivateBanking = lobDAO.getAllEmployeesFromLob("Private Banking");

        assertNotEquals(null, shouldBeTwoInPrivateBanking);
        assertEquals(2,shouldBeTwoInPrivateBanking.size());

    }
}