package org.example.model;

import org.example.entity.Employee;
import org.example.entity.LineOfBusiness;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class JDBCLobDAOTest {

    private static SingleConnectionDataSource dataSource;
    private JDBCLobDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/application");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    /* After all tests have finished running, this method will close the DataSource */
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setup() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        dao = new JDBCLobDAO(dataSource);
    }

    /* After each test, we rollback any changes that were made to the database so that
     * everything is clean for the next test */
    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getAllLOBs() {
        ArrayList<LineOfBusiness> allLobs = dao.getAllLOBs();
        assertNotEquals(null, allLobs);
        assertEquals(4,allLobs.size());
    }

    @Test
    public void getAllEmployeesFromLob() {
        ArrayList<Employee> allEmployeesInPrivate = dao.getAllEmployeesFromLob("Private Banking");
        assertNotEquals(null, allEmployeesInPrivate);
        assertEquals(1, allEmployeesInPrivate.size());
    }

}