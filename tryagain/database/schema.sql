/*BEGIN;

-- CREATE statements go here
DROP TABLE IF EXISTS employee, lineofbusiness, lob_employee;

CREATE TABLE employee (
employee_id SERIAL PRIMARY KEY,
first_name varchar(32) NOT NULL,
last_name varchar(32) NOT NULL,
email varchar(32) NOT NULL,
picture varchar(32),
location varchar(32),
city varchar(32),
role varchar(32) NOT NULL,
year_hired int NOT NULL
);

CREATE TABLE lineofbusiness (
lob_id SERIAL PRIMARY KEY,
lob_name varchar(32) NOT NULL

);

CREATE TABLE lob_employee (
lob_id int NOT NULL,
employee_id bigint NOT NULL UNIQUE,

constraint fk_lob foreign key (lob_id) references lineofbusiness (lob_id),
constraint fk_employee foreign key (employee_id) references employee (employee_id)
);

INSERT INTO employee(first_name, last_name, email, role, year_hired)
VALUES('Christen', 'Hill', 'christen@gmail.com', 'admin', 2016),
        ('Clinton', 'Asalu', 'clinton@gmail.com', 'employee', 2020),
        ('Ece', 'Batmaz', 'eceb@gmail.com', 'employee', 2020),
        ('Jodi', 'Na', 'jodi@gmail.com', 'admin', 2017),
        ('Mijolae', 'Wright', 'mijolae@gmail.com', 'employee', 2020);
        
INSERT INTO lineofbusiness(lob_name)
VALUES ('Corporate Client Banking'),('Private Banking'),('Corporate Investment Banking'),('Asset Management');

INSERT INTO lob_employee(lob_id, employee_id)
VALUES(4,1),
        (1,2),
        (2,3),
        (3,4);


COMMIT;*/

INSERT into lob_employee(lob_id, employee_id)
                Values((select lob_id from lineofbusiness where lob_name ILIKE '%Corporate Clien%'), (select employee_id from employee where employee.employee_id = 5))