<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<div id="weightBox">
    <h1>Alien Weight Calculator</h1>
    <form method="GET" action="">
        <div class="formInputGroup">
            <label for="planetChoice">Choose a planet:</label>
            <select name="planetChoice" id="planetChoice">
                <option value="Mercury">Mercury</option>
                <option value="Venus">Venus</option>
                <option value="Earth">Earth</option>
                <option value="Mars">Mars</option>
                <option value="Jupiter">Jupiter</option>
                <option value="Saturn">Saturn</option>
                <option value="Uranus">Uranus</option>
                <option value="Neptune">Neptune</option>
                <option value="Pluto">Pluto</option>
            </select>
        </div>

        <div class="formInputGroup">
            <label for="weight">Enter your weight:</label>
            <input type="text" name="weight" id="weight"/>
        </div>

        <button class="formSubmitButton" type="submit" value="Calculate Weight"><a href="?planetChoice=${weightCalculator.planetChosen}&weight=${weightCalculator.enteredWeight}">Calculate Weight</a></button>
    </form>
</div>