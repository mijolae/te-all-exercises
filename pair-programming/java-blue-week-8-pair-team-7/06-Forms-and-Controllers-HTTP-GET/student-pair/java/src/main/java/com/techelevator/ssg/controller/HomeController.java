package com.techelevator.ssg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String displayHomePage() {
		return "homePage";
	}

	@RequestMapping(value = "/alienWeight", method = RequestMethod.GET)
	public String displayAlienWeight(){
		return "alienWeight";
	}

	@RequestMapping(value = "/alienWeight", method = RequestMethod.POST)
	public String calculateAlienWeight(@RequestParam String planetChoice,
									   @RequestParam String weight){

		return "redirect:/alienWeightResults";
	}




}
