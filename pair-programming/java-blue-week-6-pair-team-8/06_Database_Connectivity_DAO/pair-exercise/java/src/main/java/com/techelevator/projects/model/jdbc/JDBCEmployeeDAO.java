package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Department;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCEmployeeDAO implements EmployeeDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCEmployeeDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employees = new ArrayList<>();
		String sqlGetAllEmployees = "SELECT * "+
				"FROM employee";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllEmployees);
		while(results.next()) {
			Employee placeholder = new Employee(results.getLong("employee_id"), results.getLong("department_id"),
					results.getString("first_name"),results.getString("last_name"),results.getDate("birth_date").toLocalDate(),results.getString("gender").charAt(0),
					results.getDate("hire_date").toLocalDate());
			//Department theDepartment = createDepartment(placeholder);
			employees.add(placeholder);
		}
		return employees;

	}

	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {
		List<Employee> employees = new ArrayList<>();
		String sqlSearchDepartmentsByName = "SELECT * "+
				"FROM employee " +
				"WHERE first_name ILIKE ? AND last_name ILIKE ?"; //edit to fuzzy match
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchDepartmentsByName, firstNameSearch,lastNameSearch);
		while(results.next()) {
			Employee placeholder = new Employee(results.getLong("employee_id"), results.getLong("department_id"),
					results.getString("first_name"),results.getString("last_name"),results.getDate("birth_date").toLocalDate(),results.getString("gender").charAt(0),
					results.getDate("hire_date").toLocalDate());
			//Department theDepartment = createDepartment(placeholder);
			employees.add(placeholder);
		}
		return employees;
	}

	@Override
	public List<Employee> getEmployeesByDepartmentId(long id) {
		List<Employee> employees = new ArrayList<>();
		String sqlSearchEmployeesById = "SELECT * "+
				"FROM employee " +
				"WHERE department_id = ?";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchEmployeesById, id);

		while(results.next()) {
			Employee placeholder = new Employee(results.getLong("employee_id"), results.getLong("department_id"),
					results.getString("first_name"),results.getString("last_name"),results.getDate("birth_date").toLocalDate(),results.getString("gender").charAt(0),
					results.getDate("hire_date").toLocalDate());
			//Department theDepartment = createDepartment(placeholder);
			employees.add(placeholder);
		}
		return employees;
	}

	@Override
	public List<Employee> getEmployeesWithoutProjects() {
		List<Employee> employees = new ArrayList<>();
		String sqlSearchEmployeesById = "SELECT * "+
				"FROM employee " +
				"WHERE employee.employee_id NOT IN (SELECT project_employee.employee_id FROM project_employee) ";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchEmployeesById);

		while(results.next()) {
			Employee placeholder = new Employee(results.getLong("employee_id"), results.getLong("department_id"),
					results.getString("first_name"),results.getString("last_name"),results.getDate("birth_date").toLocalDate(),results.getString("gender").charAt(0),
					results.getDate("hire_date").toLocalDate());
			//Department theDepartment = createDepartment(placeholder);
			employees.add(placeholder);
		}
		return employees;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {
		List<Employee> employees = new ArrayList<>();
		String sqlSearchEmployeesById = "SELECT * "+
				"FROM employee " +
				"WHERE employee.employee_id IN (SELECT project_employee.employee_id FROM project_employee WHERE project_id = ?)";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchEmployeesById, projectId);

		while(results.next()) {
			Employee placeholder = new Employee(results.getLong("employee_id"), results.getLong("department_id"),
					results.getString("first_name"),results.getString("last_name"),results.getDate("birth_date").toLocalDate(),results.getString("gender").charAt(0),
					results.getDate("hire_date").toLocalDate());
			//Department theDepartment = createDepartment(placeholder);
			employees.add(placeholder);
		}
		return employees;
	}

	@Override
	public void changeEmployeeDepartment(Long employeeId, Long departmentId) {
		String sqlChangeEmployeeDepartment = "UPDATE employee "+
				"SET department_id = ? " +
				"WHERE employee_id = ?";
		jdbcTemplate.update(sqlChangeEmployeeDepartment, departmentId, employeeId);
		
	}

}
