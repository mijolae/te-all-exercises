package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.ProjectDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCProjectDAO implements ProjectDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCProjectDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Project> getAllActiveProjects() {
		List<Project> projectList = new ArrayList<>();
		String sqlActiveProjects = "SELECT * "+
									"FROM project "+
									"WHERE to_date >= current_date";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlActiveProjects);

		while(results.next()){
			Project project = mapRowToProject(results);
			projectList.add(project);
		}
		return projectList;
	}

	@Override
	public void removeEmployeeFromProject(Long projectId, Long employeeId) {
		String sqlRemoveEmployee = "DELETE FROM project_employee "+
									"WHERE employee_id = ? AND project_id = ?";
		jdbcTemplate.update(sqlRemoveEmployee, employeeId,projectId);
	}

	@Override
	public void addEmployeeToProject(Long projectId, Long employeeId) {
		String sqlAddEmployee = "INSERT INTO project_employee (project_id, employee_id) "+
				"VALUES (?, ?)";
		jdbcTemplate.update(sqlAddEmployee, projectId, employeeId);
	}

	public Project mapRowToProject(SqlRowSet rowSet){
		Project project;
		project = new Project(rowSet.getLong("project_id"), rowSet.getString("name"), rowSet.getDate("from_date").toLocalDate(), rowSet.getDate("to_date").toLocalDate());
		return project;
	}

}
