package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Project;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCDepartmentDAO implements DepartmentDAO {
	
	private JdbcTemplate jdbcTemplate;

	public JDBCDepartmentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Department> getAllDepartments() {
		List<Department> departments = new ArrayList<>();
		String sqlGetAllDepartments = "SELECT * "+
										"FROM department";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllDepartments);
		while(results.next()){
			Department department = mapRowToDepartment(results);
			departments.add(department);
		}
		return departments;
	}

	@Override
	public List<Department> searchDepartmentsByName(String nameSearch) {
		List<Department> departments = new ArrayList<>();
		String sqlSearchDepartmentsByName = "SELECT * "+
									"FROM department " +
									"WHERE name ILIKE ?"; //edit to fuzzy match
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchDepartmentsByName, nameSearch);
		while(results.next()){
			Department department = mapRowToDepartment(results);
			departments.add(department);
		}
		return departments;
	}

	@Override
	public void saveDepartment(Department updatedDepartment) {
		String sqlSearchDepartmentsByName = "UPDATE department "+
											"SET name = ? " +
											"WHERE department_id = ?";
		jdbcTemplate.update(sqlSearchDepartmentsByName, updatedDepartment.getName(), updatedDepartment.getId());
	}

	@Override
	public Department createDepartment(Department newDepartment) {
		String sqlSearchDepartmentsByName = "INSERT INTO department (name) "+
											"VALUES (?) ";

		jdbcTemplate.update(sqlSearchDepartmentsByName, newDepartment.getName());
		List<Department> department = new ArrayList<>();
		department = searchDepartmentsByName(newDepartment.getName());
		return department.get(0);
	}

	@Override
	public Department getDepartmentById(Long id) {
		String sqlSearchDepartmentsByName = "SELECT * "+
											"FROM department " +
											"WHERE department_id= ?";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchDepartmentsByName, id);
		Department placeholder = null;
		if(results.next()) {
			placeholder = mapRowToDepartment(results);
		}
		return placeholder;
	}

	public Department mapRowToDepartment(SqlRowSet rowSet){
		Department department;
		department = new Department(rowSet.getLong("department_id"), rowSet.getString("name"));
		return department;
	}

}
