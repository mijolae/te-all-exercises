package com.techelevator.projects.model.jdbc;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.jdbc.JDBCEmployeeDAO;
import com.techelevator.projects.model.Project;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class JDBCProjectDAOTest {
    private static SingleConnectionDataSource dataSource;
    private JDBCProjectDAO dao;
    private JDBCEmployeeDAO daoTwo;

    @BeforeClass
    public static void setupDatasource() throws Exception {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5433/projects");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() {
        String sqlInsertProject = "INSERT INTO project (name, from_date, to_date) VALUES ('BlackPink Takeover', '2016-08-12', '2026-12-21')";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sqlInsertProject);
        dao = new JDBCProjectDAO(dataSource);
        daoTwo = new JDBCEmployeeDAO(dataSource);
    }

    @After
    public void tearDown() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getAllActiveProjects() {
        List<Project> actual = dao.getAllActiveProjects();
        assertEquals(2, actual.size());
        //check for Plan 9 since the id would never change
        Project project = new Project(6l,"Plan 9", LocalDate.of(2014,10,1), LocalDate.of(2020,12,31));
        assertProjectsAreEqual(project, actual.get(0));
    }

    @Test
    public void removeEmployeeFromProject() {
        dao.removeEmployeeFromProject(6l, 5l);
        List<Employee> employeeList = daoTwo.getEmployeesByProjectId(6l);

        assertEquals(2, employeeList.size());

    }

    @Test
    public void addEmployeeToProject() {
        //Employee employee = new Employee(13l, 2l, "Jennie", "Kim", LocalDate.of(1999,03,12), 'f',LocalDate.of(2015,3,12));
        dao.addEmployeeToProject(6l, 4l);
        List<Employee> employeeList = daoTwo.getEmployeesByProjectId(6l);

        assertEquals(4, employeeList.size());

    }

    private void assertProjectsAreEqual(Project expected, Project actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
    }

}