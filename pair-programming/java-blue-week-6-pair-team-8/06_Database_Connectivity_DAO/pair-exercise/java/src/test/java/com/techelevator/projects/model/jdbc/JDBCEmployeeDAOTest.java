package com.techelevator.projects.model.jdbc;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.Employee;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class JDBCEmployeeDAOTest {


    private static SingleConnectionDataSource dataSource;
    private JDBCEmployeeDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5433/projects");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false); //since transactions are automatically commit, we want to disable that
    }

    /* After all tests have finished running, this method will close the DataSource */
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() {
        dao = new JDBCEmployeeDAO(dataSource);
    }

    @After
    public void tearDown() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getAllEmployees() {
        List<Employee> actual = dao.getAllEmployees();
        assertEquals(12, actual.size());

        Employee employee = getEmployee(1L, null, "Fredrick", "Keppard",    LocalDate.parse("1953-07-15"), 'M', LocalDate.parse("2001-04-01"));

        assertEmployeesAreEqual(employee, actual.get(0));
    }

    @Test
    public void searchEmployeesByName() {
        List<Employee> actual = dao.searchEmployeesByName("flo", "henderson");
        List<Employee> expected = new ArrayList<>();
        Employee employee = getEmployee(2L, 1L, "Flo", "Henderson",    LocalDate.parse("1990-12-28"), 'F', LocalDate.parse("2011-08-01"));
        expected.add(employee);
        assertEquals(expected.size(), actual.size());
    }

    @Test
    public void getEmployeesByDepartmentId() {
        List<Employee> actual = dao.getEmployeesByDepartmentId(4L);
        List<Employee> expected = new ArrayList<>();
        Employee employee = getEmployee(11L, 4L, "Jarred", "Lukach",    LocalDate.parse("1981-09-25"), 'M', LocalDate.parse("2003-05-01"));
        Employee employee1 = getEmployee(12L, 4L, "Gabreila", "Christie",    LocalDate.parse("1980-03-17"), 'F', LocalDate.parse("1999-08-01"));
        expected.add(employee);
        expected.add(employee1);

        assertEquals(expected.size(), actual.size());
        assertEmployeesAreEqual(employee, actual.get(0));
        assertEmployeesAreEqual(employee1, actual.get(1));
    }

    @Test
    public void getEmployeesWithoutProjects() {
        List<Employee> actual = dao.getEmployeesWithoutProjects();
        List<Employee> expected = new ArrayList<>();
        Employee employee = getEmployee(4L, 2L, "Delora", "Coty",    LocalDate.parse("1976-07-04"), 'F', LocalDate.parse("2009-03-01"));
        Employee employee1 = getEmployee(12L, 4L, "Gabreila", "Christie",    LocalDate.parse("1980-03-17"), 'F', LocalDate.parse("1999-08-01"));
        expected.add(employee);
        expected.add(employee1);

        assertEquals(expected.size(), actual.size());
        assertEmployeesAreEqual(employee, actual.get(0));
        assertEmployeesAreEqual(employee1, actual.get(1));
    }

    @Test
    public void getEmployeesByProjectId() {
        List<Employee> actual = dao.getEmployeesByProjectId(4L);
        List<Employee> expected = new ArrayList<>();
        Employee employee = getEmployee(2L, 1L, "Flo", "Henderson",    LocalDate.parse("1990-12-28"), 'F', LocalDate.parse("2011-08-01"));
        Employee employee1 = getEmployee(6L, 3L, "Mary Lou", "Wolinski",    LocalDate.parse("1983-04-08"), 'F', LocalDate.parse("2012-04-01"));
        expected.add(employee);
        expected.add(employee1);

        assertEquals(expected.size(), actual.size());
        assertEmployeesAreEqual(employee, actual.get(0));
        assertEmployeesAreEqual(employee1, actual.get(1));
    }

    @Test
    public void changeEmployeeDepartment() {
        dao.changeEmployeeDepartment(2L, 3L);
        List<Employee> actual = dao.getEmployeesByDepartmentId(3L);
        assertEquals(6, actual.size());
    }

    private Employee getEmployee(Long employeeId, Long departmentId, String firstName, String lastName, LocalDate birthDay, char gender, LocalDate hireDate) {
        Employee employee = new Employee(employeeId, departmentId, firstName, lastName, birthDay, gender, hireDate) ;
        return employee;
    }

    private void assertEmployeesAreEqual(Employee expected, Employee actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDay(), actual.getBirthDay());
        assertEquals(expected.getGender(), actual.getGender());
        assertEquals(expected.getHireDate(), actual.getHireDate());
    }
}

