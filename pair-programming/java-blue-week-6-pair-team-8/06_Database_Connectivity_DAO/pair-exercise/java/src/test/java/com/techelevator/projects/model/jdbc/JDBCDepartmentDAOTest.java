package com.techelevator.projects.model.jdbc;

import com.techelevator.projects.model.Department;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class JDBCDepartmentDAOTest {

    private static SingleConnectionDataSource dataSource;
    private JDBCDepartmentDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5433/projects");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false); //since transactions are automatically commit, we want to disable that
    }

    /* After all tests have finished running, this method will close the DataSource */
    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() {
        String sqlInsertDepartment = "INSERT INTO department(name) VALUES ('Department of Baddies')";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sqlInsertDepartment);
        dao = new JDBCDepartmentDAO(dataSource);
    }

    @After
    public void tearDown() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void getAllDepartments() {
        List<Department> actual = dao.getAllDepartments();
        assertEquals(5, actual.size());

        Department expectedDept = getDepartment(1L, "Department of Redundancy Department");
        assertDepartmentsAreEqual(expectedDept, actual.get(0));
    }

    @Test
    public void searchDepartmentsByName() {
        List<Department> actual = new ArrayList<>();
        Department department = getDepartment(1L, "Department of Redundancy Department");
        actual.add(department);
        List<Department> expected = dao.searchDepartmentsByName("Department of Redundancy Department");

        assertEquals(actual.size(), expected.size());

    }

    @Test
    public void saveDepartment() throws SQLException {
        Department department = getDepartment(4L, "Tech Connectors");
        dao.saveDepartment(department);

        Department newDept = dao.getDepartmentById(department.getId());
        assertNotEquals(null, department.getId());
        assertDepartmentsAreEqual(department, newDept);
    }

    @Test
    public void createDepartment() {
        Department department = getDepartment(10L, "Keep your money");
        dao.createDepartment(department);

        assertEquals(6, dao.getAllDepartments().size());
    }

    @Test
    public void getDepartmentById() {
       Department actual = getDepartment(1L, "Department of Redundancy Department");
       Department expected = dao.getDepartmentById(1L);

       assertDepartmentsAreEqual(actual, expected);
    }

    @Test
    public void mapRowToDepartment() {
    }

    private Department getDepartment(Long id, String name) {
        Department department = new Department(id, name);
        return department;
    }

    private void assertDepartmentsAreEqual(Department expected, Department actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
    }
}
