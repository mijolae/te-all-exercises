<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Recipe Table View</title>
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
        <ul>
            <li><a href="recipeTiles">Tile Layout</a></li>
            <li><a href="recipeTable">Table Layout</a></li>
        </ul>
        
    </nav>
    <section id="Table-main-content">
        <h1 style="text-align: center; padding-bottom: 20px">Recipes</h1>

        <section id="tableSectionGrid">
            <div id="titles">
                <p id="text-name">Name</p>
                <p id="text-type">Type</p>
                <p id="text-time">Cook Time</p>
                <p id="text-ingred">Ingredients</p>
                <p id="text-rating">Rating</p>
            </div>
       <!-- Use the request attribute "recipes" (List<Recipe>) -->
        <c:forEach var="recipe" items="${requestScope.recipes}">
<%--            <div class="tableSpecificRecipe">--%>
                <div id="tableRecipePic" style="grid-area: pic${recipe.recipeId}">
                    <img src="img/recipe${recipe.recipeId}.jpg" alt="Recipe Image"/>
                </div>




            <div id="tableRecipeName" style="grid-area: name${recipe.recipeId}">
                <p>${recipe.name}</p>
            </div>

            <div id="tableRecipeType" style="grid-area: type${recipe.recipeId}">
                <p>${recipe.recipeType}</p>
            </div>

            <div id="tableRecipeTime" style="grid-area: rate${recipe.recipeId}">
                <p>${recipe.cookTimeInMinutes} min</>
            </div>

            <div id="tableNumberOfIngredients" style="grid-area: ingredients${recipe.recipeId}">
                <p>
                        ${recipe.ingredients.size()} ingredients
                </p>
            </div>

            <div id="tableRating" style="grid-area: rating${recipe.recipeId}">
                    <img src="img/${Math.round(recipe.averageRating)}-star.png"/>
            </div>
<%--        </div>--%>
        </c:forEach>

    </section>
</body>
</html>