<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>Recipe List View</title>
    <link rel="stylesheet" href="css/site.css"/>
</head>
<body>
<header>
    <h1>MVC Exercises - Views Part 2: Models</h1>
</header>
<nav>
    <ul>
        <li><a href="recipeTiles">Tile Layout</a></li>
        <li><a href="recipeTable">Table Layout</a></li>
    </ul>

</nav>
<section id="main-content">
    <h1 style="text-align: center; padding-bottom: 20px">Recipes</h1>
    <div id="sectionGrid">

        <!-- Use the request attribute "recipes" (List<Recipe>) -->


        <c:forEach var="recipe" items="${requestScope.recipes}">
            <div class="specificRecipe">
                <div id="recipePic">
                    <img src="img/recipe${recipe.recipeId}.jpg" alt="Recipe Image"/>
                    <!--<td>${recipe.ingredients.size()}</td>-->
                </div>

            </div>

            <div id="recipeName" style="grid-area: name${recipe.recipeId}">
                <h3>${recipe.name}</h3>
            </div>

            <div class="rateAndIngredients" style="grid-area: rate${recipe.recipeId}">
                <div id="rating">
                    <img src="img/${Math.round(recipe.averageRating)}-star.png"/>
                </div>
                <div id="numberOfIngredients">
                    <p>
                            ${recipe.ingredients.size()} ingredients
                    </p>
                </div>
            </div>
        </c:forEach>
    </div>
</section>
</body>
</html>