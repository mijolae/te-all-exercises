-- Write queries to return the following:
-- Make the following changes in the "world" database.

-- 1. Add Superman's hometown, Smallville, Kansas to the city table. The 
-- countrycode is 'USA', and population of 45001. (Yes, I looked it up on 
-- Wikipedia.)
insert into city(name, countrycode,district, population)
values ('Smallville','USA','Kansas',45001);

-- 2. Add Kryptonese to the countrylanguage table. Kryptonese is spoken by 0.0001
-- percentage of the 'USA' population.
insert into countrylanguage(countrycode,language, isofficial, percentage)
values ('USA','Kryptonese',false,0.0001);

-- 3. After heated debate, "Kryptonese" was renamed to "Krypto-babble", change 
-- the appropriate record accordingly.
update countrylanguage set language='Kryto-babble' where language = 'Kryptonese';

-- 4. Set the US captial to Smallville, Kansas in the country table.
update country set capital = (select id from city where name = 'Smallville') where name = 'United States';

-- 5. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)

--Did not work due to it being a foreign key in the country table
delete from city where name = 'Smallville';

-- 6. Return the US captial to Washington.
update country set capital = (select id from city where name ILIKE 'Washing%' and countrycode = 'USA') where name = 'United States';

-- 7. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)
delete from city where name = 'Smallville';

-- 8. Reverse the "is the official language" setting for all languages where the
-- country's year of independence is within the range of 1800 and 1972 
-- (exclusive). 
-- (590 rows affected)
update countrylanguage set isofficial = NOT isofficial where countrycode IN (select code from country where indepyear between 1800 and 1972);

-- 9. Convert population so it is expressed in 1,000s for all cities. (Round to
-- the nearest integer value greater than 0.)
-- (4079 rows affected)
update city set population = round(population / 1000);

-- 10. Assuming a country's surfacearea is expressed in square miles, convert it to 
-- square meters for all countries where French is spoken by more than 20% of the 
-- population.
-- (7 rows affected)
update country set surfacearea = surfacearea * .00000259 where code IN (select countrycode from countrylanguage where language = 'French' AND percentage > 20.0);
