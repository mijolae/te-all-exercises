package com.techelevator;

public class KataStringCalculator {
    public int add(String stringOfNums){
        String[] parts;
        if(stringOfNums.contains(",")){
            parts = stringOfNums.split(",");
        }else{
            parts = stringOfNums.split("\n");
        }

        int result = 0;

        for(String string : parts){
            if(string != ""){
                int add = Integer.parseInt(string);
                result += add;
            }
        }

        return result;
    }
}
