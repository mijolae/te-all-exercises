package com.techelevator;

import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KataStringCalculatorTest {

    private KataStringCalculator stringCalculator;

    public void setUp() throws Exception {
        stringCalculator = new KataStringCalculator();
    }

    @Test
    public void testAnEmptyString(){
        assertEquals("", stringCalculator.add(""));
    }

    @Test
    public void testAStringThatSumsTo1(){
        assertEquals(1, stringCalculator.add("1"));
        assertEquals(1, stringCalculator.add("0,1"));
    }

    @Test
    public void testAStringThatSumsTo3(){
        assertEquals(3, stringCalculator.add("1,2"));
    }

    @Test
    public void testAnUnlimitedAmountOfNumbers(){
        assertEquals(24, stringCalculator.add("5,7,12"));
        assertEquals(17, stringCalculator.add("6,2,1,8"));
        assertEquals(22, stringCalculator.add("1,3,5,7,8,-2"));
    }

    @Test
    public void testForNewLineCharacters(){
        assertEquals(10, stringCalculator.add("5\n3,2"));
        assertEquals(14,stringCalculator.add("3\n5\n2,4"));
        assertEquals(17, stringCalculator.add("6\n7,3\n1"));
    }

    @Test
    public void testNumberSeparatorIsExpressedByParallelBars(){
        assertEquals(3, stringCalculator.add("//;\n1;2"));
    }

    @Test(expected = NumberFormatException.class)
    public void testNegativeNumbersShouldThrowException(){
        stringCalculator.add("//;\n-1;-2;-34,-56,345,1023");
    }
}
